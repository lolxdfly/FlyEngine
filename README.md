Todo:
- improve Lighting/Shadow System
	* fix Shadow-Case where Objects dive out of scene
	* Shadow Support for multiple Lights
	* Spotlights
 	* render Shadowmap with glsl to Framebuffer?
	* Screenindependent Lightpositions
- GLWorld
	* add Lighting
	* improve file format
		* binary format
	* GLGridWorld
		* fixed grid size
- GLNet
	* finish BT
	* test
	* improve
- GLPhysics
	* improve or
	* use existing Physics API (Box2D?)
- GLDestroyable?
	* Bones
- GL3DPoly
	* Texturing
