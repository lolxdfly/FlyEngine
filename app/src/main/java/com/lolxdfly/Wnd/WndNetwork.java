package com.lolxdfly.Wnd;

import android.bluetooth.BluetoothDevice;
import android.opengl.GLES20;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUICombobox;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLUtils.GLNet.GLSync;
import com.lolxdfly.flyengine.GLUtils.GLNet.GLWifi;
import com.lolxdfly.flyengine.R;

public class WndNetwork extends WndBase
{
	//private final GLBlueClient gln;
	private final GLWifi gln;
	private final GLRect glo;
	
	private final GLUICombobox<BluetoothDevice> glc;
	
	public WndNetwork()
	{
		glc = new GLUICombobox<BluetoothDevice>(R.drawable.cmbbox, null);
		glc.setPos(10, 175, 500, 275);
		glc.setItemTexture(R.drawable.cmbitem);
		
		//gln = new GLBlueClient(GLSurf.mHelper.getContext());
		//gln.connect();
		//gln.startsearch();
		gln = new GLWifi("192.168.178.25", 8000);
		
		//glc.setItems(gln.getFoundDevices());
		
		glo = new GLRect(false);
		glo.setPos(400, 400, 690, 900);
		glo.setColor(0, 1, 0, 1);
		gln.mServerSync.put(GLSync.nid_test, glo);
	}

	@Override
	public final void onTouch(final MotionEvent event)
	{
		glc.onTouch(event);
		glo.setPos(event.getX(), event.getY());
		gln.serialize(GLSync.nid_test, glo);
	}

	@Override
	public final void onDraw()
	{
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		glo.Render();
		glc.Render();
	}

	@Override
	public final void Process(final long elapsed)
	{
		// TODO: Implement this method
	}
	
	@Override
	public final void Destroy()
	{
		super.Destroy();
		glo.Destroy();
		glc.Destroy();
		gln.Destroy();
	}
}
