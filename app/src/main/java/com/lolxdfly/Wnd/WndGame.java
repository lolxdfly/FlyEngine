package com.lolxdfly.Wnd;

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUIGamePad;
import com.lolxdfly.flyengine.GLElements.GLWorld;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.R;

import java.io.IOException;

public class WndGame extends WndBase
{
	private final GLTexture mBG;
	private final GLWorld mWorld;
	private final GLUIGamePad glp;

	private final GLText mDebug;
	private int time = 0;
	private int frames = 0;

	public WndGame()
	{
		mBG = new GLTexture(R.drawable.background, false);
		mBG.setPos(0, 0, 1920, 1080);

		mWorld = new GLWorld();
		try
		{
			mWorld.initByFile(GLSurf.mHelper.getContext().getAssets().open("test.wld"));
		}
		catch (final IOException e)
		{}

		mDebug = new GLText("00#0000#0000", 0, 0);

		glp = new GLUIGamePad(R.drawable.gamepad,
							  R.drawable.dot,
							  200,
							  50);
	}

	@Override
	public final void onTouch(final MotionEvent event)
	{
		glp.onTouch(event);
	}

	@Override
	public final void onDraw()
	{
		mBG.Render();
		mWorld.Render();
		glp.Render();
		mDebug.Render();
		frames++;
	}

	@Override
	public final void Process(final long elapsed)
	{
		mWorld.Update(elapsed);
		
		time += elapsed;
		if (time >= 1000)
		{
			mDebug.setText(frames + "#" + glp.getXAcc() + "#" + glp.getYAcc());
			frames = time = 0;
		}
	}

	@Override
	public final void Destroy()
	{
		mBG.Destroy();
		mDebug.Destroy();
		glp.Destroy();
		mWorld.Destroy();
		super.Destroy();
	}


}
