package com.lolxdfly.Wnd;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLFire;
import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLSurf;

public final class WndParticles extends WndBase
{
	private final GLText mDebug;
	private int time = 0;
	private int frames = 0;
	
	private final GLFire glf;

	public WndParticles()
	{
		mDebug = new GLText("00#0000#0000", 0, 0);
		glf = new GLFire();
	}
	
	@Override
	public final void onTouch(final MotionEvent event)
	{
		glf.setPos(event.getX(), event.getY());
	}

	@Override
	public final void onDraw()
	{
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		glf.Render();
		mDebug.Render();
		frames++;
	}

	@Override
	public final void Process(final long elapsed)
	{
		glf.Process(elapsed);
		
		time += elapsed;
		if(time >= 1000)
		{
			mDebug.setText(frames + "#" + GLSurf.mCamera.mScreenWidth + "#" + GLSurf.mCamera.mScreenHeight + "@" + glf.getParticleCount());
			frames = time = 0;
		}
	}

	@Override
	public final void Destroy()
	{
		glf.Destroy();
		mDebug.Destroy();
		super.Destroy();
	}
	
}
