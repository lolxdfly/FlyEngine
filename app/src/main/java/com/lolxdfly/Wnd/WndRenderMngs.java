package com.lolxdfly.Wnd;

import android.opengl.GLES20;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLRenderMngs.GLDynamicTileRenderMng;
import com.lolxdfly.flyengine.R;

public final class WndRenderMngs extends WndBase
{
	private final GLText mDebug;
	private int time = 0;
	private int frames = 0;
	
	private float tx = 0f;
	private float ty = 0f;
	private float w = 0.0f;
	
	//private final GLStaticObjRenderMng<GLRect> mRenderer;
	//private final GLStaticTileRenderMng mRenderer;
	//private final GLDynamicObjRenderMng<GLRect> mRenderer;
	private final GLDynamicTileRenderMng mRenderer;
	
	public WndRenderMngs()
	{
		mDebug = new GLText("00#0000#0000", 0, 0);
		
		//mRenderer = new GLStaticObjRenderMng<GLRect>(GLRect.class);
		//mRenderer = new GLStaticTileRenderMng(R.drawable.background);
		//mRenderer = new GLDynamicObjRenderMng<GLRect>(GLRect.class);
		mRenderer = new GLDynamicTileRenderMng(R.drawable.background);
		
		int x = -20, y = 40;
		for(int i = 0; i < 500; i++)
		{
			//final GLRect glo = new GLRect(true);
			//final GLPoint glo = new GLPoint(true);
			//final GLLine glo = new GLLine(true);
			//final GLCircle glo = new GLCircle(true);
			final GLTexture glo = new GLTexture(0, true);
			
			x += 30;
			if(x > 1920)
			{
				x = 10;
				y += 30;
			}
			glo.setPos(x, y, x + 20, y + 20);
			//glo.setPos(x, y);
			//glo.setPos(x, y, x + 20, y + 20);
			//glo.setCircle(x + 10, y + 10, 10, 32);
			//glo.setColor(1, 0, 0, 1);
			mRenderer.addObj(glo, 0, 0, 500, 500);
		}
	}
	
	@Override
	public final void onTouch(final MotionEvent event)
	{
		tx = event.getX();
		ty = event.getY();
		w = 10f;
	}

	@Override
	public final void onDraw()
	{
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		mRenderer.Render();
		mDebug.Render();
		frames++;
	}

	@Override
	public final void Process(final long elapsed)
	{
		time += elapsed;
		if(time >= 1000)
		{
			mDebug.setText(frames + "#" + GLSurf.mCamera.mScreenWidth + "#" + GLSurf.mCamera.mScreenHeight);
			frames = time = 0;
		}
		
		w *= 0.9;
		if(w < 0.5)
			return;
			
		for(int i = 0; i < 500; i++)
			mRenderer.getObj(i).MoveToPoint(tx, ty, w);
	}

	@Override
	public final void Destroy()
	{
		mDebug.Destroy();
		mRenderer.Destroy();
		super.Destroy();
	}
}
