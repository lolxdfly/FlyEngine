package com.lolxdfly.Wnd;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.R;

public class WndFlyEngineBanner extends WndBase
{
	final GLTexture mBanner;
	float fBanAlpha = 0.0f;
	final float mBanPos[] = new float[4];

	public WndFlyEngineBanner()
	{
		GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		mBanner = new GLTexture(R.drawable.font, false);
		mBanPos[0] = GLCamera.mVirtualScreenWidth / 2;
		mBanPos[1] = GLCamera.mVirtualScreenHeight / 2;
		mBanPos[2] = GLCamera.mVirtualScreenWidth / 2;
		mBanPos[3] = GLCamera.mVirtualScreenHeight / 2;
		mBanner.setPos(mBanPos[0], mBanPos[1], mBanPos[2], mBanPos[3]);
		mBanner.setAlpha(0f);
	}
		
	@Override
	public final void onDraw()
	{
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		mBanner.Render();
	}

	@Override
	public final void Destroy()
	{
		mBanner.Destroy();
		super.Destroy();
	}

	@Override
	public final void Process(final long elapsed)
	{
		if (elapsed > 10)
		{
			fBanAlpha += 0.005f;
			mBanner.setAlpha(fBanAlpha);
		}

		if (fBanAlpha < 1.0f)
		{
			mBanPos[0] -= 1.0f;
			mBanPos[1] -= 0.12f;
			mBanPos[2] += 1.0f;
			mBanPos[3] += 0.12f;
			mBanner.setPos(mBanPos[0], mBanPos[1], mBanPos[2], mBanPos[3]);
		}
		else
		{
			//preload
			
			//GLSurf.mTexMng.loadTexture(R.drawable.background);
			GLSurf.mTexMng.loadTexture(R.drawable.font_calibri);
			//GLSurf.mAnimMng.preloadAnim(R.drawable.animtest, 200, 200, 500);
			
			Destroy();
			//final WndDebug mWnd = new WndDebug();
			//final WndShadowDemo mWnd = new WndShadowDemo();
			final WndNormalMapping mWnd = new WndNormalMapping();
			//final WndAnimDemo mWnd = new WndAnimDemo();
			//final WndRenderObjs mWnd = new WndRenderObjs();
			//final WndRenderMngs mWnd = new WndRenderMngs();
			//final WndParticles mWnd = new WndParticles();
			//final WndNetwork mWnd = new WndNetwork();
			//final WndGame mWnd = new WndGame();
			GLSurf.mWndMng.registerWnd(mWnd);
		}
	}

	@Override
	public final void onTouch(final MotionEvent event)
	{
		fBanAlpha = 1.0f; //skip
	}

}
