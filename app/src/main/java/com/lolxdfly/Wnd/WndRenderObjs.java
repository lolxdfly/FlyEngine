package com.lolxdfly.Wnd;

import android.opengl.GLES20;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GL3DPoly;
import com.lolxdfly.flyengine.GLElements.GLCircle;
import com.lolxdfly.flyengine.GLElements.GLLine;
import com.lolxdfly.flyengine.GLElements.GLPoint;
import com.lolxdfly.flyengine.GLElements.GLPoly;
import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLPostProcessor;
import com.lolxdfly.flyengine.R;

public final class WndRenderObjs extends WndBase
{
	private final GLTexture mBG;
	private final GLText mDebug;
	private int time = 0;
	private int frames = 0;

	private final GLPoint glpt;
	private final GLLine gll;
	private final GLRect glo;
	private final GLPoly glp;
	private final GLCircle glc;
	private final GL3DPoly gl3dp;

	private final GLPostProcessor glpp;

	public WndRenderObjs()
	{
		mBG = new GLTexture(R.drawable.background, false);
		mBG.setPos(0, 0, 1920, 1080);
		mDebug = new GLText("00#0000#0000", 0, 0);

		glpp = new GLPostProcessor();
		GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		glpt = new GLPoint(false);
		glpt.setPos(50, 50);
		glpt.setColor(0, 0, 0, 1);

		//works
		//if(glpt.checkPtCollision(40, 50))
		//glpt.setColor(1, 1, 1, 1);

		gll = new GLLine(false);
		gll.setPos(1020, 0, 1020, 905);
		gll.setColor(0, 0, 0, 1);

		//works
		//if(gll.checkPtCollision(1920, 500))
		//gll.setColor(1, 0, 0, 1);

		glo = new GLRect(false);
		glo.setPos(970, 900, 1070, 1050);
		glo.setColor(0, 0, 0, 1);

		//works
		//if(glo.checkPtCollision(550, 250))
		//glo.setColor(1, 1, 1, 1);

		glp = new GLPoly(false, GLES20.GL_TRIANGLE_FAN);
		final float pos[] = 
		{
			550, 900,
			700, 900,
			1000, 1000,
			600, 500,
			550, 510,
			500, 500
		};
		//GLSurf.mCamera.SortVecDeg(pos);
		glp.setVertices(pos);
		glp.setColor(0, 0, 0, 1);

		//works
		//if(glp.checkPtCollision(0, 60))
		//glp.setColor(1, 1, 1, 1);

		glc = new GLCircle(false);
		glc.setCircle(300, 300, 200, 64);
		glc.setColor(0, 0, 0, 1);
		
		GLSurf.mCamera.setNearAndFar(0f, 200f);
		gl3dp = new GL3DPoly(false);
		//gl3dp.setVertices(..);
		gl3dp.Move(500, 500);
		//gl3dp.setColor(0, 0, 0, 1);
	}

	@Override
	public final void onTouch(final MotionEvent event)
	{
		gll.rotate(1);
		glo.rotate(1);
		glp.rotate(1);
		glc.rotate(1);
		gl3dp.rotate(1, (short)1);

		glpt.setColor(0, 0, 0, 1);
		gll.setColor(0, 0, 0, 1);
		glo.setColor(0, 0, 0, 1);
		glp.setColor(0, 0, 0, 1);
		glc.setColor(0, 0, 0, 1);
		
		if (glp.checkCollision(gll))
			glp.setColor(1, 0, 0, 1);
		if (gll.checkCollision(glp))
			gll.setColor(1, 0, 0, 1);

		if (glo.checkCollision(gll))
			glo.setColor(1, 0, 0, 1);
		if (gll.checkCollision(glo))
			gll.setColor(1, 0, 0, 1);

		if (glp.checkCollision(glo))
			glp.setColor(1, 0, 0, 1);
		if (glo.checkCollision(glp))
			glo.setColor(1, 0, 0, 1);

		if (glp.checkCollision(glc))
			glp.setColor(1, 0, 0, 1);
		if (glc.checkCollision(glp))
			glc.setColor(1, 0, 0, 1);
	}

	@Override
	public final void onDraw()
	{
		glpp.beginRender();
		mBG.Render();
		glpt.Render();
		gll.Render();
		glo.Render();
		glp.Render();
		glc.Render();
		gl3dp.Render();
		glpp.endRender();
		glpp.Render();
		mDebug.Render();
		frames++;
	}

	@Override
	public final void Process(final long elapsed)
	{
		time += elapsed;
		if (time >= 1000)
		{
			mDebug.setText(frames + "#" + GLSurf.mCamera.mScreenWidth + "#" + GLSurf.mCamera.mScreenHeight);
			frames = time = 0;
		}
	}

	@Override
	public void Destroy()
	{
		mBG.Destroy();
		glpp.Destory();
		glpt.Destroy();
		gll.Destroy();
		glo.Destroy();
		glp.Destroy();
		glc.Destroy();
		gl3dp.Destroy();
		mDebug.Destroy();
		super.Destroy();
	}
}
