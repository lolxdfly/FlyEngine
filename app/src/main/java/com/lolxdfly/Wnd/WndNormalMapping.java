package com.lolxdfly.Wnd;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTextureEx;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.R;

public final class WndNormalMapping extends WndBase
{
	private final GLTextureEx mBG;
	private final GLText mDebug;
	private int time = 0;
	private int frames = 0;

	public WndNormalMapping()
	{
		mBG = new GLTextureEx(R.drawable.background, R.drawable.background_normal, false);
		mBG.setPos(0, 0, 1920, 1080);

		mDebug = new GLText("00#0000#0000", 0, 0);
	}

	@Override
	public final void onTouch(final MotionEvent event)
	{
		for(short i = 0; i < GLHelp.MAX_LIGHTS; i++)
			mBG.turnoffLight(i);
			
		if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			for (int i = 0; i < event.getPointerCount() && i < GLHelp.MAX_LIGHTS; i++)
			{
				final float x = event.getX(i);
				final float y = event.getY(i);
				mBG.addLight(i, new float[]{x, y, 200}, new float[]{1, 1, 1});
			}
		}
	}

	@Override
	public final void onDraw()
	{
		mBG.Render();
		mDebug.Render();
		frames++;
	}

	@Override
	public final void Process(final long elapsed)
	{
		time += elapsed;
		if (time >= 1000)
		{
			mDebug.setText(frames + "#" + GLSurf.mCamera.mScreenWidth + "#" + GLSurf.mCamera.mScreenHeight);
			frames = time = 0;
		}
	}

	@Override
	public final void Destroy()
	{
		mBG.Destroy();
		mDebug.Destroy();
		super.Destroy();
	}
}
