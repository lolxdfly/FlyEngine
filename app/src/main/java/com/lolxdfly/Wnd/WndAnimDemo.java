package com.lolxdfly.Wnd;

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLAnimation;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.R;

public class WndAnimDemo extends WndBase
{
	final GLTexture mBG;
	final GLAnimation mAnim;
	
	public WndAnimDemo()
	{
		mBG = new GLTexture(R.drawable.background, false);
		mBG.setPos(0, 0, 1920, 1080);
		
		mAnim = new GLAnimation(GLSurf.mAnimMng.getAnim(R.drawable.animtest));
		mAnim.setPos(500, 500);
	}
	
	@Override
	public void onTouch(MotionEvent event)
	{
		// TODO: Implement this method
	}

	@Override
	public void onDraw()
	{
		mBG.Render();
		mAnim.Render();
	}

	@Override
	public void Process(long elapsed)
	{
		mAnim.Update(elapsed);
	}

	@Override
	public void Destroy()
	{
		mBG.Destroy();
		mAnim.Destroy();
		super.Destroy();
	}
}
