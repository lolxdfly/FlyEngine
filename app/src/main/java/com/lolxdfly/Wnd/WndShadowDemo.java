package com.lolxdfly.Wnd;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLUtils.GLLightMng;
import com.lolxdfly.flyengine.GLUtils.GLShadowMng;
import com.lolxdfly.flyengine.R;

public final class WndShadowDemo extends WndBase
{
	private final GLLightMng gllm = new GLLightMng();
	private final GLShadowMng glsm = new GLShadowMng(gllm);

	private final GLTexture mBG;
	private final GLRect glo;
	private final GLRect glo1;
	private final GLRect glo2;
	private final GLRect glo3;

	public WndShadowDemo()
	{
		mBG = new GLTexture(R.drawable.background, false);
		mBG.setPos(0, 0, 1920, 1080);
		//mBG.setShader(GLShader.sProgram_Lighting);

		glo = new GLRect(false);
		glo.setPos(500, 500, 650, 650);
		glo.setColor(0, 0.8f, 0, 1);

		glo1 = new GLRect(false);
		glo1.setPos(100, 100, 250, 250);
		glo1.setColor(0, 0.8f, 0, 1);

		glo2 = new GLRect(false);
		glo2.setPos(1650, 100, 1800, 250);
		glo2.setColor(0, 0.8f, 0, 1);

		glo3 = new GLRect(false);
		glo3.setPos(850, 450, 1000, 600);
		glo3.setColor(0, 0.8f, 0, 1);

		glsm.registerObj(glo);
		glsm.registerObj(glo1);
		glsm.registerObj(glo2);
		glsm.registerObj(glo3);
		
		//GLSurf.mCamera.Move(-400, -400);
	}

	@Override
	public void onTouch(MotionEvent event)
	{
		if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			gllm.addLight(0, new float[]{event.getX(), event.getY()}, 4, new float[]{1, 1, 1, 1});
		}
	}

	@Override
	public void onDraw()
	{
		gllm.beginRender();
		mBG.Render();
		gllm.endRender();
		gllm.Render();
		glo.Render();
		glo1.Render();
		glo2.Render();
		glo3.Render();
		glsm.Render();
	}

	float w = 0.0f;

	@Override
	public final void Process(final long elapsed)
	{
		w += 0.01;
		glo.setPos(900 + (float)Math.cos(w) * 650, 450 + (float)Math.sin(w) * 350);
		glo1.setPos(100, 450 + (float)Math.sin(w) * 550);
		glo2.setPos(900 + (float)Math.cos(w) * 650, 100);
	}

	@Override
	public void Destroy()
	{
		mBG.Destroy();
		glo.Destroy();
		glo1.Destroy();
		glo2.Destroy();
		glo3.Destroy();
		glsm.Destroy();
		gllm.Destory();
		super.Destroy();
	}
}
