package com.lolxdfly.flyengine;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdSize;
import com.lolxdfly.Wnd.WndFlyEngineBanner;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.AdvertisingMng;
import com.lolxdfly.flyengine.GLUtils.GPlayServiceMng;

public final class MainActivity extends Activity
{
	private GLSurf glSurfaceView;
	public static AdvertisingMng mAMng;
	public static GPlayServiceMng mGPlay;

    /** Called when the activity is first created. */
    @Override
    public final void onCreate(final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		//create a layout and an entryWnd
		final RelativeLayout rl = new RelativeLayout(this);
		final WndFlyEngineBanner g_entry = null;
		glSurfaceView = new GLSurf(this, g_entry, WndFlyEngineBanner.class);
		rl.addView(glSurfaceView);
        setContentView(rl);

		mGPlay = new GPlayServiceMng(this);
		mGPlay.signIn();

		//attach Ads on layout
		final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT); 
		lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		mAMng = new AdvertisingMng(this, rl, "ca-app-pub-4161712998623794/8466330222", AdSize.BANNER, lp);
		mAMng.attach();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mGPlay.onActivityResult(requestCode, resultCode, data);
	}

	@Override
    public final void onPause()
	{
		super.onPause();
		glSurfaceView.onPause();
		mAMng.setState(true);
    }

    @Override
    public final void onResume()
	{
		super.onResume();
		glSurfaceView.onResume();
		mAMng.setState(false);
    }

	@Override
	public final boolean onKeyDown(final int keyCode, final KeyEvent event)
	{
		glSurfaceView.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			//mGPlay.Destroy();
			mAMng.Destroy();
			finish();
			System.exit(0);
		}
		return true;
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		glSurfaceView.Destroy();
	}
}
