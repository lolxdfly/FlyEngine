package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public final class GL3DPoly extends GLRenderObj
{
	//GLRenderObjEx
	public float center[];
	private float rot[];

	private FloatBuffer vertexBuffer;
	private FloatBuffer colorBuffer;

	/**
	 * Only use this class for visualisation and not for game components!
	 */
	public GL3DPoly(final boolean _bUsedInRenderMng)
	{
		center = new float[]{0, 0, 0};
		rot = new float[]{0, 0, 0};

		mRect = new float[]
		{
			-100.0f,-100.0f,-100.0f,
			-100.0f,-100.0f, 100.0f,
			-100.0f, 100.0f, 100.0f,
			100.0f, 100.0f,-100.0f,
			-100.0f,-100.0f,-100.0f,
			-100.0f, 100.0f,-100.0f,
			100.0f,-100.0f, 100.0f,
			-100.0f,-100.0f,-100.0f,
			100.0f,-100.0f,-100.0f,
			100.0f, 100.0f,-100.0f,
			100.0f,-100.0f,-100.0f,
			-100.0f,-100.0f,-100.0f,
			-100.0f,-100.0f,-100.0f,
			-100.0f, 100.0f, 100.0f,
			-100.0f, 100.0f,-100.0f,
			100.0f,-100.0f, 100.0f,
			-100.0f,-100.0f, 100.0f,
			-100.0f,-100.0f,-100.0f,
			-100.0f, 100.0f, 100.0f,
			-100.0f,-100.0f, 100.0f,
			100.0f,-100.0f, 100.0f,
			100.0f, 100.0f, 100.0f,
			100.0f,-100.0f,-100.0f,
			100.0f, 100.0f,-100.0f,
			100.0f,-100.0f,-100.0f,
			100.0f, 100.0f, 100.0f,
			100.0f,-100.0f, 100.0f,
			100.0f, 100.0f, 100.0f,
			100.0f, 100.0f,-100.0f,
			-100.0f, 100.0f,-100.0f,
			100.0f, 100.0f, 100.0f,
			-100.0f, 100.0f,-100.0f,
			-100.0f, 100.0f, 100.0f,
			100.0f, 100.0f, 100.0f,
			-100.0f, 100.0f, 100.0f,
			100.0f,-100.0f, 100.0f
		};

		mColor = new float[]
		{
			0.583f,  0.771f,  0.014f, 1.0f,
			0.609f,  0.115f,  0.436f, 1.0f,
			0.327f,  0.483f,  0.844f, 1.0f,
			0.822f,  0.569f,  0.201f, 1.0f,
			0.435f,  0.602f,  0.223f, 1.0f,
			0.310f,  0.747f,  0.185f, 1.0f,
			0.597f,  0.770f,  0.761f, 1.0f,
			0.559f,  0.436f,  0.730f, 1.0f,
			0.359f,  0.583f,  0.152f, 1.0f,
			0.483f,  0.596f,  0.789f, 1.0f,
			0.559f,  0.861f,  0.639f, 1.0f,
			0.195f,  0.548f,  0.859f, 1.0f,
			0.014f,  0.184f,  0.576f, 1.0f,
			0.771f,  0.328f,  0.970f, 1.0f,
			0.406f,  0.615f,  0.116f, 1.0f,
			0.676f,  0.977f,  0.133f, 1.0f,
			0.971f,  0.572f,  0.833f, 1.0f,
			0.140f,  0.616f,  0.489f, 1.0f,
			0.997f,  0.513f,  0.064f, 1.0f,
			0.945f,  0.719f,  0.592f, 1.0f,
			0.543f,  0.021f,  0.978f, 1.0f,
			0.279f,  0.317f,  0.505f, 1.0f,
			0.167f,  0.620f,  0.077f, 1.0f,
			0.347f,  0.857f,  0.137f, 1.0f,
			0.055f,  0.953f,  0.042f, 1.0f,
			0.714f,  0.505f,  0.345f, 1.0f,
			0.783f,  0.290f,  0.734f, 1.0f,
			0.722f,  0.645f,  0.174f, 1.0f,
			0.302f,  0.455f,  0.848f, 1.0f,
			0.225f,  0.587f,  0.040f, 1.0f,
			0.517f,  0.713f,  0.338f, 1.0f,
			0.053f,  0.959f,  0.120f, 1.0f,
			0.393f,  0.621f,  0.362f, 1.0f,
			0.673f,  0.211f,  0.457f, 1.0f,
			0.820f,  0.883f,  0.371f, 1.0f,
			0.982f,  0.099f,  0.879f, 1.0f
		};

		bUsedInRenderMng = _bUsedInRenderMng;
		if (bUsedInRenderMng)
			return;

		// initialize vertex byte buffer for shape coordinates
		ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		vertexBuffer = b.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		colorBuffer = b.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		///VBO///
		buffers = new int[2];
		GLES20.glGenBuffers(2, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		///

		vertexBuffer.clear();
		colorBuffer.clear();

		mProgram = GLShader.sProgram_Obj.mProgram;
		mPosHandle = GLShader.sProgram_Obj.mPosHandle;
		mColorHandle = GLShader.sProgram_Obj.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Obj.mMatrixHandle;
	}

	@Override
	public final void Render()
	{
		if (!bVisible)
			return;

        GLES20.glUseProgram(mProgram);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX + 1, GLES20.GL_FLOAT, false, 0, 0);//vertexBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);//colorBuffer);

		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxMVP/*mtrx*/, 0);

		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, mRect.length / 2);
		//GLES20.glDrawElements(mType, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, 0);

		// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}

	public final void setVertices(final float vec[])
	{
		mRect = vec;

		for (int i = 0; i < 3; i++)
			rot[i] = 0.0f;
		center = GLCamera.getCenter3D(mRect);

		final int len = mRect.length;
		final float tmpcolor[] = new float[4];
		for (short s = 0; s < 4; s++)
			tmpcolor[s] = mColor[s];

		mColor = new float[len * 2];
		setColor(tmpcolor[0], tmpcolor[1], tmpcolor[2], tmpcolor[3]);

		UpdateVertice(true);
	}

	public final void Move(final float x, final float y)
	{
		final float[] tmp = new float[]{rot[0], rot[1], rot[2]};
		for (short i = 0; i < 3; i++)
		{
			if (rot[i] != 0)
			{
				setdegree(0, i);
			}
		}

		for (int i = 0; i < mRect.length; i += 3)
		{
			mRect[i] += x;
			mRect[i + 1] += y;
		}

		center = GLCamera.getCenter3D(mRect);
		for (short i = 0; i < 3; i++)
		{
			if (tmp[i] != 0)
			{
				setdegree(tmp[i], i);
			}
		}

		UpdateVertice(false);
	}

	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[5]);

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / dy1) * speed;
		float dy = (dy1 / dx1) * speed;

		if (dx > (int)GLCamera.mVirtualScreenWidth || dy > (int)GLCamera.mVirtualScreenHeight)
			return;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[5]);
		//double d = Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / (dx1 + dy1)) * speed;
		float dy = (dy1 / (dx1 + dy1)) * speed;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void UpdateVertice(final boolean brot)
	{
		super.UpdateVertice();
		if (!checkRender())
			return;

		if (brot)
		{
			for (int i = 0; i < 3; i++)
				rot[i] = 0.0f;
			center = GLCamera.getCenter3D(mRect);
		}

		final ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		vertexBuffer = b.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		//GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, vBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vBuffer);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		vertexBuffer.clear();
	}

	@Override
	public final void UpdateColors()
	{
		if (bUsedInRenderMng)
			return;

		final ByteBuffer b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		colorBuffer = b.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		//GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		colorBuffer.clear();
	}


	public final void rotate(final float degree, final short axis)
	{
		rot[axis] = degree;
		rotatePos3D(degree, axis);
	}

	public final void setdegree(final float degree, final short axis)
	{
		if ((degree - rot[axis]) != 0)
		{
			rotatePos3D(degree - rot[axis], axis);
			rot[axis] = degree;
		}
	}

	public final float getdegree(final short axis)
	{
		return rot[axis];
	}

	protected final void rotatePos3D(final float degree, final short axis)
	{
		switch (axis)
		{
			case 0: //x
				{
					final float c[] = new float[]{center[1], center[2]};
					for (int i = 0; i < mRect.length; i += 3)
					{
						final float points[] = new float[]{mRect[i + 1], mRect[i + 2]};
						GLCamera.rotateVec_point(points, c, degree);
						mRect[i + 1] = points[0];
						mRect[i + 2] = points[1];
					}
					break;
				}
			case 1: //y
				{
					final float c[] = new float[]{center[0], center[2]};
					for (int i = 0; i < mRect.length; i += 3)
					{
						final float points[] = new float[]{mRect[i], mRect[i + 2]};
						GLCamera.rotateVec_point(points, c, degree);
						mRect[i] = points[0];
						mRect[i + 2] = points[1];
					}
					break;
				}
			case 2: //z
				{
					for (int i = 0; i < mRect.length; i += 3)
					{
						final float points[] = new float[]{mRect[i], mRect[i + 1]};
						GLCamera.rotateVec_point(points, center, degree);
						mRect[i] = points[0];
						mRect[i + 1] = points[1];
					}
					break;
				}
		}
		UpdateVertice(false);
	}
}
