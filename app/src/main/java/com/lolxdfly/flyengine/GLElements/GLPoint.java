package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLPoint extends GLRenderObj
{
	private FloatBuffer vertexBuffer;
	//private ShortBuffer drawListBuffer;
	private FloatBuffer colorBuffer;

	private static final byte drawOrder[] = {0};

	private static final int mStdIdxBuff[] = new int[1];
	private static ByteBuffer bbvertexbuff;
	private static ByteBuffer bbcolorbuff;

	public GLPoint(final boolean _bUsedInRenderMng)
	{
		mRect = new float[]
		{
			100.0f,	100.0f
		};

		mColor = new float[]
		{
			1.0f, 1.0f, 1.0f, 1.0f
		};
		
		bUsedInRenderMng = _bUsedInRenderMng;
		if (bUsedInRenderMng)
			return;

		// initialize vertex byte buffer for shape coordinates
		/*ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		 b.order(ByteOrder.nativeOrder());*/
		vertexBuffer = bbvertexbuff.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		/*b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		 b.order(ByteOrder.nativeOrder());*/
		colorBuffer = bbcolorbuff.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		///VBO///
		buffers = new int[2];
		GLES20.glGenBuffers(2, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		///

		vertexBuffer.clear();
		colorBuffer.clear();

		mProgram = GLShader.sProgram_Obj.mProgram;
		mPosHandle = GLShader.sProgram_Obj.mPosHandle;
		mColorHandle = GLShader.sProgram_Obj.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Obj.mMatrixHandle;
	}

	public static final void genStdBuffers()
	{
		//Vertex
		bbvertexbuff = ByteBuffer.allocateDirect(2 * GLHelp.BYTES_PER_FLOAT);
		bbvertexbuff.order(ByteOrder.nativeOrder());

		//Color
		bbcolorbuff = ByteBuffer.allocateDirect(4 * GLHelp.BYTES_PER_FLOAT);
		bbcolorbuff.order(ByteOrder.nativeOrder());

		//Index
		final ByteBuffer drawListBuffer = ByteBuffer.allocateDirect(drawOrder.length);
        drawListBuffer.order(ByteOrder.nativeOrder());
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

		//move into vram
		GLES20.glGenBuffers(1, mStdIdxBuff, 0);						
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mStdIdxBuff[0]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity(), drawListBuffer, GLES20.GL_STATIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	public static final void ReleaseStdIdxBuff()
	{
		//release std index buffer
		GLES20.glDeleteBuffers(1, mStdIdxBuff, 0);
	}

	@Override
	public final void Render()
	{
		if (!bVisible)
			return;

        GLES20.glUseProgram(mProgram);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);//vertexBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);//colorBuffer);

		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxMVP/*mtrx*/, 0);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mStdIdxBuff[0]);
		//GLES20.glDrawArrays(GLES20.GL_POINTS, 0, drawOrder.length);
		GLES20.glDrawElements(GLES20.GL_POINTS, drawOrder.length, GLES20.GL_UNSIGNED_BYTE, 0);

		// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}
	
	public final void setPos(final float x, final float y)
	{
		mRect[0] = x;
		mRect[1] = y;
		UpdateVertice();
	}
	
	public final void Move(final float dx, final float dy)
	{
		mRect[0] += dx;
		mRect[1] += dy;
		UpdateVertice();
	}
	
	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[1]);

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / dy1) * speed;
		float dy = (dy1 / dx1) * speed;

		if (dx > (int)GLCamera.mVirtualScreenWidth || dy > (int)GLCamera.mVirtualScreenHeight)
			return;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[1]);
		//double d = Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / (dx1 + dy1)) * speed;
		float dy = (dy1 / (dx1 + dy1)) * speed;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	@Override
	public final void UpdateVertice()
	{
		if (!checkRender())
			return;

		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		vertexBuffer.clear();
	}

	@Override
	protected final void UpdateColors()
	{
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		colorBuffer.clear();
	}

	@Override
	public final boolean checkPtCollision(final float x, final float y)
	{
		return mRect[0] == x && mRect[1] == y;
	}
}
