package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Path;
import android.graphics.Region;
import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLNet.GLSerializable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public abstract class GLRenderObj extends GLBaseObj implements GLSerializable
{
	protected boolean bUsedInRenderMng = false;
	
	protected int mPosHandle;
	protected int mMatrixHandle;
	protected int mColorHandle;
	
	protected int buffers[]; // 3=>vertex,(index),color
	public float mRect[];
	public float mColor[];
	//private short mDrawOrder[];
	
	public Region mReg;
	
	public abstract void Render();
	
	protected void UpdateVertice()
	{
		mReg = null;
	}
	protected abstract void UpdateColors();
	
	protected Region getRgn()
	{
		final Path p = new Path();
		p.moveTo(mRect[0], mRect[1]);
		for(int i = 2; i < mRect.length; i+= 2)
			p.lineTo(mRect[i], mRect[i + 1]);
		p.close();
		final Region r = new Region();
		r.setPath(p, new Region(0, 0, (int)GLCamera.mVirtualScreenWidth, (int)GLCamera.mVirtualScreenHeight));
		return r;
	}
	
	public void clearCacheCollision(final GLRenderObj glr)
	{
		mReg = null;
		glr.mReg = null;
	}
	public boolean checkCollision_cached(final GLRenderObj glr)
	{
		if(mReg == null)
			mReg = getRgn();
		if(glr.mReg == null)
			glr.mReg = getRgn();
		
		return mReg.op(glr.mReg, Region.Op.INTERSECT);
	}
	public boolean checkCollision(final GLRenderObj glr)
	{
		return getRgn().op(glr.getRgn(), Region.Op.INTERSECT);
	}
	public final boolean checkCollision(final GLPoint glp)
	{
		return checkPtCollision(glp.mRect[0], glp.mRect[1]);
	}
	public boolean checkPtCollision(final float x, final float y)
	{
		return getRgn().contains((int)x, (int)y);
	}
	
	public void Destroy()
	{
		if (bUsedInRenderMng)
			return;

		GLES20.glDeleteBuffers(buffers.length, buffers, 0);
		setVisible(false);
	}

	protected final boolean checkRender()
	{
		if (bUsedInRenderMng)
			return false;

		if (!GLSurf.mCamera.isVisible(mRect))
		{
			setVisible(false);
			return false;
		}

		setVisible(true);
		return true;
	}
	
	public final void setColor(final float r, final float g, final float b, final float a)
	{
		if (a == 0.0f)
		{
			setVisible(false);
			return;
		}

		for (int i = 0; i < (mColor.length / 4); i++)
		{
			mColor[i * 4] = r;
			mColor[i * 4 + 1] = g;
			mColor[i * 4 + 2] = b;
			mColor[i * 4 + 3] = a;
		}

		if (!bUsedInRenderMng)
			UpdateColors();
	}

	public final void setAlpha(final float a)
	{
		for (int i = 3; i < mColor.length; i += 4)
			mColor[i] = a;

		if (a == 0.0f)
		{
			setVisible(false);
			return;
		}

		if (!bUsedInRenderMng)
			UpdateColors();
	}
	
	@Override
	public final void serialize(final ObjectOutputStream oos)
	{
		try
		{
			oos.write(mRect.length);
			for (final float f : mRect)
				oos.writeFloat(f);
		}
		catch (final IOException e)
		{}
	}
	
	@Override
	public final void serialize(final ObjectInputStream ois)
	{
		try
		{
			final int n = ois.read();
			for (int i = 0; i < n; i++)
				mRect[i] = ois.readFloat();
		}
		catch (final IOException e)
		{}
	}
}
