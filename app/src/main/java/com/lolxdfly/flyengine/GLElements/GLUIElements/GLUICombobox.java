package com.lolxdfly.flyengine.GLElements.GLUIElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLMain.GLSurf;

import java.util.ArrayList;

public final class GLUICombobox<E> extends GLUIButton
{
	private boolean showDropMenu = false;
	private ArrayList<E> mItems;
	private int mItemClicked = -1;
	private final E sDefault;
	private final int mTexID[] = new int[2];
	private boolean bHoldOnSelection = false;

	public GLUICombobox(final int RID, final E defaultItem)
	{
		super(RID);
		mItems = new ArrayList<E>();
		sDefault = defaultItem;
		if(sDefault != null)
			setText(sDefault.toString());
		mTexID[1] = mTexID[0] = mTextureID;
	}

	public final void setItemTexture(final int RID)
	{
		if (mTexID[0] != mTexID[1])
			GLSurf.mTexMng.ReleaseTexture(mTexID[1]);
		mTexID[1] = GLSurf.mTexMng.loadTexture(RID);
	}

	@Override
	public final void Render()
	{
		super.Render();

		if (showDropMenu)
		{
			final int MainMode = nMode;
			for (short s = 0; s < mItems.size(); s++)
			{
				Move(0, (mRect[1] - mRect[5]));
				if (s == mItemClicked)
					setMode(1);
				else
					setMode(0);
				setText(mItems.get(s).toString());
				mTextureID = mTexID[1];
				super.Render();
			}

			//reset
			Move(0, -mItems.size() * (mRect[1] - mRect[5]));
			setMode(MainMode);
			if (mItemClicked != -1)
				setText(getSelectedItem().toString());
			else if(sDefault != null)
				setText(sDefault.toString());
			mTextureID = mTexID[0];
		}
	}

	public final void addItem(final E item)
	{
		mItems.add(item);
	}
	
	public final void setItems(final ArrayList<E> al)
	{
		mItems = al;
	}

	@Override
	public final boolean isClicked(final MotionEvent event)
	{
		boolean ret = super.isClicked(event);
		if (ret)
			showDropMenu = !showDropMenu;

		if (showDropMenu)
		{
			for (short s = 0; s < mItems.size(); s++)
			{
				Move(0, (mRect[1] - mRect[5]));
				if (checkPtCollision(event.getX(), event.getY()))
				{
					ret = true;
					mItemClicked = s;
					if (!bHoldOnSelection)
					{
						showDropMenu = false;
						setText(getSelectedItem().toString());
					}
				}
			}
			Move(0, -mItems.size() * (mRect[1] - mRect[5]));
		}
		return ret;
	}

	public final boolean onTouch(final MotionEvent event)
	{
		return isClicked(event);
	}

	public final E getSelectedItem()
	{
		if(mItemClicked != -1)
			return mItems.get(mItemClicked);
		else
			return sDefault;
	}

	public final void setHoldOnSelection(final boolean b)
	{
		bHoldOnSelection = b;
	}

	@Override
	public final void Destroy()
	{
		mItems.clear();
		mItems = null;
		mTextureID = mTexID[0];
		if (mTexID[0] != mTexID[1])
			GLSurf.mTexMng.ReleaseTexture(mTexID[1]);
		super.Destroy();
	}
}
