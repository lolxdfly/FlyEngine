package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Bitmap;

import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLAnimationMng;
import com.lolxdfly.flyengine.R;

public class GLAnimation extends GLTexture
{
	private final float uvoffset[] = new float[2];
	private final float atlassize[] = new float[2];
	private int time = 0;
	private long deltatime = 0;
	private boolean bOnfinish = false;
	private Runnable rOnfinish;
	private boolean ispreloaded = false;

	public GLAnimation()
	{
		super(R.drawable.texdef, false);
	}

	public GLAnimation(final GLAnimationMng.Anim a)
	{
		super(R.drawable.texdef, false);
		ispreloaded = true;
		//setTexture(a.RID);
		mTextureID = a.texid;
		atlassize[0] = a.atlaswidth;
		atlassize[1] = a.atlasheight;
		uvoffset[0] = a.width;
		uvoffset[1] = a.height;
		deltatime = a.dtime;

		//uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
		uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
		uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);
		updateUVBuff();
	}

	public GLAnimation(final int RID, final float width, final float height, final int dtime)
	{
		super(RID, false);
		deltatime = dtime;
		if (dtime == -1) //inactive anim
			return;

		final Bitmap bmp = GLSurf.mTexMng.getBitmap(RID);
		atlassize[0] = bmp.getWidth(); //144??
		atlassize[1] = bmp.getHeight(); //"
		uvoffset[0] = width;
		uvoffset[1] = height;

		//uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
		uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
		uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);
		updateUVBuff();
	}

	@Override
	public final void setTexture(final int RID)
	{
		super.setTexture(RID);
		final Bitmap bmp = GLSurf.mTexMng.getBitmap(RID);
		atlassize[0] = bmp.getWidth();
		atlassize[1] = bmp.getHeight();

		//reset
		uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
		uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
		uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);
		updateUVBuff();
	}

	@Override
	public final void setTexture(final Bitmap bmp)
	{
		super.setTexture(bmp);
		atlassize[0] = bmp.getWidth();
		atlassize[1] = bmp.getHeight();

		//reset
		uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
		uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
		uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);
		updateUVBuff();
	}

	public final void setAnimation(final int RID, final float width, final float height, final int dtime)
	{
		if (ispreloaded)
		{
			ispreloaded = false;
			mTextureID = GLSurf.mTexMng.loadTexture(RID);
		}
		else
			setTexture(RID);
		deltatime = dtime;
		if (dtime == -1) //inactive anim
		{
			uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
			uvs[2] = uvs[6] = uvs[1] = uvs[3] = 1;
			updateUVBuff();
			return;
		}
		final Bitmap bmp = GLSurf.mTexMng.getBitmap(RID);
		atlassize[0] = bmp.getWidth();
		atlassize[1] = bmp.getHeight();
		uvoffset[0] = width;
		uvoffset[1] = height;

		//uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
		uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
		uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);
		updateUVBuff();
	}

	public final void setAnimation(final GLAnimationMng.Anim a)
	{
		ispreloaded = true;
		//setTexture(a.RID);
		mTextureID = a.texid;
		atlassize[0] = a.atlaswidth;
		atlassize[1] = a.atlasheight;
		uvoffset[0] = a.width;
		uvoffset[1] = a.height;
		deltatime = a.dtime;

		//uvs[0] = uvs[4] = uvs[5] = uvs[7] = 0;
		uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
		uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);
		updateUVBuff();
	}

	public void Update(final long elapsed)
	{
		if (!bVisible || deltatime == -1)
			return;

		time += elapsed;
		if (time >= deltatime)
		{
			if (uvs[2] >= 1.0f) //line finished
			{
				//new line
				uvs[0] = uvs[4] = 0;
				uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);

				if (uvs[1] >= 1.0f) //finish
				{
					//reset
					/*uvs[0] = uvs[4] =*/ uvs[5] = uvs[7] = 0;
					//uvs[2] = uvs[6] = (uvoffset[0] / atlassize[0]);
					uvs[1] = uvs[3] = (uvoffset[1] / atlassize[1]);

					if (bOnfinish)
						rOnfinish.run();
				}
				else
				{
					for (short s = 1; s <= 7; s += 2)
						uvs[s] += (uvoffset[1] / atlassize[1]);
				}
			}
			else
			{
				for (short s = 0; s <= 6; s += 2)
					uvs[s] += (uvoffset[0] / atlassize[0]);
			}
			updateUVBuff();
			time = 0;
		}
	}

	public final void onAnimationfinish(final Runnable run)
	{
		bOnfinish = true;
		rOnfinish = run;
	}
}
