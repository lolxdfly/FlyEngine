package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;
import com.lolxdfly.flyengine.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLText
{
	private final int mTexID;
	private String mText;
	private float mPosX;
	private float mPosY;
	private final float mColor[];
	private float mScale = 1.0f;
	private float mtrx[];
	private float rot = 0.0f;

	private int mProgram;
	private FloatBuffer vertexuvBuffer;
	private ShortBuffer drawListBuffer;
	private FloatBuffer colorBuffer;

	private final int mPosHandle;
	private final int mTexCoordHandle;
	private final int mSamplerHandle;
	private final int mMatrixHandle;
	private final int mColorHandle;

	private float mCursorPosX;
	private boolean bVisible;

	private float vecuv[];
	private short drawOrder[];
	private float color[];
	private final int buffers[] = new int[3];

	private static final int stride = (GLHelp.COORDS_PER_VERTEX + GLHelp.TEX_COORDS_PER_VERTEX) * GLHelp.BYTES_PER_FLOAT;

	private static final float mCharInfo[][] =
	{
		//uvright, width
		{0.00000f, 00f}, //32  ' '
		{0.00331f, 04f}, //33  '!'
		{0.01077f, 09f}, //34  '"'
		{0.02320f, 15f}, //35  '#'
		{0.03480f, 28f}, //36  '$'
		{0.05302f, 23f}, //37  '%'
		{0.06877f, 19f}, //38  '&'
		{0.07126f, 03f}, //39  '''
		{0.07622f, 06f}, //40  '('
		{0.08202f, 07f}, //41  ')'
		{0.09114f, 11f}, //42  '*'
		{0.10356f, 15f}, //43  '+'
		{0.10854f, 06f}, //44  ','
		{0.11599f, 09f}, //45  '-'
		{0.11930f, 04f}, //46  '.'
		{0.13007f, 13f}, //47  '/'
		{0.14250f, 15f}, //48  '0'
		{0.15327f, 13f}, //49  '1'
		{0.16404f, 13f}, //50  '2'
		{0.17481f, 13f}, //51  '3'
		{0.18724f, 15f}, //52  '4'
		{0.19801f, 13f}, //53  '5'
		{0.20961f, 14f}, //54  '6'
		{0.22121f, 14f}, //55  '7'
		{0.23364f, 15f}, //56  '8'
		{0.24524f, 14f}, //57  '9'
		{0.24855f, 04f}, //58  ':'
		{0.24352f, 06f}, //59  ';'
		{0.26512f, 14f}, //60  '<'
		{0.27672f, 14f}, //61  '='
		{0.28832f, 14f}, //62  '>'
		{0.29909f, 13f}, //63  '?'
		{0.31814f, 23f}, //64  '@'
		{0.33306f, 18f}, //65  'A'
		{0.34466f, 14f}, //66  'B'
		{0.35791f, 16f}, //67  'C'
		{0.37200f, 17f}, //68  'D'
		{0.38194f, 12f}, //69  'E'
		{0.39105f, 11f}, //70  'F'
		{0.40514f, 17f}, //71  'G'
		{0.41839f, 16f}, //72  'H'
		{0.42171f, 04f}, //73  'I'
		{0.42833f, 08f}, //74  'J'
		{0.43993f, 14f}, //75  'K'
		{0.44988f, 12f}, //76  'L'
		{0.46893f, 23f}, //77  'M'
		{0.48219f, 16f}, //78  'N'
		{0.49710f, 18f}, //79  'O'
		{0.50787f, 13f}, //80  'P'
		{0.52610f, 22f}, //81  'Q'
		{0.53770f, 14f}, //82  'R'
		{0.54930f, 14f}, //83  'S'
		{0.56255f, 16f}, //84  'T'
		{0.57581f, 16f}, //85  'U'
		{0.59072f, 18f}, //86  'V'
		{0.61309f, 27f}, //87  'W'
		{0.62635f, 16f}, //88  'X'
		{0.63960f, 16f}, //89  'Y'
		{0.65120f, 14f}, //90  'Z'
		{0.65700f, 07f}, //91  '['
		{0.66777f, 13f}, //92  '\'
		{0.67274f, 06f}, //93  ']'
		{0.68351f, 13f}, //94  '^'
		{0.69759f, 17f}, //95  '_'
		{0.70256f, 06f}, //96  '`'
		{0.71251f, 12f}, //97  'a'
		{0.72411f, 14f}, //98  'b'
		{0.73405f, 12f}, //99  'c'
		{0.74482f, 13f}, //100 'd'
		{0.75642f, 14f}, //101 'e'
		{0.76553f, 11f}, //102 'f'
		{0.77713f, 14f}, //103 'g'
		{0.78790f, 13f}, //104 'h'
		{0.79039f, 03f}, //105 'i'
		{0.79619f, 07f}, //106 'j'
		{0.80613f, 12f}, //107 'k'
		{0.80944f, 04f}, //108 'l'
		{0.82684f, 21f}, //109 'm'
		{0.83761f, 13f}, //110 'n'
		{0.85004f, 15f}, //111 'o'
		{0.86081f, 14f}, //112 'p'
		{0.87324f, 14f}, //113 'q'
		{0.88152f, 10f}, //114 'r'
		{0.89064f, 11f}, //115 's'
		{0.89892f, 10f}, //116 't'
		{0.90969f, 13f}, //117 'u'
		{0.92192f, 14f}, //118 'v'
		{0.93952f, 22f}, //119 'w'
		{0.95029f, 13f}, //120 'x'
		{0.96189f, 14f}, //121 'y'
		{0.97100f, 11f}, //122 'z'
		{0.97763f, 08f}, //123 '{'
		{0.98012f, 03f}, //124 '|'
		{0.98757f, 09f}, //125 '}'
		{1.00000f, 15f}, //126 '~'
	};
	private static final short MAX_CHAR_INDEX = 94;

	public GLText()
	{
		this("", 0, 0, new float[]{0.0f, 0.0f, 0.0f, 1.0f});
	}

	public GLText(final String text, final float x, final float y)
	{
		this(text, x, y, new float[]{0.0f, 0.0f, 0.0f, 1.0f});
	}

	public GLText(final String text, final float x, final float y, final float Color[])
	{
		mText = text;
		mCursorPosX = mPosX = x;
		mPosY = y;
		mColor = Color;

		mTexID = GLSurf.mTexMng.loadTexture(R.drawable.font_calibri);
		
		mProgram = GLShader.sProgram_Texture.mProgram;
		mPosHandle = GLShader.sProgram_Texture.mPosHandle;
		mColorHandle = GLShader.sProgram_Texture.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Texture.mMatrixHandle;
		mTexCoordHandle = GLShader.sProgram_Texture.mTexCoordHandle;
		mSamplerHandle = GLShader.sProgram_Texture.mSamplerHandle;

		bVisible = !mText.isEmpty();

		vecuv = new float[text.length() * 16];
		for (int i = 0; i < text.length(); i++)
		{
			//uv[i * 8] = 0.0f;
			vecuv[i * 16 + 3] = 1.0f;
			//uv[i * 8 + 2] = 1.0f;
			vecuv[i * 16 + 7] = 1.0f;
			//uv[i * 8 + 4] = 0.0f;
			vecuv[i * 16 + 11] = 0.0f;
			//uv[i * 8 + 6] = 1.0f;
			vecuv[i * 16 + 15] = 0.0f;
		}
		drawOrder = new short[text.length() * 6];
		for (int i = 0; i < text.length(); i++)
		{
			drawOrder[i * 6] = (short)(i * 4);
			drawOrder[i * 6 + 1] = (short)(i * 4 + 1);
			drawOrder[i * 6 + 2] = (short)(i * 4 + 3);
			drawOrder[i * 6 + 3] = (short)(i * 4);
			drawOrder[i * 6 + 4] = (short)(i * 4 + 3);
			drawOrder[i * 6 + 5] = (short)(i * 4 + 2);
		}
		color = new float[text.length() * 16];
		for (int i = 0; i < text.length() * 4; i++)
		{
			color[i * 4] = mColor[0];
			color[i * 4 + 1] = mColor[1];
			color[i * 4 + 2] = mColor[2];
			color[i * 4 + 3] = mColor[3];
		}

        ByteBuffer bb = ByteBuffer.allocateDirect(drawOrder.length * 2);
        bb.order(ByteOrder.nativeOrder());
        drawListBuffer = bb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        bb = ByteBuffer.allocateDirect(vecuv.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexuvBuffer = bb.asFloatBuffer();
        vertexuvBuffer.put(vecuv);
        vertexuvBuffer.position(0);

		bb = ByteBuffer.allocateDirect(color.length * 4);
		bb.order(ByteOrder.nativeOrder());
		colorBuffer = bb.asFloatBuffer();
		colorBuffer.put(color);
		colorBuffer.position(0);

		GLES20.glGenBuffers(3, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexuvBuffer.capacity() * 4, vertexuvBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * 4, colorBuffer, GLES20.GL_STATIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity() * 2, drawListBuffer, GLES20.GL_STATIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

		vertexuvBuffer.clear();
		colorBuffer.clear();
		drawListBuffer.clear();

		mtrx = GLSurf.mCamera.getRotatedMatrix(rot, getLenght(), getHeight());
		Update();
	}
	
	public void Destroy()
	{
		vecuv = null;
		drawOrder = null;
		color = null;
		vertexuvBuffer = null;
		colorBuffer = null;
		drawListBuffer = null;

		GLES20.glDeleteBuffers(buffers.length, buffers, 0);
		GLSurf.mTexMng.ReleaseTexture(mTexID);
	}

	public final void setText(final String text)
	{
		if (mText.equals(text))
			return;

		if (text.length() == mText.length())
		{
			changeText(text);
			return;
		}

		if (text.isEmpty())
			setVisibility(false);
		else
			setVisibility(true);

		mText = text;

		vecuv = null;
		drawOrder = null;
		color = null;

		vecuv = new float[text.length() * 16];
		for (int i = 0; i < text.length(); i++)
		{
			//uv[i * 8] = 0.0f;
			vecuv[i * 16 + 3] = 1.0f;
			//uv[i * 8 + 2] = 1.0f;
			vecuv[i * 16 + 7] = 1.0f;
			//uv[i * 8 + 4] = 0.0f;
			vecuv[i * 16 + 11] = 0.0f;
			//uv[i * 8 + 6] = 1.0f;
			vecuv[i * 16 + 15] = 0.0f;
		}
		drawOrder = new short[text.length() * 6];
		for (int i = 0; i < text.length(); i++)
		{
			drawOrder[i * 6] = (short)(i * 4);
			drawOrder[i * 6 + 1] = (short)(i * 4 + 1);
			drawOrder[i * 6 + 2] = (short)(i * 4 + 3);
			drawOrder[i * 6 + 3] = (short)(i * 4);
			drawOrder[i * 6 + 4] = (short)(i * 4 + 3);
			drawOrder[i * 6 + 5] = (short)(i * 4 + 2);
		}
		color = new float[text.length() * 16];
		for (int i = 0; i < text.length() * 4; i++)
		{
			color[i * 4] = mColor[0];
			color[i * 4 + 1] = mColor[1];
			color[i * 4 + 2] = mColor[2];
			color[i * 4 + 3] = mColor[3];
		}

        ByteBuffer bb = ByteBuffer.allocateDirect(drawOrder.length * 2);
        bb.order(ByteOrder.nativeOrder());
        drawListBuffer = bb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        bb = ByteBuffer.allocateDirect(vecuv.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexuvBuffer = bb.asFloatBuffer();
        /*vertexBuffer.put(vec);
		 vertexBuffer.position(0);*/

		bb = ByteBuffer.allocateDirect(color.length * 4);
		bb.order(ByteOrder.nativeOrder());
		colorBuffer = bb.asFloatBuffer();
		colorBuffer.put(color);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexuvBuffer.capacity() * 4, vertexuvBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * 4, colorBuffer, GLES20.GL_STATIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity() * 2, drawListBuffer, GLES20.GL_STATIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

		colorBuffer.clear();
		drawListBuffer.clear();

		Update();
	}

	public final void setVisibility(final boolean visible)
	{
		bVisible = visible;
	}

	private final void changeText(final String text) //better performance.. (charcount const!!)
	{
		mText = text;
		Update();
	}

	public void setPos(final float x, final float y)
	{
		mCursorPosX = mPosX = x;
		mPosY = y;
		Update();
	}

	public final void scaleText(final float f)
	{
		mScale = f;
		Update();
	}

	public final void setColor(final float r, final float g, final float b, final float a)
	{
		mColor[0] = r;
		mColor[1] = g;
		mColor[2] = b;
		mColor[3] = a;
		for (int i = 0; i < mText.length() * 4; i++)
		{
			color[i * 4] = r;
			color[i * 4 + 1] = g;
			color[i * 4 + 2] = b;
			color[i * 4 + 3] = a;
		}
		colorBuffer.put(color);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, colorBuffer.capacity() * 4, colorBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		colorBuffer.clear();
	}

	private final void Update()
	{
		for (int i = 0; i < mText.length(); i++)
			UpdateVecandUV(mText.charAt(i), i);

		vertexuvBuffer.put(vecuv);
		vertexuvBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, vertexuvBuffer.capacity() * 4, vertexuvBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		vertexuvBuffer.clear();

		mCursorPosX = mPosX;
	}

	private final void UpdateVecandUV(final char c, final int idx)
	{
		final int index = c - 32;

		if (index > 0 && index <= MAX_CHAR_INDEX)
		{
			vecuv[idx * 16] = mCursorPosX;
			vecuv[idx * 16 + 1] = mPosY + 30.0f * mScale;
			vecuv[idx * 16 + 2] = mCharInfo[index - 1][0];
			vecuv[idx * 16 + 4] = mCursorPosX + mCharInfo[index][1] * mScale;
			vecuv[idx * 16 + 5] = mPosY + 30.0f * mScale;
			vecuv[idx * 16 + 6] = mCharInfo[index][0];
			vecuv[idx * 16 + 8] = mCursorPosX;
			vecuv[idx * 16 + 9] = mPosY;
			vecuv[idx * 16 + 10] = mCharInfo[index - 1][0];
			vecuv[idx * 16 + 12] = mCursorPosX + mCharInfo[index][1] * mScale;
			vecuv[idx * 16 + 13] = mPosY;
			vecuv[idx * 16 + 14] = mCharInfo[index][0];

			mCursorPosX += (mCharInfo[index][1] + 4) * mScale; //4px space
		}
		else if(c == '\n') //=> \n\r
		{
			mPosY += 30 * mScale;// \n
			mCursorPosX = mPosX; // \r
		}
		else
			mCursorPosX += 8 * mScale; //space
	}

	public void Render()
	{
		if (!bVisible)
			return;

		GLES20.glUseProgram(mProgram);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, stride, 0); //vertexBuffer);

		GLES20.glEnableVertexAttribArray(mTexCoordHandle);
		GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, stride, GLHelp.COORDS_PER_VERTEX * GLHelp.BYTES_PER_FLOAT);//uvBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);//colorBuffer);

		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, mtrx, 0);
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexID);
		GLES20.glUniform1i(mSamplerHandle, 0);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, 0);//drawListBuffer);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mTexCoordHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}

	public final void rotate(final float degree)
	{
		rot += degree;
		mtrx = GLSurf.mCamera.getRotatedMatrix(rot, getLenght(), getHeight());
	}

	public final void setdegree(final float degree)
	{
		rot = degree;
		mtrx = GLSurf.mCamera.getRotatedMatrix(rot, getLenght(), getHeight());
	}

	public final float getdegree()
	{
		return rot;
	}

	public final float getHeight()
	{
		return 30.0f * mScale;
	}

	public final float getLenght()
	{
		if (mText.isEmpty())
			return 0;

		return vecuv[vecuv.length - 4] - vecuv[0];
	}
}
