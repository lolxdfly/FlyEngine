package com.lolxdfly.flyengine.GLElements.GLUIElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLTexture;

public class GLUIGamePad extends GLTexture
{
	public final GLTexture dot;
	private final float[] dotreset;
	private final float[] dotpos;
	private final float rad;
	private final float dotrad;

	private boolean catched = false;

	public GLUIGamePad(final int bgRID, final int dotRID, final float radius, final float dotrad)
	{
		super(bgRID, false);

		dot = new GLTexture(dotRID, false);
		rad = radius;
		this.dotrad = dotrad;

		dotreset = new float[]{
			200 + radius - dotrad,
			400 + radius - dotrad
		};
		dotpos = new float[]{
			0,
			0
		};
		dot.setPos(dotreset[0], dotreset[1], dotreset[0] + dotrad * 2, dotreset[1] + dotrad * 2);
		setPos(200, 400, 200 + radius * 2, 400 + radius * 2);
	}

	public final boolean onTouch(final MotionEvent event)
	{
		float x = event.getX();
		float y = event.getY();

		if (event.getAction() == MotionEvent.ACTION_DOWN && checkPtCollision(x, y))
			catched = true;

		if (event.getAction() == MotionEvent.ACTION_MOVE && catched)
		{
			x -= dotrad;
			y -= dotrad;

			x -= dotreset[0];
			y -= dotreset[1];

			final double len = Math.sqrt(x * x + y * y);
			if (len > rad)
			{
				x /= len;
				x *= rad;
				y /= len;
				y *= rad;
			}

			dotpos[0] = x / rad;
			dotpos[1] = -y / rad;

			x += dotreset[0];
			y += dotreset[1];

			dot.setPos(x, y);

			return true;
		}

		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			catched = false;
			dot.setPos(dotreset[0], dotreset[1]);
			dotpos[0] = 0;
			dotpos[1] = 0;
		}

		return false;
	}

	public final float getXAcc()
	{
		return dotpos[0];
	}

	public final float getYAcc()
	{
		return dotpos[1];
	}

	@Override
	public final void Render()
	{
		super.Render();
		dot.Render();
	}

	@Override
	public final void setPos(final float x, final float y)
	{
		super.setPos(x, y);

		dotreset[0] = x + rad - dotrad;
		dotreset[1] = y + rad - dotrad;
		dot.setPos(dotreset[0], dotreset[1]);
	}

	@Override
	public final void Move(final float x, final float y)
	{
		super.Move(x, y);
		dot.Move(x, y);

		dotreset[0] += x;
		dotreset[1] += y;
	}

	@Override
	public void Destroy()
	{
		dot.Destroy();
		super.Destroy();
	}

}
