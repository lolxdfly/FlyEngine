package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLRect extends GLRenderObjEx
{
	private FloatBuffer vertexBuffer;
	//private ShortBuffer drawListBuffer;
	private FloatBuffer colorBuffer;
	
	public static final int mStdIdxBuff[] = new int[1];
	private static ByteBuffer bbvertexbuff;
	private static ByteBuffer bbcolorbuff;
	
	private boolean bIsUi;

	public static final byte drawOrder[] = { 0, 1, 3, 0, 3, 2};
	/*
	 2---3
	 |  /|
	 |/  |
	 0---1
	 */
	
	public GLRect(final boolean _bUsedInRenderMng)
	{
		mRect = new float[]
		{
			0.0f,	100.0f,	// left bottom 		0
			100.0f, 100.0f,	// right bottom 	1
			0.0f,   0.0f,	// left top 		2
			100.0f,   0.0f	// right top 		3
		};
		
		mColor = new float[]
		{
			1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f
		};
		
		bUsedInRenderMng = _bUsedInRenderMng;

		if (bUsedInRenderMng)
			return;

		// initialize vertex byte buffer for shape coordinates
		/*ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		 b.order(ByteOrder.nativeOrder());*/
		vertexBuffer = bbvertexbuff.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		/* // initialize byte buffer for the draw list
		 b = ByteBuffer.allocateDirect(drawOrder.length * 2);
		 b.order(ByteOrder.nativeOrder());
		 drawListBuffer = b.asShortBuffer();
		 drawListBuffer.put(drawOrder);
		 drawListBuffer.position(0);*/

		/*b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		 b.order(ByteOrder.nativeOrder());*/
		colorBuffer = bbcolorbuff.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		///VBO///
		buffers = new int[2];
		GLES20.glGenBuffers(2, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		///

		vertexBuffer.clear();
		colorBuffer.clear();

		mProgram = GLShader.sProgram_Obj.mProgram;
		mPosHandle = GLShader.sProgram_Obj.mPosHandle;
		mColorHandle = GLShader.sProgram_Obj.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Obj.mMatrixHandle;
	}
	
	public static final void genStdBuffers()
	{
		//Vertex
		bbvertexbuff = ByteBuffer.allocateDirect(8 * GLHelp.BYTES_PER_FLOAT);
		bbvertexbuff.order(ByteOrder.nativeOrder());

		//Color
		bbcolorbuff = ByteBuffer.allocateDirect(16 * GLHelp.BYTES_PER_FLOAT);
		bbcolorbuff.order(ByteOrder.nativeOrder());

		//Index
		final ByteBuffer drawListBuffer = ByteBuffer.allocateDirect(GLRect.drawOrder.length);
        drawListBuffer.order(ByteOrder.nativeOrder());
        drawListBuffer.put(GLRect.drawOrder);
        drawListBuffer.position(0);

		//move into vram
		GLES20.glGenBuffers(1, mStdIdxBuff, 0);						
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mStdIdxBuff[0]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity(), drawListBuffer, GLES20.GL_STATIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	
	public static final void ReleaseStdIdxBuff()
	{
		//release std index buffer
		GLES20.glDeleteBuffers(1, mStdIdxBuff, 0);
	}

	@Override
	public void Render()
	{
		if (!bVisible)
			return;

        GLES20.glUseProgram(mProgram);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);//vertexBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);//colorBuffer);

		if (!bIsUi)
      		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxMVP/*mtrx*/, 0);
		else
			GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxUI, 0);

		RenderInline();

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mStdIdxBuff[0]);
		//GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, drawOrder.length);
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_BYTE, 0);

		// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}

	public void RenderInline()
	{}

	public void setRect(final RectF r)
	{
		mRect[0] = r.left;
		mRect[1] = r.bottom;
		mRect[2] = r.right;
		mRect[3] = r.bottom;
		mRect[4] = r.left;
		mRect[5] = r.top;
		mRect[6] = r.right;
		mRect[7] = r.top;

		UpdateVertice(true);
	}

	public void setPos(final float left, final float top, final float right, final float bottom)
	{
		mRect[0] = left;
		mRect[1] = bottom;
		mRect[2] = right;
		mRect[3] = bottom;
		mRect[4] = left;
		mRect[5] = top;
		mRect[6] = right;
		mRect[7] = top;

		UpdateVertice(true);
	}

	public void Move(final float x, final float y)
	{
		final float tmp = rot;
		if (rot != 0)
			setdegree(0);

		mRect[0] += x;
		mRect[1] += y;
		mRect[2] += x;
		mRect[3] += y;
		mRect[4] += x;
		mRect[5] += y;
		mRect[6] += x;
		mRect[7] += y;

		calccenter();
		if (tmp != 0)
		{
			setdegree(tmp);
		}

		UpdateVertice(false);
	}

	public void setPos(final float x, final float y)
	{
		Move(x - mRect[0], y - mRect[5]);
	}

	public void MoveToPoint(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[5]);

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / dy1) * speed;
		float dy = (dy1 / dx1) * speed;

		if (dx > (int)GLCamera.mVirtualScreenWidth || dy > (int)GLCamera.mVirtualScreenHeight)
			return;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[5]);
		//double d = Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / (dx1 + dy1)) * speed;
		float dy = (dy1 / (dx1 + dy1)) * speed;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final RectF getRect()
	{
		//ignore rotation
		return new RectF(mRect[0], 
						 mRect[5], 
						 mRect[2],
						 mRect[1]);
	}

	public final void UpdateVertice(final boolean brot)
	{
		super.UpdateVertice();
		if (!checkRender())
			return;

		if (brot)
		{
			calccenter();
			rot = 0;
		}

		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		vertexBuffer.clear();
	}

	@Override
	public final void UpdateColors()
	{
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		colorBuffer.clear();
	}

	private void calccenter()
	{
		center[0] = mRect[0] + ((mRect[2] - mRect[0]) / 2f);
		center[1] = mRect[5] + ((mRect[1] - mRect[5]) / 2f);
	}

	@Override
	protected final void rotatePos(final float degree)
	{
		final float points[][] = new float[4][2];
		points[0] = new float[]{mRect[0], mRect[1]};
		points[1] = new float[]{mRect[2], mRect[3]};
		points[2] = new float[]{mRect[4], mRect[5]};
		points[3] = new float[]{mRect[6], mRect[7]};

		GLCamera.rotateVec_point(points[0], center, degree);
		GLCamera.rotateVec_point(points[1], center, degree);
		GLCamera.rotateVec_point(points[2], center, degree);
		GLCamera.rotateVec_point(points[3], center, degree);

		mRect[0] = points[0][0];
		mRect[1] = points[0][1];
		mRect[2] = points[1][0];
		mRect[3] = points[1][1];
		mRect[4] = points[2][0];
		mRect[5] = points[2][1];
		mRect[6] = points[3][0];
		mRect[7] = points[3][1];

		UpdateVertice(false);
	}

	public final boolean checkCollision(final GLRect glo)
	{
		if (rot == 0 && glo.getdegree() == 0)
			return RectF.intersects(getRect(), glo.getRect());

		//SAT
		final boolean collideaxe[] = new boolean[]{false, false, false, false};

		for (short s = 0; s < 4; s++)
		{
			//get axis
			float x[];
			if (s == 0)
				x = new float[]{mRect[2] - mRect[0], mRect[3] - mRect[1]};
			else if (s == 1)
				x = new float[]{mRect[6] - mRect[2], mRect[7] - mRect[3]};
			else if (s == 2)
				x = new float[]{glo.mRect[2] - glo.mRect[0], glo.mRect[3] - glo.mRect[1]};
			else
				x = new float[]{glo.mRect[6] - glo.mRect[2], glo.mRect[7] - glo.mRect[3]};

			//get axis-lenght
			float lenx = (float)Math.sqrt(x[0] * x[0] + x[1] * x[1]);
			if (x[s % 2] < 0)
				lenx *= (-1);

			final float points[][] = new float[4][2];
			final float plenght[] = new float[4];
			float min=0, max=0;

			for (short s1 = 0; s1 < 4; s1++)
			{
				//project points on axis
				if (s == 0)
					points[s1] = GLCamera.orthProject(x, new float[]{glo.mRect[s1 * 2] - mRect[0], glo.mRect[(s1 * 2) + 1] - mRect[1]});
				else if (s == 1)
					points[s1] = GLCamera.orthProject(x, new float[]{glo.mRect[s1 * 2] - mRect[2], glo.mRect[(s1 * 2) + 1] - mRect[3]});
				else if (s == 2)
					points[s1] = GLCamera.orthProject(x, new float[]{mRect[s1 * 2] - glo.mRect[0], mRect[(s1 * 2) + 1] - glo.mRect[1]});
				else
					points[s1] = GLCamera.orthProject(x, new float[]{mRect[s1 * 2] - glo.mRect[2], mRect[(s1 * 2) + 1] - glo.mRect[3]});

				//get lenght
				plenght[s1] = (float)Math.sqrt(points[s1][0] * points[s1][0] + points[s1][1] * points[s1][1]);
				if (points[s1][s % 2] < 0)
					plenght[s1] *= (-1);

				//get shape start/end
				if (s1 == 0)
				{
					min = plenght[0];
					max = plenght[0];
				}
				else
				{
					if (plenght[s1] < min)
						min = plenght[s1];
					else if (plenght[s1] > max)
						max = plenght[s1];
				}
			}

			//test overlap
			if (
				min <= lenx && min >= 0 ||
				max <= lenx && max >= 0 ||
				max >= lenx && min <= 0
				)
				collideaxe[s] = true;
		}

		return collideaxe[0] && collideaxe[1] &&
			collideaxe[2] && collideaxe[3];
	}

	@Override
	public final boolean checkPtCollision(final float x, final float y)
	{
		return RectF.intersects(getRect(), new RectF(x, y, x + 1, y + 1));
	}

	@Override
	protected Region getRgn()
	{
		final Path p = new Path();
		p.moveTo(mRect[0], mRect[1]);
		p.lineTo(mRect[4], mRect[5]);
		p.lineTo(mRect[6], mRect[7]);
		p.lineTo(mRect[2], mRect[3]);
		p.close();
		final Region r = new Region();
		r.setPath(p, new Region(0, 0, (int)GLCamera.mVirtualScreenWidth, (int)GLCamera.mVirtualScreenHeight));
		return r;
	}
	
	protected final void setUI(final boolean b)
	{
		bIsUi = b;
	}

}
