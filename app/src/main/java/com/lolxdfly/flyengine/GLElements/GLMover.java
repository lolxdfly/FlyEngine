package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.RectF;

import com.lolxdfly.flyengine.GLMain.GLSurf;

public class GLMover extends GLAnimation
{
	private Bitmap mtex; //for pixel perfect intersect
	public double mtime;

	public GLMover(final int RID)
	{
		super(RID, 0, 0, -1);
	}

	public final void SetBitmap(final int RID)
	{
		//for pixel perfect intersect
		mtex = GLSurf.mTexMng.getBitmap(RID);
		rescaleBitmap();
	}

	@Override
	public void Destroy()
	{
		if(mtex != null)
			mtex.recycle();
	}

	public final Bitmap getBitmap()
	{
		return mtex;
	}

	private final void rescaleBitmap()
	{
		if (mtex == null)
			return;

		final int width = (int)(mRect[2] - mRect[0]);
		final int height = (int)(mRect[1] - mRect[5]);

		if (width > 0 && height > 0)
			mtex = Bitmap.createScaledBitmap(mtex, width, height, false);
	}

	@Override
	public final void setRect(final RectF r)
	{
		super.setRect(r);
		rescaleBitmap();
	}

	public final boolean CheckPixelCollision(final GLMover obj)
	{
		final RectF r1 = getRect();
		final RectF r2 = obj.getRect();

		if (RectF.intersects(r1, r2))
		{
			final Bitmap bmp2 = obj.getBitmap();
			final RectF collrect = new RectF(Math.max(r1.left, r2.left), 
											 Math.max(r1.top, r2.top), 
											 Math.min(r1.right, r2.right), 
											 Math.min(r1.bottom, r2.bottom));

			for (float i = collrect.left; i < collrect.right; i++)
			{
				for (float j = collrect.top; j < collrect.bottom; j++)
				{
					if (mtex.getPixel((int)(i - r1.left), (int)(j - r1.top)) != Color.TRANSPARENT &&
						bmp2.getPixel((int)(i - r2.left), (int)(j - r2.top)) != Color.TRANSPARENT)
						return true;
				}
			}
		}
		return false;
	}
}
