package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Bitmap;
import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLTexture extends GLRect
{
	private FloatBuffer uvBuffer;
	private final int uvbuff[] = new int[1];
	protected int mTexCoordHandle;
	protected int mSamplerHandle;
	public int mTextureID;

	// Create our UV coordinates.
	public final float uvs[] =
	{
		0.0f, 1.0f, // 0
		1.0f, 1.0f, // 1
		0.0f, 0.0f, // 2
		1.0f, 0.0f  // 3
	};

	public GLTexture(final int RID, final boolean _bUsedInRenderMng)
	{
		super(_bUsedInRenderMng);

		if (bUsedInRenderMng)
			return;

		if(RID != -1)
			mTextureID = GLSurf.mTexMng.loadTexture(RID);
		
		// The texture buffer
		final ByteBuffer uvb = ByteBuffer.allocateDirect(uvs.length * 4);
		uvb.order(ByteOrder.nativeOrder());
		uvBuffer = uvb.asFloatBuffer();
		uvBuffer.put(uvs);
		uvBuffer.position(0);

		GLES20.glGenBuffers(1, uvbuff, 0);						
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, uvbuff[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, uvBuffer.capacity() * 4, uvBuffer, GLES20.GL_DYNAMIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		uvBuffer.clear();

		mProgram = GLShader.sProgram_Texture.mProgram;
		mPosHandle = GLShader.sProgram_Texture.mPosHandle;
		mColorHandle = GLShader.sProgram_Texture.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Texture.mMatrixHandle;
		mTexCoordHandle = GLShader.sProgram_Texture.mTexCoordHandle;
		mSamplerHandle = GLShader.sProgram_Texture.mSamplerHandle;
	}

	@Override
	public void Destroy()
	{
		super.Destroy();
		if (!bUsedInRenderMng)
			GLES20.glDeleteBuffers(uvbuff.length, uvbuff, 0);
		GLSurf.mTexMng.ReleaseTexture(mTextureID);
	}

	@Override
	public void RenderInline()
	{
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, uvbuff[0]);
		GLES20.glEnableVertexAttribArray(mTexCoordHandle);
		GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, 0); //uvBuffer);
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureID);
		GLES20.glUniform1i(mSamplerHandle, 0);
	}

	@Override
	public void Render()
	{
		super.Render();
		//GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		if (bVisible)
			GLES20.glDisableVertexAttribArray(mTexCoordHandle);
	}

	public void setTexture(final int RID)
	{
		GLSurf.mTexMng.ReleaseTexture(mTextureID);
		mTextureID = GLSurf.mTexMng.loadTexture(RID);
	}
	
	public void setTexture(final Bitmap bmp)
	{
		GLSurf.mTexMng.ReleaseTexture(mTextureID);
		mTextureID = GLSurf.mTexMng.loadTexture(bmp);
	}

	public final void setUV(final float _uvs[])
	{
		System.arraycopy(_uvs, 0, uvs, 0, 8);
		if (!bUsedInRenderMng)
			updateUVBuff();
	}

	public final void updateUVBuff()
	{
		uvBuffer.put(uvs);
		uvBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, uvbuff[0]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, uvBuffer.capacity() * 4, uvBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		uvBuffer.clear();
	}
}
