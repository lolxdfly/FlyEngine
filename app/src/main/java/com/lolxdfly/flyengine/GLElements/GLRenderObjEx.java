package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

public abstract class GLRenderObjEx extends GLRenderObj
{
	public float center[] = new float[]{0, 0};
	protected float rot = 0.0f;
	
	public final void rotate(final float degree)
	{
		rot = degree;
		rotatePos(degree);
	}

	public final void setdegree(final float degree)
	{
		if ((degree - rot) != 0)
		{
			rotatePos(degree - rot);
			rot = degree;
		}
	}

	public final float getdegree()
	{
		return rot;
	}

	protected abstract void rotatePos(final float degree);
}
