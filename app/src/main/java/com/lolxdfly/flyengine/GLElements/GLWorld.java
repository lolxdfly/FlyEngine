package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Rect;
import android.graphics.RectF;

import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLRenderMngs.GLStaticTileRenderMng;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public final class GLWorld
{
	private GLStaticTileRenderMng g_Renderer;
	private final HashMap<Integer, TileDef> g_TileDefs;
	private final ArrayList<GLAnimation> g_Anims;
	private final ArrayList<WorldObj> g_WldObjs;

	public final class WorldObj
	{
		public float rect[];
		public int dwObjFlags;

		//ObjFlags:
		public final static int CollisionObj 	= 1 << 0;
		public final static int Walkdefault 	= 1 << 1;
		public final static int Walkwater 		= 1 << 2;
		//...
	}
	
	public final class TileDef
	{
		public boolean isAnim;
		public int dwObjFlags;
		
		// if !isAnim
		public float uvs[];
		
		// if isAnim
		public int RID;
		public float animwidth;
		public float animheight;
		public int dtime;
	}

	public GLWorld()
	{
		g_TileDefs = new HashMap<Integer, TileDef>();
		g_Anims = new ArrayList<GLAnimation>();
		g_WldObjs = new ArrayList<WorldObj>();
	}

	private final void loadTileDef(final InputStream is)
	{
		final Scanner scanner = new Scanner(is);
		
		// get Tilemap RID
		g_Renderer = new GLStaticTileRenderMng(GLSurf.mHelper.getRID("drawable", scanner.next()));
		
		int id = 0;
		while(scanner.hasNext())
		{
			id = scanner.nextInt();
			final TileDef td = new TileDef();
			
			td.isAnim = scanner.nextBoolean();
			if(!td.isAnim) // read TextureTile def
			{
				td.uvs = new float[]{
					scanner.nextFloat(),
					scanner.nextFloat(),
					scanner.nextFloat(),
					scanner.nextFloat()
				};
			}
			else // read animation def
			{
				td.RID = GLSurf.mHelper.getRID("drawable", scanner.next());
				td.animwidth = scanner.nextFloat();
				td.animheight = scanner.nextFloat();
				td.dtime = scanner.nextInt();
			}
			
			td.dwObjFlags = scanner.nextInt();
			g_TileDefs.put(id, td);
		}
	}
	
	public final void initByFile(final InputStream is)
	{
		//test.wld
		//"test.def"
		//ID pos_left pos_top pos_right pos_bottom
		
		//test.tdef
		//TileMap
		//ID bAnim(false) uv_left uv_top uv_right uv_bottom dwObjFlags
		//ID bAnim(true) RID glani.width glani.height glani.dtime dwObjFlags
		
		final Scanner scanner = new Scanner(is);
		final float pos[] = new float[4];
		
		// check for tiledef include
		final String tileDefInclude = scanner.next();
		try
		{
			loadTileDef(GLSurf.mHelper.getContext().getAssets().open(tileDefInclude.replace("\"", "")));
		}
		catch (final IOException e)
		{}

		while(scanner.hasNext())
		{
			// get TileDef
			final TileDef td = g_TileDefs.get(scanner.nextInt());
			
			// check for Anim
			if (!td.isAnim)
			{
				// add texture
				for (short s = 0; s < 4; s++)
					pos[s] = scanner.nextFloat();
				addTexture(td, pos);
			}
			else
			{
				// add animation
				final GLAnimation glani = new GLAnimation(td.RID, td.animwidth, td.animheight, td.dtime);
				glani.setPos(scanner.nextFloat(), scanner.nextFloat(), scanner.nextFloat(), scanner.nextFloat());
				addAnimation(td, glani);
			}
		}
		
		scanner.close();
		g_TileDefs.clear(); // we could keep this for chunk loading
		
		g_Renderer.prepareDraw();
	}

	public final void setShader(final GLShader.GLShaderProgram program)
	{
		//may use postprocessor instead!
		g_Renderer.setShader(program);
		for (final GLAnimation glani : g_Anims)
			glani.setShader(program);
	}

	public final void Render()
	{
		g_Renderer.Render();
		for (final GLAnimation glani : g_Anims)
			glani.Render();
	}

	public final void Update(final long elapsed)
	{
		for (final GLAnimation glani : g_Anims)
			glani.Update(elapsed);
	}

	public final void Destroy()
	{
		g_Renderer.Destroy();
		for (GLAnimation glani : g_Anims)
			glani.Destroy();
		g_Anims.clear();
		g_WldObjs.clear();
	}

	public final void addAnimation(final TileDef td, final GLAnimation glo)
	{
		g_Anims.add(glo);
		final WorldObj wldo = new WorldObj();
		wldo.dwObjFlags = td.dwObjFlags;
		final RectF r = glo.getRect();
		wldo.rect = new float[]{r.left, r.top, r.right, r.bottom};
		g_WldObjs.add(wldo);
	}

	public final void addTexture(final TileDef td, final float[] pos)
	{
		final GLTexture glo = new GLTexture(0, true);
		glo.setPos(pos[0], pos[1], pos[2], pos[3]);
		g_Renderer.addObj(glo, td.uvs[0], td.uvs[1], td.uvs[2], td.uvs[3]);
		final WorldObj wldo = new WorldObj();
		wldo.dwObjFlags = td.dwObjFlags;
		wldo.rect = new float[]{pos[0], pos[1], pos[2], pos[3]};
		g_WldObjs.add(wldo);
	}

	private final boolean intersectsWldObj(final float rect[], final float pos[])
	{
		return new Rect((int)rect[0], (int)rect[1], (int)rect[2], (int)rect[3]).intersects((int)pos[0], (int)pos[1], (int)pos[2], (int)pos[3]);
		/*return (
		 pos[3] >= rect[0] && pos[3] <= rect[2] ||
		 pos[1] >= rect[0] && pos[1] <= rect[2] ||
		 pos[0] >= rect[1] && pos[0] <= rect[3] ||
		 pos[2] >= rect[1] && pos[2] <= rect[3]
		 );*/
	}

	public final boolean checkWorldIntersection(final float pos[])
	{
		//Note: Rotated World does not work in this case!
		for (final WorldObj wobj: g_WldObjs)
		{
			if (((wobj.dwObjFlags & WorldObj.CollisionObj) != 0) && intersectsWldObj(wobj.rect, pos))
				return true;
		}
		return false;
	}
}
