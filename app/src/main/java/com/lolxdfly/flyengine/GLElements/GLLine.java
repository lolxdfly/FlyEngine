package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Path;
import android.graphics.Region;
import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public final class GLLine extends GLRenderObjEx
{
	private FloatBuffer vertexBuffer;
	//private ShortBuffer drawListBuffer;
	private FloatBuffer colorBuffer;

	public static final byte drawOrder[] = {0, 1};

	private static final int mStdIdxBuff[] = new int[1];
	private static ByteBuffer bbvertexbuff;
	private static ByteBuffer bbcolorbuff;

	public GLLine(final boolean _bUsedInRenderMng)
	{
		mRect = new float[]
		{
			0.0f, 0.0f,
			100.0f, 100.0f
		};

		mColor = new float[]
		{
			1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f
		};

		bUsedInRenderMng = _bUsedInRenderMng;
		if (bUsedInRenderMng)
			return;

		// initialize vertex byte buffer for shape coordinates
		/*ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		 b.order(ByteOrder.nativeOrder());*/
		vertexBuffer = bbvertexbuff.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		/*b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		 b.order(ByteOrder.nativeOrder());*/
		colorBuffer = bbcolorbuff.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		///VBO///
		buffers = new int[2];
		GLES20.glGenBuffers(2, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		///

		vertexBuffer.clear();
		colorBuffer.clear();

		mProgram = GLShader.sProgram_Obj.mProgram;
		mPosHandle = GLShader.sProgram_Obj.mPosHandle;
		mColorHandle = GLShader.sProgram_Obj.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Obj.mMatrixHandle;
	}

	public static final void genStdBuffers()
	{
		//Vertex
		bbvertexbuff = ByteBuffer.allocateDirect(4 * GLHelp.BYTES_PER_FLOAT);
		bbvertexbuff.order(ByteOrder.nativeOrder());

		//Color
		bbcolorbuff = ByteBuffer.allocateDirect(8 * GLHelp.BYTES_PER_FLOAT);
		bbcolorbuff.order(ByteOrder.nativeOrder());

		//Index
		final ByteBuffer drawListBuffer = ByteBuffer.allocateDirect(drawOrder.length);
        drawListBuffer.order(ByteOrder.nativeOrder());
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

		//move into vram
		GLES20.glGenBuffers(1, mStdIdxBuff, 0);						
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mStdIdxBuff[0]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity(), drawListBuffer, GLES20.GL_STATIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	public static final void ReleaseStdIdxBuff()
	{
		//release std index buffer
		GLES20.glDeleteBuffers(1, mStdIdxBuff, 0);
	}

	@Override
	public final void Render()
	{
		if (!bVisible)
			return;

        GLES20.glUseProgram(mProgram);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);//vertexBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);//colorBuffer);

		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxMVP/*mtrx*/, 0);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mStdIdxBuff[0]);
		//GLES20.glDrawArrays(GLES20.GL_LINES, 0, drawOrder.length);
		GLES20.glDrawElements(GLES20.GL_LINES, drawOrder.length, GLES20.GL_UNSIGNED_BYTE, 0);

		// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}

	@Override
	protected final void rotatePos(final float degree)
	{
		for (int i = 0; i < mRect.length; i += 2)
		{
			final float points[] = new float[]{mRect[i], mRect[i + 1]};
			GLCamera.rotateVec_point(points, center, degree);
			mRect[i] = points[0];
			mRect[i + 1] = points[1];
		}
		UpdateVertice(false);
	}

	public final void setPos(final float x1, final float y1, final float x2, final float y2)
	{
		mRect[0] = x1;
		mRect[1] = y1;
		mRect[2] = x2;
		mRect[3] = y2;
		UpdateVertice(true);
	}

	public final void setPos(final float x, final float y)
	{
		Move(x - mRect[0], y - mRect[1]);
	}

	public final void Move(final float dx, final float dy)
	{
		final float tmp = rot;
		if (rot != 0)
			setdegree(0);
		
		mRect[0] += dx;
		mRect[1] += dy;
		mRect[2] += dx;
		mRect[3] += dy;
		
		center = GLCamera.getCenter(mRect);
		if (tmp != 0)
		{
			setdegree(tmp);
		}
		
		UpdateVertice(false);
	}

	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[1]);

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / dy1) * speed;
		float dy = (dy1 / dx1) * speed;

		if (dx > (int)GLCamera.mVirtualScreenWidth || dy > (int)GLCamera.mVirtualScreenHeight)
			return;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[1]);
		//double d = Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / (dx1 + dy1)) * speed;
		float dy = (dy1 / (dx1 + dy1)) * speed;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void UpdateVertice(final boolean brot)
	{
		super.UpdateVertice();
		if (!checkRender())
			return;

		if (brot)
		{
			center = GLCamera.getCenter(mRect);
			rot = 0;
		}

		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		vertexBuffer.clear();
	}

	@Override
	protected final void UpdateColors()
	{
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		colorBuffer.clear();
	}

	@Override
	public final boolean checkPtCollision(final float x, final float y)
	{
		return (
			//dst sp
			Math.sqrt(
			Math.pow(x - mRect[0], 2) + 
			Math.pow(y - mRect[1], 2)
		) +

			//dst pe
			Math.sqrt(
			Math.pow(mRect[2] - x, 2) + 
			Math.pow(mRect[3] - y, 2)
		) ==

			//dst se
			Math.sqrt(
			Math.pow(mRect[2] - mRect[0], 2) + 
			Math.pow(mRect[3] - mRect[1], 2)
		)
			);
	}

	@Override
	protected final Region getRgn()
	{
		final Path p = new Path();
		p.moveTo(mRect[0], mRect[1]);
		p.lineTo(mRect[2], mRect[3]);
		p.lineTo(mRect[2] + 1, mRect[3] + 1);
		p.lineTo(mRect[0] + 1, mRect[1] + 1);
		p.close();
		final Region r = new Region();
		r.setPath(p, new Region(0, 0, (int)GLCamera.mVirtualScreenWidth, (int)GLCamera.mVirtualScreenHeight));
		return r;
	}
}
