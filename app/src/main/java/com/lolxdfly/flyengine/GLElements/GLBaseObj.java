package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import com.lolxdfly.flyengine.GLUtils.GLShader;

public abstract class GLBaseObj
{
	protected boolean bVisible = true;
	protected int mProgram;

	protected GLBaseObj()
	{
		mProgram = GLShader.sProgram_Obj.mProgram;
	}
	
	public final void setShader(final GLShader.GLShaderProgram program)
	{
		// the new shader has to keep the attrib locations of the old!
		mProgram = program.mProgram;
	}

	public final void setVisible(final boolean bVis)
	{
		bVisible = bVis;
	}

	public final boolean isVisible()
	{
		return bVisible;
	}
}
