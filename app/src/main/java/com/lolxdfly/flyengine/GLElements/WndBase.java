package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLMain.GLSurf;

public abstract class WndBase
{
	private boolean mPause;
	private boolean mVisible;

	public WndBase()
	{
		mPause = false;
		mVisible = true;
	}

	public abstract void onTouch(final MotionEvent event);

	public void Destroy()
	{
		GLSurf.mWndMng.deleteWnd(this);
	}

	public abstract void onDraw();

	public abstract void Process(final long elapsed);

	public final void setPaused(final boolean pause)
	{
		mPause = pause;
	}

	public final boolean isPaused()
	{
		return mPause;
	}

	public final void setVisible(final boolean visible)
	{
		mVisible = visible;
	}

	public final boolean isVisible()
	{
		return mVisible;
	}
}
