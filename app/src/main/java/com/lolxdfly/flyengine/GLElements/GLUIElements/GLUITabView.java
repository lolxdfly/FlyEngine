package com.lolxdfly.flyengine.GLElements.GLUIElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.R;

public final class GLUITabView
{
	private final GLUIButton mBtnTabs[];
	private final WndBase mWnds[];
	private int mTab = 0;
	
	public GLUITabView(final int nTabs, final float btnheight, final WndBase wnds[])
	{
		mBtnTabs = new GLUIButton[nTabs];
		mWnds = wnds;
		
		final float width = GLCamera.mVirtualScreenWidth / nTabs;
		for(short i = 0; i < nTabs; i++)
		{
			mBtnTabs[i] = new GLUIButton(R.drawable.btndefault);
			mBtnTabs[i].setPos(width * i, GLCamera.mVirtualScreenHeight - btnheight, width * (i + 1), GLCamera.mVirtualScreenHeight);
			//mBtnTabs[i].setToggle(true);
		}
	}
	
	public final void Render()
	{
		mWnds[mTab].onDraw();
		for(final GLUIButton btn : mBtnTabs)
			btn.Render();
	}

	public final boolean isClicked(final MotionEvent event)
	{
		boolean ret = false;
		for(short i = 0; i < mBtnTabs.length; i++)
		{
			ret = mBtnTabs[i].isClicked(event);
			if(ret)
			{
				mTab = i;
				/*for(short s = 0; s < mBtnTabs.length; s++)
				{
					if(s == i)
						continue;
						
					mBtnTabs[s].setMode(0);
				}*/
				return true;
			}
		}
		return ret;
	}

	public final boolean onTouch(final MotionEvent event)
	{
		for(final WndBase wnd : mWnds)
			wnd.onTouch(event);
		return isClicked(event);
	}

	public final int getSelectedTab()
	{
		return mTab;
	}

	public final void Destroy()
	{
		for(short i = 0; i < mBtnTabs.length; i++)
		{
			mBtnTabs[i].Destroy();
			mWnds[i].Destroy();
		}
	}
}
