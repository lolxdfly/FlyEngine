package com.lolxdfly.flyengine.GLElements.GLUIElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.RectF;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLMain.GLSurf;

public final class GLUITextbox extends GLTexture
{
	private final GLText mText;
	private String sText;
	private boolean update = false;
	private final float TextXOffset;
	private boolean hasHintText = false;

	public GLUITextbox(final int RID, final float textxoffset)
	{
		super(RID, false);
		mText = new GLText();
		mText.scaleText(1.5f);
		TextXOffset = textxoffset;
		sText = "";
		setUI(true);
	}

	@Override
	public final void Render()
	{
		super.Render();
		if (update)
		{
			mText.setText(sText);
			update = false;

			if (hasHintText)
				mText.setColor(0.2f, 0.2f, 0.2f, 0.6f);
			else
				mText.setColor(0f, 0f, 0f, 1f);
		}
		mText.Render();
	}

	public final boolean onTouch(final MotionEvent event)
	{
		if (checkPtCollision(event.getX(), event.getY()) && event.getAction() != MotionEvent.ACTION_UP)
		{
			if (hasHintText)
				setText("");

			GLSurf.mHelper.openEditText(this);
			//Update();
			return true;
		}

		return false;
	}

	@Override
	public final void setPos(final float x, final float y)
	{
		super.setPos(x, y);
		Update();
	}

	@Override
	public final void setRect(final RectF r)
	{
		super.setRect(r);
		Update();
	}

	@Override
	public final void setPos(final float left, final float top, final float right, final float bottom)
	{
		super.setPos(left, top, right, bottom);
		Update();
	}

	@Override
	public final void Move(final float x, final float y)
	{
		super.Move(x, y);
		Update();
	}

	@Override
	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		super.MoveToPoint(x, y, speed);
		Update();
	}

	private final void Update()
	{
		mText.setPos(mRect[0] + TextXOffset, mRect[5] + ((mRect[1] - mRect[5]) / 2.0f) - (mText.getHeight() / 2.0f));
	}

	public final void setText(final String text)
	{
		sText = text;
		update = true;
		//mText.setText(sText);
		hasHintText = false;
	}

	public final void setHintTex(final String text)
	{
		sText = text;
		update = true;
		hasHintText = true;
	}

	public final String getText()
	{
		return sText;
	}

	@Override
	public final void Destroy()
	{
		super.Destroy();
		mText.Destroy();
	}
}
