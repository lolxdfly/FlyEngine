package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Bitmap;
import android.os.Parcel;

import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class SaveGameBase
{
	private final String mName;
	private String mDesc;
	private Bitmap mCover;
	private final byte[] mData;

	public SaveGameBase(final String name, final int size)
	{
		mName = name;
		mData = new byte[size];
	}
	
	public SaveGameBase(final String name, final byte data[])
	{
		mName = name;
		mData = data;
	}

	public final String getName()
	{
		return mName;
	}

	public final byte[] getData()
	{
		return mData;
	}
	
	public final void setCoverImage(final Bitmap bmp)
	{
		mCover = bmp;
	}
	
	public final void setDescription(final String desc)
	{
		mDesc = desc;
	}

	public final SnapshotMetadataChange getMetaData()
	{
		return new SnapshotMetadataChange.Builder()
            .setCoverImage(mCover)
            .setDescription(mDesc)
			.build();
	}

	public final void exportSave(final String sPath)
	{
		try
		{
			final File path = new File(sPath);
			if (!path.exists())
				path.mkdirs();
			final File file = new File(sPath);
			final FileOutputStream fOut = new FileOutputStream(file);
			fOut.write(mData, 0, mData.length);
			fOut.flush();
			fOut.close();
		}
		catch (final Exception e) 
		{}
	}

	public final void importSave(final String sPath)
	{
		try
		{
			final File path = new File(sPath);
			if (!path.exists())
				path.mkdirs();
			final File file = new File(sPath);
			final FileInputStream fIn  = new FileInputStream(file);
			fIn.read(mData, 0, mData.length);
			fIn.close();
		}
		catch (final Exception e) 
		{}
	}

	/*//Example:
	 public final void saveInventory(final byte[] inv)
	 {
	 for (int i = 0; i < inv.length; i++)
	 mData[POS_INV + i] = inv[i];
	 }

	 public final void getnventoty(byte[] inv)
	 {
	 for (int i = 0; i < mData.length; i++)
	 inv[i] = mData[POS_INV + i];
	 }*/

}
