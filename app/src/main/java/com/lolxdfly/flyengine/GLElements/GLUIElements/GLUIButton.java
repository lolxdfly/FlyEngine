package com.lolxdfly.flyengine.GLElements.GLUIElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.RectF;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;

public class GLUIButton extends GLTexture
{
	// normal|clicked|disabled
	private static final float f = 1.0f / 3.0f;
	protected int nMode = 0;
	private boolean buseText = false;
	private GLText mText;
	private boolean bisToggle = false;
	private boolean bToggleState = false;
	private boolean washit = false; // was hit

	public GLUIButton(final int RID)
	{
		super(RID, false);
		setMode(0);
		setUI(true);
	}

	public final void setEnabled(final boolean enabled)
	{
		if (!enabled)
			setMode(2);
		else
			setMode(0);
	}

	public final void setToggle(final boolean toggle)
	{
		bisToggle = toggle;
	}

	public boolean isClicked(final MotionEvent event)
	{
		if (nMode == 2)
			return false;

		if(!bisToggle)
			setMode(0);
		
		for (int i = 0; i < event.getPointerCount(); i++)
		{
			final int pointerIndex = event.findPointerIndex(event.getPointerId(i));
			//float x = event.getX(pointerIndex);
			//float y = event.getY(pointerIndex);

			if (checkPtCollision(event.getX(pointerIndex), event.getY(pointerIndex)))
			{
				if (washit && event.getAction() == MotionEvent.ACTION_UP && !bisToggle)
				{
					washit = false;
					setMode(0);
					return true;
				}
				else
				{
					if (bisToggle && event.getAction() == MotionEvent.ACTION_UP)
					{
						bToggleState = !bToggleState;
						setMode(bToggleState ? 1 : 0);
						return true;
					}
					washit = true;
					setMode(1);
					//return false;
				}
			}
			//else if (!bisToggle)
				//setMode(0);
		}

		return false;
	}

	protected final void setMode(final int n)
	{
		nMode = n;
		uvs[0] = uvs[4] = n * f;
		uvs[2] = uvs[6] = (n + 1) * f;
		updateUVBuff();
	}

	public final void setText(final String text)
	{
		if (!buseText)
		{
			buseText = true;
			mText = new GLText(text, 0, 0);
			mText.scaleText(1.5f);
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
			return;
		}
		mText.setText(text);
		mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	@Override
	public void Render()
	{
		super.Render();
		if (buseText)
			mText.Render();
	}

	@Override
	public void Destroy()
	{
		super.Destroy();
		if (buseText)
			mText.Destroy();
	}

	@Override
	public final void setPos(final float x, final float y)
	{
		super.setPos(x, y);
		if (buseText)
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	@Override
	public final void setRect(final RectF r)
	{
		super.setRect(r);
		if (buseText)
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	@Override
	public final void setPos(final float left, final float top, final float right, final float bottom)
	{
		super.setPos(left, top, right, bottom);
		if (buseText)
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	@Override
	public final void Move(final float x, final float y)
	{
		super.Move(x, y);
		if (buseText)
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	@Override
	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		super.MoveToPoint(x, y, speed);
		if (buseText)
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	@Override
	public final void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		super.MoveToPoint_smooth(x, y, speed);
		if (buseText)
			mText.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mText.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mText.getHeight() / 2.0f));
	}

	public final void setFont(final GLText glt)
	{
		if (!buseText)
			return;
		mText = glt;
	}
}
