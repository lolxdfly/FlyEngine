package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

public final class GLParticle extends GLPoint
{
	private final float deltapos[] = new float[2];
	private final float deltacolor[] = new float[4];
	private final int tlifeend;
	private int tlife = 0;
	
	public boolean isAlive = true;
	
	public GLParticle(final float x, final float y, final int lifetime)
	{
		super(true);
		setPos(x, y);
		tlifeend = lifetime;
	}
	
	public final void setDeltaPos(final float dx, final float dy)
	{
		deltapos[0] = dx;
		deltapos[1] = dy;
	}
	
	public final void setDeltaColor(final float r, final float g, final float b, final float a)
	{
		deltacolor[0] = r;
		deltacolor[1] = g;
		deltacolor[2] = b;
		deltacolor[3] = a;
	}
	
	public final void Process(final long time)
	{
		tlife += time;
		if(tlifeend <= tlife)
			isAlive = false;
			
		Move(deltapos[0], deltapos[1]);
		for(int i = 0; i < 4; i++)
			mColor[i] += deltacolor[i];
	}
	
	/*public final void ProcessRender()
	{
		Move(deltapos[0], deltapos[1]);
		
		for(int i = 0; i < 4; i++)
			mColor[i] += deltacolor[i];
	}*/
}
