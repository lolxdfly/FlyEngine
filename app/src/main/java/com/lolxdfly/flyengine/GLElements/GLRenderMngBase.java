package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;

public abstract class GLRenderMngBase extends GLBaseObj
{
	protected float mtrx[];
	private float rot = 0.0f;
	private final float cpos[] = {GLCamera.mVirtualScreenWidth / 2, GLCamera.mVirtualScreenHeight / 2};

	public void Render()
	{
		if (!bVisible)
			return;
		mtrx = GLSurf.mCamera.getRotatedMatrix(rot, cpos[0] * 2, cpos[1] * 2);
	}
	
	public final void rotate(final float degree, final float x, final float y)
	{
		cpos[0] = x;
		cpos[1] = y;
		rot += degree;
	}

	public final void setdegree(final float degree, final float x, final float y)
	{
		cpos[0] = x;
		cpos[1] = y;
		rot = degree;
	}

	public final float getdegree()
	{
		return rot;
	}
}
