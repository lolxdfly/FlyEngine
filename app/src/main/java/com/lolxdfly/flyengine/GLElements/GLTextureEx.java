package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

public final class GLTextureEx extends GLTexture
{
	private final int mLightPosHandle;
	private final int mLightColorHandle;
	private final int mCamPosHandle;

	private final float mLightPos[] = new float[GLHelp.MAX_LIGHTS * 3];
	private final float mLightColor[] = new float[GLHelp.MAX_LIGHTS * 3];
	private final float mLightFinalPos[] = new float[GLHelp.MAX_LIGHTS * 3];
	private short sLiD = -1;
	
	private final int mNormalTextureID;
	private final int mNormalSamplerHandle;
	
	public GLTextureEx(final int RID, final int normalRID, final boolean _bUsedInRenderMng)
	{
		super(RID, _bUsedInRenderMng);
		
		mNormalTextureID = GLSurf.mTexMng.loadTexture(normalRID);
		
		mProgram = GLShader.sProgram_Phong.mProgram;
		mPosHandle = GLShader.sProgram_Phong.mPosHandle;
		mColorHandle = GLShader.sProgram_Phong.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Phong.mMatrixHandle;
		mTexCoordHandle = GLShader.sProgram_Phong.mTexCoordHandle;
		mSamplerHandle = GLShader.sProgram_Phong.mSamplerHandle;
		mLightPosHandle = GLShader.sProgram_Phong.mLightPosHandle;
		mLightColorHandle = GLShader.sProgram_Phong.mLightColorHandle;
		mCamPosHandle = GLShader.sProgram_Phong.mCamPosHandle;
		mNormalSamplerHandle = GLShader.sProgram_Phong.mNormalSamplerHandle;
	}
	
	public final float[] getLightInfo()
	{
		return mLightPos;
	}

	@Override
	public final void RenderInline()
	{
		super.RenderInline();
		
		GLES20.glUniform3fv(mLightPosHandle, GLHelp.MAX_LIGHTS, mLightFinalPos, 0);
		GLES20.glUniform3fv(mLightColorHandle, GLHelp.MAX_LIGHTS, mLightColor, 0);
		final float camPos[] = new float[]{
			GLSurf.mCamera.getX() + GLSurf.mCamera.mScreenWidth / 2,
			GLSurf.mCamera.getY() + GLSurf.mCamera.mScreenHeight / 2,
			200
		};
		GLES20.glUniform3fv(mCamPosHandle, 1, camPos, 0);
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mNormalTextureID);
		GLES20.glUniform1i(mNormalSamplerHandle, 1);
	}

	public final void addLight(final int lID, final float pos[], final float color[])
	{
		if (lID > sLiD)
		{
			addLight(pos, color);
			return;
		}

		GLSurf.mCamera.transformVec(pos); // transform 2D part
		mLightFinalPos[lID * 3] = pos[0];
		mLightFinalPos[lID * 3 + 1] = pos[1];
		mLightFinalPos[lID * 3 + 2] = pos[2];
		mLightColor[lID * 3] = color[0];
		mLightColor[lID * 3 + 1] = color[1];
		mLightColor[lID * 3 + 2] = color[2];
	}

	public final void addLight(final float pos[], final float color[])
	{
		if (sLiD == GLHelp.MAX_LIGHTS) return;

		sLiD++;
		GLSurf.mCamera.transformVec(pos); // transform 2D part
		mLightFinalPos[sLiD * 3] = pos[0];
		mLightFinalPos[sLiD * 3 + 1] = pos[1];
		mLightFinalPos[sLiD * 3 + 2] = pos[2];
		mLightColor[sLiD * 3] = color[0];
		mLightColor[sLiD * 3 + 1] = color[1];
		mLightColor[sLiD * 3 + 2] = color[2];
	}

	public final void turnoffLight(final int lID)
	{
		mLightFinalPos[lID * 3 + 2] = 0;
	}

	public final void turnonLight(final int lID)
	{
		mLightFinalPos[lID * 3 + 2] = mLightPos[lID * 3 + 2];
	}

	@Override
	public void Destroy()
	{
		GLSurf.mTexMng.ReleaseTexture(mNormalTextureID);
		super.Destroy();
	}
	
}
