package com.lolxdfly.flyengine.GLElements.GLUIElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.RectF;

import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;

public final class GLUIProgressbar extends GLTexture
{
	private GLText mPercent;
	private float fPercent = 0.0f;
	private final float lr[] = new float[2];
	private final boolean buseText;

	public GLUIProgressbar(final int RID, final boolean usetext)
	{
		super(RID, false);
		buseText = usetext;
		if (buseText)
		{
			mPercent = new GLText();
			mPercent.scaleText(1.5f);
		}
		setUI(true);
	}

	private final void setPercentMode()
	{
		//set % uv
		uvs[1] = uvs[3] = 0.5f;
		uvs[2] = uvs[6] = (fPercent / 100.0f);
		uvs[5] = uvs[7] = 0.0f;
		updateUVBuff();

		mRect[2] = mRect[6] = lr[0] + (fPercent / 100.0f) * (lr[1] - lr[0]);
		UpdateVertice(false);
	}

	private final void setOverlayMode()
	{
		//reset uv
		uvs[1] = uvs[3] = 1.0f;
		uvs[2] = uvs[6] = 1.0f;
		uvs[5] = uvs[7] = 0.5f;
		updateUVBuff();

		mRect[2] = mRect[6] = lr[1];
		UpdateVertice(false);
	}

	@Override
	public final void Render()
	{
		setPercentMode();
		super.Render();

		setOverlayMode();
		super.Render();

		if (buseText)
			mPercent.Render();
	}

	public final void setPercent(final float percent)
	{
		if (percent > 100.0f || percent < 0.0f)
			return;

		fPercent = percent;
		if (buseText)
		{
			mPercent.setText(percent + "%");
			mPercent.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2f) - (mPercent.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mPercent.getHeight() / 2.0f));
		}
	}

	@Override
	public final void setPos(final float x, final float y)
	{
		super.setPos(x, y);
		Update();
	}

	@Override
	public final void setRect(final RectF r)
	{
		super.setRect(r);
		Update();
	}

	@Override
	public final void setPos(final float left, final float top, final float right, final float bottom)
	{
		super.setPos(left, top, right, bottom);
		Update();
	}

	@Override
	public final void Move(final float x, final float y)
	{
		super.Move(x, y);
		Update();
	}

	@Override
	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		super.MoveToPoint(x, y, speed);
		Update();
	}

	@Override
	public final void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		super.MoveToPoint_smooth(x, y, speed);
		Update();
	}

	public final void setFont(final GLText glt)
	{
		if (!buseText)
			return;
		mPercent = glt;
	}

	private final void Update()
	{
		if (buseText)
			mPercent.setPos(mRect[0] + ((mRect[2] - mRect[0]) / 2.0f) - (mPercent.getLenght() / 2.0f), (mRect[5] + ((mRect[1] - mRect[5]) / 2f)) - (mPercent.getHeight() / 2.0f));

		lr[0] = mRect[0];
		lr[1] = mRect[2];
	}

	@Override
	public final void Destroy()
	{
		super.Destroy();
		if (buseText)
			mPercent.Destroy();
	}
}
