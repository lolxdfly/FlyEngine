package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLRenderMngs.GLDynamicObjRenderMng;

import java.util.concurrent.ArrayBlockingQueue;

public final class GLFire extends GLRenderObjEx
{
	private final GLDynamicObjRenderMng<GLParticle> mParticles;
	private final Thread spawner;
	private final ArrayBlockingQueue<GLParticle> toSpawn;
	private int nParticles = 0;

	public GLFire()
	{
		mRect = new float[]{
			500,
			500
		};
		mParticles = new GLDynamicObjRenderMng<GLParticle>(GLES20.GL_POINTS);
		toSpawn = new ArrayBlockingQueue<GLParticle>(10000);
		spawner = new Thread()
		{
			public void run()
			{
				while (!interrupted())
				{
					if (nParticles < 10000 && toSpawn.size() < 10000)
					{
						final GLParticle glp = new GLParticle(mRect[0], mRect[1], 5000);
						glp.setDeltaPos(
							((float)Math.random() * 0.75f) - 0.375f,
							((float)Math.random() * 5.0f) - 5.5f);
						glp.setColor((float)Math.random() + 0.5f, 0, 0, 1);
						glp.setDeltaColor(-0.01f, 0, 0, -0.005f);
						toSpawn.add(glp);
						//mParticles.addObj(glp);
						//nParticles++;
					}
					try
					{
						Thread.sleep(0, 100000);
					}
					catch (InterruptedException e)
					{}
				}
			}
		};
		spawner.start();
	}

	@Override
	protected final void rotatePos(final float degree)
	{
		mParticles.rotate(degree, mRect[0], mRect[1]);
	}

	@Override
	public final void Render()
	{
		mParticles.Render();
	}

	public final int getParticleCount()
	{
		return nParticles;
	}

	public final void Process(final long time)
	{
		for (int i = 0; i < nParticles; i++)
		{
			final GLParticle glp = mParticles.getObj(i);
			if (!glp.isAlive)
			{
				glp.Destroy();
				mParticles.removeObj(glp);
				i--;
				nParticles--;
			}
			else
				glp.Process(time);
		}

		if (!toSpawn.isEmpty())
		{
			for (final GLParticle glp : toSpawn)
			{
				mParticles.addObj(glp);
				nParticles++;
			}
			toSpawn.clear();
		}

		//if (nParticles < 750)
		/*for (int i = 0; i < 65; i++)
		 {
		 final GLParticle glp = new GLParticle(mRect[0], mRect[1], 5000);
		 glp.setDeltaPos(
		 ((float)Math.random() * 0.75f) - 0.375f,
		 ((float)Math.random() * 5.0f) - 5.5f);
		 glp.setColor((float)Math.random() + 0.5f, 0, 0, 1);
		 glp.setDeltaColor(-0.01f, 0, 0, -0.005f);
		 mParticles.addObj(glp);
		 nParticles++;
		 }*/
	}

	public final void setPos(final float x, final float y)
	{
		mRect[0] = x;
		mRect[1] = y;
	}

	public final void Destroy()
	{
		//spawner.interrupt();
		mParticles.Destroy();
	}

	@Override
	protected final void UpdateColors()
	{
		// TODO: Implement this method
	}
}
