package com.lolxdfly.flyengine.GLElements;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Path;
import android.graphics.Region;
import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public final class GLCircle extends GLRenderObjEx
{
	private FloatBuffer vertexBuffer;
	private ShortBuffer drawListBuffer;
	private FloatBuffer colorBuffer;

	public short drawOrder[];

	private float radius = 1.0f;

	public GLCircle(final boolean _bUsedInRenderMng)
	{
		mRect = new float[4 * 2 + 4];

		//set center
		center[0] = mRect[0] = 100;
		center[1] = mRect[1] = 100;

		int idx = 2;
		for (int i = 0; i < 4; i++)
		{
			final double angle = 2 * Math.PI * i / 4;
			mRect[idx++] = 100 + (float)Math.sin(angle) * 50;
			mRect[idx++] = 100 + (float)Math.cos(angle) * 50;
		}

		mRect[mRect.length - 2] = mRect[2];
		mRect[mRect.length - 1] = mRect[3];

		drawOrder = new short[mRect.length / 2];
		for (short s = 0; s < drawOrder.length; s++)
			drawOrder[s] = s;

		mColor = new float[mRect.length * 2];
		for (int i = 0; i < mColor.length; i++)
			mColor[i] = 1.0f;

		bUsedInRenderMng = _bUsedInRenderMng;
		if (bUsedInRenderMng)
			return;

		// initialize vertex byte buffer for shape coordinates
		ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		vertexBuffer = b.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		// initialize byte buffer for the draw list
		b = ByteBuffer.allocateDirect(drawOrder.length * 2);
		b.order(ByteOrder.nativeOrder());
		drawListBuffer = b.asShortBuffer();
		drawListBuffer.put(drawOrder);
		drawListBuffer.position(0);

		b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		colorBuffer = b.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		///VBO///
		buffers = new int[3];
		GLES20.glGenBuffers(3, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity() * 2, drawListBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		///

		vertexBuffer.clear();
		drawListBuffer.clear();
		colorBuffer.clear();

		mProgram = GLShader.sProgram_Obj.mProgram;
		mPosHandle = GLShader.sProgram_Obj.mPosHandle;
		mColorHandle = GLShader.sProgram_Obj.mColorHandle;
		mMatrixHandle = GLShader.sProgram_Obj.mMatrixHandle;
	}

	public final void setCircle(final float x, final float y, final float r, final int precision)
	{
		if (precision != (mRect.length / 2) - 4)
		{
			final float tmpcolor[] = new float[4];
			for (short s = 0; s < 4; s++)
				tmpcolor[s] = mColor[s];

			mRect = new float[precision * 2 + 4];
			drawOrder = new short[mRect.length / 2];
			mColor = new float[mRect.length * 2];

			for (short s = 0; s < drawOrder.length; s++)
				drawOrder[s] = s;

			setColor(tmpcolor[0], tmpcolor[1], tmpcolor[2], tmpcolor[3]);

			if (!bUsedInRenderMng)
			{
				final ByteBuffer b = ByteBuffer.allocateDirect(drawOrder.length * 2);
				b.order(ByteOrder.nativeOrder());
				drawListBuffer = b.asShortBuffer();
				drawListBuffer.put(drawOrder);
				drawListBuffer.position(0);

				GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
				//GLES20.glBufferSubData(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0, drawListBuffer.capacity() * 2, drawListBuffer);
				GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity() * 2, drawListBuffer, GLES20.GL_DYNAMIC_DRAW);
				GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

				drawListBuffer.clear();
			}
		}

		radius = r;
		mRect[0] = x;
		mRect[1] = y;

		int idx = 2;
		for (int i = 0; i < precision; i++)
		{
			final double angle = 2 * Math.PI * i / precision;
			mRect[idx++] = x + (float)Math.sin(angle) * r;
			mRect[idx++] = y + (float)Math.cos(angle) * r;
		}

		mRect[mRect.length - 2] = mRect[2];
		mRect[mRect.length - 1] = mRect[3];

		UpdateVertice(true);
	}

	@Override
	public final void Render()
	{
		if (!bVisible)
			return;

        GLES20.glUseProgram(mProgram);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);//vertexBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);//colorBuffer);

		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxMVP/*mtrx*/, 0);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
		//GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, drawOrder.length);
		GLES20.glDrawElements(GLES20.GL_TRIANGLE_FAN, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, 0);

		// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}


	public final void Move(final float x, final float y)
	{
		final float tmp = rot;
		if (rot != 0)
			setdegree(0);

		for (int i = 0; i < mRect.length; i += 2)
		{
			mRect[i] += x;
			mRect[i + 1] += y;
		}

		center[0] = mRect[0];
		center[1] = mRect[1];
		if (tmp != 0)
		{
			setdegree(tmp);
		}

		UpdateVertice(false);
	}

	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[5]);

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / dy1) * speed;
		float dy = (dy1 / dx1) * speed;

		if (dx > (int)GLCamera.mVirtualScreenWidth || dy > (int)GLCamera.mVirtualScreenHeight)
			return;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void MoveToPoint_smooth(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - mRect[0]);
		final float dy1 = Math.abs(y - mRect[5]);
		//double d = Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / (dx1 + dy1)) * speed;
		float dy = (dy1 / (dx1 + dy1)) * speed;

		if (x <= mRect[0])
			dx = -dx;

		if (y <= mRect[1])
			dy = -dy;

		Move(dx, dy);
	}

	public final void UpdateVertice(final boolean brot)
	{
		super.UpdateVertice();
		if (!checkRender())
			return;

		if (brot)
		{
			rot = 0;
			center[0] = mRect[0];
			center[1] = mRect[1];
		}

		final ByteBuffer b = ByteBuffer.allocateDirect(mRect.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		vertexBuffer = b.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		//GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, vBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vBuffer);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_DYNAMIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		vertexBuffer.clear();
	}

	@Override
	public final void UpdateColors()
	{
		if (bUsedInRenderMng)
			return;

		final ByteBuffer b = ByteBuffer.allocateDirect(mColor.length * GLHelp.BYTES_PER_FLOAT);
		b.order(ByteOrder.nativeOrder());
		colorBuffer = b.asFloatBuffer();
		colorBuffer.put(mColor);
		colorBuffer.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[1]);
		//GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colorBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, colorBuffer, GLES20.GL_DYNAMIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		colorBuffer.clear();
	}

	@Override
	protected final void rotatePos(final float degree)
	{
		for (int i = 0; i < mRect.length; i += 2)
		{
			final float points[] = new float[]{mRect[i], mRect[i + 1]};
			GLCamera.rotateVec_point(points, center, degree);
			mRect[i] = points[0];
			mRect[i + 1] = points[1];
		}
		UpdateVertice(false);
	}

	@Override
	protected final Region getRgn()
	{
		final Path p = new Path();
		p.addCircle(mRect[0], mRect[1], radius, Path.Direction.CW);
		final Region r = new Region();
		r.setPath(p, new Region(0, 0, (int)GLCamera.mVirtualScreenWidth, (int)GLCamera.mVirtualScreenHeight));
		return r;
	}
}
