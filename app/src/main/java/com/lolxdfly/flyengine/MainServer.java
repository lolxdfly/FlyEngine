package com.lolxdfly.flyengine;

import com.lolxdfly.flyengine.GLServer.GLConsole;
import com.lolxdfly.flyengine.GLServer.GLSocket;
import com.lolxdfly.flyengine.GLUtils.GLNet.GLSync;

public final class MainServer {

	public static GLSocket mSock;
	public static GLConsole mConsole;
	public static GLSync gls;
	
	public static final void main(final String[] args)
	{
		//init
		mConsole = new GLConsole();
		gls = new GLSync();
		
		//port
		int port = 8000;
		if(args.length != 0)
			port = Integer.parseInt(args[0]);
		
		//create Sock
		mSock = new GLSocket(port);
		//final GLObj glo = new GLObj(false);
		//GLSocket.mServerSync.put(GLSync.nid_test, glo);
		
		//Shutdown Hook
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        mSock.stopServer();
		    }
		});
	}
}
