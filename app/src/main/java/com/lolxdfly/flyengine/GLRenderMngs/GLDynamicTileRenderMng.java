package com.lolxdfly.flyengine.GLRenderMngs;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.Bitmap;
import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.GLRenderMngBase;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

public final class GLDynamicTileRenderMng extends GLRenderMngBase
{
	private final int buffers[] = new int[2];
	private float mVBO[];
	private short mIBO[];
	private int mIBOlength = 0;
	private final ArrayList<GLTexture> mObjs;
	private static final int stride = (GLHelp.COORDS_PER_VERTEX + GLHelp.COLORS_PER_VERTEX + GLHelp.TEX_COORDS_PER_VERTEX) * GLHelp.BYTES_PER_FLOAT;
	private boolean update = false;
	private Thread tAtlasLoader;
	private FloatBuffer mBufferVBO;

	//Shader-Handles
	private final int mPosHandle;
	private final int mMatrixHandle;
	private final int mColorHandle;
	private final int mTexCoordHandle;
	private final int mSamplerHandle;

	//Atlas
	private short mTileTextureWidth;
	private short mTileTextureHeight;
	private int mTextureID;

	public GLDynamicTileRenderMng(final int RID)
	{
		mObjs = new ArrayList<GLTexture>();
		
		mProgram = GLShader.sProgram_Texture.mProgram;
		mPosHandle = GLShader.sProgram_Texture.mPosHandle;
		mMatrixHandle = GLShader.sProgram_Texture.mMatrixHandle;
		mColorHandle = GLShader.sProgram_Texture.mColorHandle;
		mTexCoordHandle = GLShader.sProgram_Texture.mTexCoordHandle;
		mSamplerHandle = GLShader.sProgram_Texture.mSamplerHandle;
		
		//init Atlas
		setTileAtlas(RID);

		//init buffers
		final float tmp[] = new float[] {0};
		ByteBuffer b = ByteBuffer.allocateDirect(tmp.length * GLHelp.BYTES_PER_FLOAT);
        b.order(ByteOrder.nativeOrder());
		final FloatBuffer mBufferVBO = b.asFloatBuffer();
        mBufferVBO.put(tmp);
        mBufferVBO.position(0);

		final short tmp2[] = new short[] {0};
		b = ByteBuffer.allocateDirect(tmp2.length * GLHelp.BYTES_PER_SHORT);
        b.order(ByteOrder.nativeOrder());
        final ShortBuffer drawListBuffer = b.asShortBuffer();
        drawListBuffer.put(tmp2);
        drawListBuffer.position(0);

		GLES20.glGenBuffers(2, buffers, 0);						

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, mBufferVBO.capacity() * GLHelp.BYTES_PER_FLOAT, mBufferVBO, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity() * GLHelp.BYTES_PER_SHORT, drawListBuffer, GLES20.GL_DYNAMIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		mBufferVBO.clear();
		drawListBuffer.clear();
	}

	public final void setTileAtlas(final int RID)
	{
		mTextureID = GLSurf.mTexMng.loadTexture(RID);
		tAtlasLoader = new Thread()
		{
			public final void run()
			{
				final Bitmap mTile = GLSurf.mTexMng.getBitmap(RID);
				mTileTextureWidth = (short)mTile.getWidth();
				mTileTextureHeight = (short)mTile.getHeight();
				mTile.recycle();
			}
		};
		tAtlasLoader.start();
	}

	public final void addObj(final GLTexture glo)
	{
		mObjs.add(glo);
		update = true;
	}

	public final void addObj(final GLTexture glo, final float left, final float top, final float right, final float bottom)
	{
		try
		{
			tAtlasLoader.join();
		}
		catch (final InterruptedException e)
		{}

		final float uvs[] =
		{
			(left / (float)mTileTextureWidth), (bottom / (float)mTileTextureHeight),
			(right / (float)mTileTextureWidth), (bottom / (float)mTileTextureHeight),
			(left / (float)mTileTextureWidth), (top / (float)mTileTextureHeight),
			(right / (float)mTileTextureWidth), (top / (float)mTileTextureHeight)
		};
		glo.setUV(uvs);

		addObj(glo);
	}
	
	public final GLTexture getObj(final int idx)
	{
		return mObjs.get(idx);
	}

	public final void removeObj(final GLTexture glo)
	{
		mObjs.remove(glo);
		update = true;
	}

	@Override
	public final void Render()
	{
		if (mObjs.isEmpty() || !bVisible)
			return;
			
		super.Render();
		Update();

		GLES20.glUseProgram(mProgram);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, stride, 0);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glVertexAttribPointer(mColorHandle, GLHelp.COLORS_PER_VERTEX, GLES20.GL_FLOAT, false, stride, GLHelp.COORDS_PER_VERTEX * GLHelp.BYTES_PER_FLOAT);

		GLES20.glEnableVertexAttribArray(mTexCoordHandle);
		GLES20.glVertexAttribPointer(mTexCoordHandle, GLHelp.TEX_COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, stride, (GLHelp.COORDS_PER_VERTEX + GLHelp.COLORS_PER_VERTEX) * GLHelp.BYTES_PER_FLOAT);

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureID);
		GLES20.glUniform1i(mSamplerHandle, 0);
		
        GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, mtrx, 0);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		//GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, drawOrder.length);
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIBOlength, GLES20.GL_UNSIGNED_SHORT, 0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}

	public final void Update()
	{
		if (update)
		{
			mVBO = null;
			mVBO = new float[mObjs.size() * 4 * (GLHelp.COORDS_PER_VERTEX + GLHelp.COLORS_PER_VERTEX + GLHelp.TEX_COORDS_PER_VERTEX)];
			mIBO = new short[mObjs.size() * 6];
		}

		int idx = 0;
		for (short i = 0; i < mObjs.size(); i++)
		{
			final GLTexture glo = mObjs.get(i);

			for (short j = 0; j < 4; j++)
			{
				for (short k = 0; k < GLHelp.COORDS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.mRect[j * GLHelp.COORDS_PER_VERTEX + k];
				}
				for (short k = 0; k < GLHelp.COLORS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.mColor[j * GLHelp.COLORS_PER_VERTEX + k];
				}
				for (short k = 0; k < GLHelp.TEX_COORDS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.uvs[j * GLHelp.TEX_COORDS_PER_VERTEX + k];
				}
			}
			if (update)
			{
				mIBO[i * 6] = (short)(i * 4);
				mIBO[i * 6 + 1] = (short)(i * 4 + 1);
				mIBO[i * 6 + 2] = (short)(i * 4 + 3);
				mIBO[i * 6 + 3] = (short)(i * 4);
				mIBO[i * 6 + 4] = (short)(i * 4 + 3);
				mIBO[i * 6 + 5] = (short)(i * 4 + 2);
			}
		}

		if (update)
		{
			final ByteBuffer b = ByteBuffer.allocateDirect(mVBO.length * GLHelp.BYTES_PER_FLOAT);
			b.order(ByteOrder.nativeOrder());
			mBufferVBO = b.asFloatBuffer();
		}
		mBufferVBO.put(mVBO);
		mBufferVBO.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, mBufferVBO.capacity() * GLHelp.BYTES_PER_FLOAT, mBufferVBO, GLES20.GL_DYNAMIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

		mBufferVBO.clear();

		if (update)
		{
			final ByteBuffer b = ByteBuffer.allocateDirect(mIBO.length * GLHelp.BYTES_PER_SHORT);
			b.order(ByteOrder.nativeOrder());
			final ShortBuffer mBufferIBO = b.asShortBuffer();
			mBufferIBO.put(mIBO);
			mBufferIBO.position(0);

			GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
			GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, mBufferIBO.capacity() * GLHelp.BYTES_PER_SHORT, mBufferIBO, GLES20.GL_DYNAMIC_DRAW);
			GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

			mIBOlength = mIBO.length;
			update = false;

			mBufferIBO.clear();
			mIBO = null;
		}
	}

	public final void Destroy()
	{
		mObjs.clear();
		mBufferVBO.clear();
		mBufferVBO = null;
		mVBO = null;
		mIBO = null;

		GLES20.glDeleteBuffers(buffers.length, buffers, 0);
		GLSurf.mTexMng.ReleaseTexture(mTextureID);
	}
}
