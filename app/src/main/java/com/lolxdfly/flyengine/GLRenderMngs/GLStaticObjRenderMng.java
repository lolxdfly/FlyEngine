package com.lolxdfly.flyengine.GLRenderMngs;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.GLRenderMngBase;
import com.lolxdfly.flyengine.GLElements.GLRenderObj;
import com.lolxdfly.flyengine.GLUtils.GLHelp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

public final class GLStaticObjRenderMng<E extends GLRenderObj> extends GLRenderMngBase
{
	private float mVBO[];
	private int mIBO[];
	private int nPolys = 0;
	private final ArrayList<E> mObjs;
	private boolean update = false;
	private final int buffers[] = new int[2];
	private static final int stride = (GLHelp.COORDS_PER_VERTEX + GLHelp.COLORS_PER_VERTEX) * GLHelp.BYTES_PER_FLOAT;
	private int mType;

	//Shader-Handles
	private int mPosHandle;
	private int mMatrixHandle;
	private int mColorHandle;

	public GLStaticObjRenderMng(final int rendertype)
	{
		mObjs = new ArrayList<E>();

		mType = rendertype;

		//init buffers
		final float tmp[] = new float[] {0};
		ByteBuffer b = ByteBuffer.allocateDirect(tmp.length * GLHelp.BYTES_PER_FLOAT);
        b.order(ByteOrder.nativeOrder());
		final FloatBuffer mBufferVBO = b.asFloatBuffer();
        mBufferVBO.put(tmp);
        mBufferVBO.position(0);

		final int tmp2[] = new int[] {0};
		b = ByteBuffer.allocateDirect(tmp2.length * GLHelp.BYTES_PER_INT);
        b.order(ByteOrder.nativeOrder());
        final IntBuffer drawListBuffer = b.asIntBuffer();
        drawListBuffer.put(tmp2);
        drawListBuffer.position(0);

		GLES20.glGenBuffers(2, buffers, 0);						
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, mBufferVBO.capacity() * GLHelp.BYTES_PER_FLOAT, mBufferVBO, GLES20.GL_STATIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawListBuffer.capacity() * GLHelp.BYTES_PER_INT, drawListBuffer, GLES20.GL_STATIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

		mBufferVBO.clear();
		drawListBuffer.clear();
	}

	public final void setPolyType(final int type)
	{
		mType = type;
	}

	public final void addObj(final E glo)
	{
		mObjs.add(glo);
		nPolys += (glo.mRect.length / 2);
		update = true;
	}

	public final E getObj(final int idx)
	{
		return mObjs.get(idx);
	}

	public final void removeObj(final E glo)
	{
		mObjs.remove(glo);
		nPolys -= (glo.mRect.length / 2);
		update = true;
	}

	private final void prepareDrawGLO()
	{
		mVBO = new float[mObjs.size() * 4 * (GLHelp.COORDS_PER_VERTEX + GLHelp.COLORS_PER_VERTEX)];
		mIBO = new int[mObjs.size() * 6];
		nPolys = mIBO.length;

		int idx = 0;
		for (int i = 0; i < mObjs.size(); i++)
		{
			final E glo = mObjs.get(i);

			for (short j = 0; j < 4; j++)
			{
				for (short k = 0; k < GLHelp.COORDS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.mRect[j * GLHelp.COORDS_PER_VERTEX + k];
				}
				for (short k = 0; k < GLHelp.COLORS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.mColor[j * GLHelp.COLORS_PER_VERTEX + k];
				}
			}
			mIBO[i * 6] = i * 4;
			mIBO[i * 6 + 1] = i * 4 + 1;
			mIBO[i * 6 + 2] = i * 4 + 3;
			mIBO[i * 6 + 3] = i * 4;
			mIBO[i * 6 + 4] = i * 4 + 3;
			mIBO[i * 6 + 5] = i * 4 + 2;
		}

		UpdateBuffers();
		mVBO = null;
		mIBO = null;
		update = false;
	}

	private final void prepareDraw()
	{
		mVBO = new float[nPolys * (GLHelp.COORDS_PER_VERTEX + GLHelp.COLORS_PER_VERTEX)];
		mIBO = new int[nPolys];

		int idx = 0;
		for (final E glo : mObjs)
		{
			for (short j = 0; j < (glo.mRect.length / 2); j++)
			{
				for (short k = 0; k < GLHelp.COORDS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.mRect[j * GLHelp.COORDS_PER_VERTEX + k];
				}
				for (short k = 0; k < GLHelp.COLORS_PER_VERTEX; k++)
				{
					mVBO[idx++] = glo.mColor[j * GLHelp.COLORS_PER_VERTEX + k];
				}
			}
		}
		for (short s = 0; s < nPolys; s++)
			mIBO[s] = s;

		UpdateBuffers();
		mVBO = null;
		mIBO = null;
		update = false;
	}

	private final void UpdateBuffers()
	{
		ByteBuffer b = ByteBuffer.allocateDirect(mVBO.length * GLHelp.BYTES_PER_FLOAT);
        b.order(ByteOrder.nativeOrder());
		final FloatBuffer mBufferVBO = b.asFloatBuffer();
        mBufferVBO.put(mVBO);
        mBufferVBO.position(0);

		b = ByteBuffer.allocateDirect(mIBO.length * GLHelp.BYTES_PER_INT);
        b.order(ByteOrder.nativeOrder());
        final IntBuffer mBufferIBO = b.asIntBuffer();
        mBufferIBO.put(mIBO);
        mBufferIBO.position(0);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, mBufferVBO.capacity() * GLHelp.BYTES_PER_FLOAT, mBufferVBO, GLES20.GL_STATIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, mBufferIBO.capacity() * GLHelp.BYTES_PER_INT, mBufferIBO, GLES20.GL_STATIC_DRAW);

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

		mBufferVBO.clear();
		mBufferIBO.clear();
	}

	@Override
	public final void Render()
	{
		if (mObjs.isEmpty() || !bVisible)
			return;

		super.Render();
		if (update) // update if render data changed
		{
			if (mType == GLES20.GL_TRIANGLES)
				prepareDrawGLO();
			else
				prepareDraw();
		}

		GLES20.glUseProgram(mProgram);

        mPosHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
		mColorHandle = GLES20.glGetAttribLocation(mProgram, "a_Color");
		mMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);

		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, stride, 0);

		GLES20.glEnableVertexAttribArray(mColorHandle);
		GLES20.glVertexAttribPointer(mColorHandle, GLHelp.COLORS_PER_VERTEX, GLES20.GL_FLOAT, false, stride, GLHelp.COORDS_PER_VERTEX * GLHelp.BYTES_PER_FLOAT);

        GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, mtrx, 0);

		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		//GLES20.glDrawArrays(mType, 0, drawOrder.length);
		if (mType != GLES20.GL_TRIANGLE_STRIP && mType != GLES20.GL_TRIANGLE_FAN)
			GLES20.glDrawElements(mType, nPolys, GLES20.GL_UNSIGNED_INT, 0);
		else
		{
			int offset = 0;
			for (final E glo : mObjs)
			{
				final int pol = glo.mRect.length / 2;
				GLES20.glDrawArrays(mType, offset, pol);
				offset += pol;
			}
		}

		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
        GLES20.glDisableVertexAttribArray(mPosHandle);
		GLES20.glDisableVertexAttribArray(mColorHandle);
	}

	public final void Destroy()
	{
		mVBO = null;
		mIBO = null;

		mObjs.clear();

		GLES20.glDeleteBuffers(buffers.length, buffers, 0);
	}
}
