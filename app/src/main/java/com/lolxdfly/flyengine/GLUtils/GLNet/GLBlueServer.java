package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public final class GLBlueServer extends GLBlue
{
	private boolean kill = false;
	private BluetoothServerSocket mServerSocket;
	private final ArrayList<GLNetServerHandler> mHandlers;

	public GLBlueServer(final Context c)
	{
		super(c);
		mHandlers = new ArrayList<GLNetServerHandler>();
	}

	@Override
	public final void connect()
	{
		super.connect();
		try
		{
			// UUID is the app's UUID string, also used by the client code.
            mServerSocket = mBluetoothAdapter.listenUsingRfcommWithServiceRecord("FlyEngine", UUID.fromString("00002415-0000-1000-8000-00805F9B34FB"));
        }
		catch (final IOException e)
		{}
	}

	@Override
	public void run()
	{
        // Keep listening until exception occurs or a socket is returned.
        for (;;)
		{
			if(kill)
				break;
			
			try
			{
				if (mServerSocket == null)
					Thread.sleep(1000);
			}
			catch (final InterruptedException e)
			{}

            try
			{
				BluetoothSocket socket = null;
                socket = mServerSocket.accept();
				if (socket != null)
				{
					// A connection was accepted. Perform work associated with
					// the connection in a separate thread.
					final GLNetServerHandler glnh = new GLNetServerHandler(socket.getInputStream(), socket.getOutputStream(), null, GLUnique.getUnique());
					glnh.start();
					mHandlers.add(glnh);
				}
            }
			catch (final IOException e)
			{}
        }
		try
		{
			mServerSocket.close();
		}
		catch (final IOException e)
		{}
    }

	public final void Destroy()
	{
		kill = true;
		for(int i = 0; i < mHandlers.size(); i++)
			mHandlers.get(i).Destroy();
	}
	
	public final void serialize(final int syncid, final GLSerializable glsr)
	{
		for(int i = 0; i < mHandlers.size(); i++)
			mHandlers.get(i).SendSerialize(syncid, glsr);
	}
	
}
