package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.SnapshotsClient;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.lolxdfly.flyengine.GLElements.SaveGameBase;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

public final class GPlayServiceMng extends Activity
{
	private GoogleSignInAccount mSignedInAccount;
	private final Context mContext;
	private Snapshot mSaveGame;
	private String mCurrentSaveName = "undefined";

	// Intents
	private static final int INT_SIGN_IN = 0x01;
	private static final int INT_ARCH_SHOW = 0x02;
	private static final int INT_LEAD_SHOW = 0x03;
	private static final int INT_SAVE_SHOW = 0x03;


	public GPlayServiceMng(final Context c)
	{
		mContext = c;
	}

	public void signIn()
	{
		final GoogleSignInOptions signInOption = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
					.requestScopes(Drive.SCOPE_APPFOLDER)
					.build();
		final GoogleSignInClient signInClient = GoogleSignIn.getClient(mContext, signInOption);
		Intent intent = signInClient.getSignInIntent();
		((Activity)mContext).startActivityForResult(intent, INT_SIGN_IN);
	}

	public void signOut()
	{
		final GoogleSignInOptions signInOption = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
				.requestScopes(Drive.SCOPE_APPFOLDER)
				.build();
		final GoogleSignInClient signInClient = GoogleSignIn.getClient(mContext, signInOption);
		signInClient.signOut(); // onComplete?
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == INT_SIGN_IN)
		{
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			if (result.isSuccess())
			{
				mSignedInAccount = result.getSignInAccount();
			}
			else
			{
				String message = result.getStatus().getStatusMessage();
				if (message == null || message.isEmpty())
					message = "Error during Login!";
				new AlertDialog.Builder(mContext).setMessage(message).setNeutralButton(android.R.string.ok, null).show();
			}
		}
		else if (requestCode == INT_SAVE_SHOW)
		{
			if (data.hasExtra(SnapshotsClient.EXTRA_SNAPSHOT_METADATA))
			{
				// Load a snapshot.
				SnapshotMetadata snapshotMetadata = data.getParcelableExtra(SnapshotsClient.EXTRA_SNAPSHOT_METADATA);
				mCurrentSaveName = snapshotMetadata.getUniqueName();

				// Load the game data from the Snapshot
				// TODO: how to get Snapshot!?
			}
			else if (data.hasExtra(SnapshotsClient.EXTRA_SNAPSHOT_NEW))
			{
				// Create a new snapshot named with a unique string
				String unique = new BigInteger(281, new Random()).toString(13);
				mCurrentSaveName = "snapshotTemp-" + unique;

				// Create the new snapshot
				// TODO: create new savegame and notify
			}
		}
	}

	///***Leaderboards***///
	public final void setLeaderboardHighScore(final String LID, final long score)
	{
		Games.getLeaderboardsClient(mContext, mSignedInAccount).submitScore(LID, score);
	}

	public final void ShowLeaderboards(final String LID)
	{
		Games.getLeaderboardsClient(mContext, mSignedInAccount)
				.getLeaderboardIntent(LID)
				.addOnSuccessListener(new OnSuccessListener<Intent>() {
					@Override
					public void onSuccess(Intent intent) {
						((Activity)mContext).startActivityForResult(intent, INT_LEAD_SHOW);
					}
				});
	}

	///***Achievements***///
	public final void unlockAchievement(final String AID)
	{
		Games.getAchievementsClient(mContext, mSignedInAccount).unlock(AID);
	}

	public final void incrementAchievement(final String AID, final int n)
	{
		Games.getAchievementsClient(mContext, mSignedInAccount).increment(AID, n);
	}

	public final void ShowAchievements()
	{
		Games.getAchievementsClient(mContext, mSignedInAccount).getAchievementsIntent().addOnSuccessListener(new OnSuccessListener<Intent>() {
					@Override
					public void onSuccess(Intent intent) {
						((Activity)mContext).startActivityForResult(intent, INT_ARCH_SHOW);
					}
				});
	}

	///***Snapshots(SaveGames)***///
	public final Task<SnapshotMetadata> saveGame(SaveGameBase svg) // use addOnCompleteListener
	{
		SnapshotsClient snapshotsClient = Games.getSnapshotsClient(mContext, mSignedInAccount);

		mSaveGame.getSnapshotContents().writeBytes(svg.getData());

		// Commit the operation
		return snapshotsClient.commitAndClose(mSaveGame, svg.getMetaData());
	}

	public final Task<byte[]> loadGame(final SaveGameBase svg[]) // use addOnCompleteListener
	{
		// Get the SnapshotsClient from the signed in account.
		SnapshotsClient snapshotsClient = Games.getSnapshotsClient(mContext, mSignedInAccount);

		// In the case of a conflict, the most recently modified version of this snapshot will be used.
		final int conflictResolutionPolicy = SnapshotsClient.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED;

		// Open the saved game using its name.
		return snapshotsClient.open(mCurrentSaveName, true, conflictResolutionPolicy)
				.addOnFailureListener(new OnFailureListener() {
					@Override
					public void onFailure(@NonNull Exception e) {	}

				}).continueWith(new Continuation<SnapshotsClient.DataOrConflict<Snapshot>, byte[]>() {
					@Override
					public byte[] then(@NonNull Task<SnapshotsClient.DataOrConflict<Snapshot>> task) throws Exception {
						mSaveGame = task.getResult().getData();

						// Opening the snapshot was a success and any conflicts have been resolved.
						try {
							// Extract the raw data from the snapshot.
							final byte data[] = mSaveGame.getSnapshotContents().readFully();
							svg[0] = new SaveGameBase(mSaveGame.getMetadata().getUniqueName(), data);
							svg[0].setDescription(mSaveGame.getMetadata().getDescription());
						} catch (IOException e) {	}

						return null; // return no real data
					}
				});
	}

	public final void ShowSavedGames()
	{
		SnapshotsClient snapshotsClient = Games.getSnapshotsClient(mContext, mSignedInAccount);
		final int maxNumberOfSavedGamesToShow = 3;

		Task<Intent> intentTask = snapshotsClient.getSelectSnapshotIntent("See My Saves", true, true, maxNumberOfSavedGamesToShow);

		intentTask.addOnSuccessListener(new OnSuccessListener<Intent>() {
			@Override
			public void onSuccess(Intent intent) {
				((Activity) mContext).startActivityForResult(intent, INT_SAVE_SHOW);
			}
		});
	}

	public final void Destroy()
	{

	}
}
