package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public final class GLBlueClient extends GLBlue
{
	private final ArrayList<BluetoothDevice> mFoundDevices;
	private BluetoothDevice mDevice;
	private BluetoothSocket mSocket;
	private GLNetClientHandler mNch;
	private final Context context;

	public GLBlueClient(final Context c)
	{
		super(c);

		context = c;
		mFoundDevices = new ArrayList<BluetoothDevice>();
	}

	public final void Destroy()
	{
		stopsearch();
		mFoundDevices.clear();
		if(mNch != null)
			mNch.Destroy();
	}

	public final void startsearch()
	{
		// Register for broadcasts when a device is discovered.
		final IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		context.registerReceiver(mReceiver, filter);
		
		mBluetoothAdapter.startDiscovery();
	}

	public final void stopsearch()
	{
		mBluetoothAdapter.cancelDiscovery();
		context.unregisterReceiver(mReceiver);
	}

	public final void setBluetoothDevice(final BluetoothDevice bd)
	{
		stopsearch();
		mDevice = bd;
		try
		{
			mSocket = mDevice.createRfcommSocketToServiceRecord(UUID.fromString("00002415-0000-1000-8000-00805F9B34FB"));
		}
		catch (final IOException e)
		{}
		start();
	}

	// Create a BroadcastReceiver for ACTION_FOUND.
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		public final void onReceive(final Context context, final Intent intent)
		{
			if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction()))
			{
				// Discovery has found a device. Get the BluetoothDevice
				// object and its info from the Intent.
				mFoundDevices.add((BluetoothDevice)intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE));
			}
		}
	};

	public final Set<BluetoothDevice> getPairedDevices()
	{
		return mBluetoothAdapter.getBondedDevices();
	}
	
	public final ArrayList<BluetoothDevice> getFoundDevices()
	{
		return mFoundDevices;
	}

	@Override
	public final void run()
	{
		super.run();
		try
		{
            // Connect to the remote device through the socket. This call blocks
            // until it succeeds or throws an exception.
            mSocket.connect();
        }
		catch (final IOException connectException)
		{
            // Unable to connect; close the socket and return.
            try
			{
                mSocket.close();
            }
			catch (final IOException closeException)
			{}
            return;
        }

        // The connection attempt succeeded. Perform work associated with
        // the connection in a separate thread.
       	try
		{
			mNch = new GLNetClientHandler(mSocket.getInputStream(), mSocket.getOutputStream(), mServerSync);
		}
		catch (IOException e)
		{}
		mNch.start();
	}

	public final void serialize(final int syncID, final GLSerializable glsr)
	{
		if(mNch != null && mNch.isConnected()) // not connected
			mNch.serialize(syncID, glsr);
	}
}
