package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.os.Environment;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLAnimation;
import com.lolxdfly.flyengine.GLElements.GLText;
import com.lolxdfly.flyengine.GLElements.GLTexture;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUIButton;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUICombobox;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUIProgressbar;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUITabView;
import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUITextbox;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.R;

import java.io.File;

public final class WndDebug extends WndBase
{
	private final GLTexture mBG;
	private final GLText mDebugInfo;
	private final GLUITextbox mTestFile, mOptions;
	private final GLUICombobox<String> mElements;
	private String sActiveElement;

	private long lFPSCnt = 0;
	private int iFPSFrames = 0;

	private final RectF mRect;
	private final String path;

	//////////TestObjects///////////
	////////////////////////////////
	private GLTexture mGLTexture;
	private GLAnimation mGLAnimation;
	private GLText mGLText;
	private GLUIButton mGLUIButton;
	private GLUIProgressbar mGLUIProgressbar;
	private GLUITextbox mGLUITextbox;
	private GLUICombobox<String> mGLUICombobox;
	private GLUITabView mGLUITabView;
	////////////////////////////////

	public WndDebug()
	{
		path = Environment.getExternalStorageDirectory() + File.separator + "FlyEnigne" + File.separator;
		final File fpath = new File(path);
		if (!fpath.exists())
			fpath.mkdirs();
		mRect = new RectF(550, 550, 1000, 1000);

		mBG = new GLTexture(R.drawable.background, false);
		mBG.setRect(new RectF(0, 0, GLCamera.mVirtualScreenWidth, GLCamera.mVirtualScreenHeight));

		mTestFile = new GLUITextbox(R.drawable.tb, 20);
		mTestFile.setPos(10, 50, 500, 150);
		mTestFile.setHintTex("TestFileName");
		mOptions = new GLUITextbox(R.drawable.tb, 20);
		mOptions.setPos(550, 50, 1200, 150);
		mOptions.setHintTex("left:top:right:bottom");

		mElements = new GLUICombobox<String>(R.drawable.cmbbox, "");
		mElements.setPos(10, 175, 500, 225);
		mElements.setItemTexture(R.drawable.cmbitem);
		mElements.addItem("GLTexture");
		mElements.addItem("GLAnimation");
		mElements.addItem("GLText");
		mElements.addItem("GLUIButton");
		mElements.addItem("GLUIProgressbar");
		mElements.addItem("GLUITextbox");
		mElements.addItem("GLUICombobox");
		mElements.addItem("GLUITabView");

		mDebugInfo = new GLText("00#0000#0000", 0, 0, new float[]{0.0f, 0.0f, 0.0f, 1.0f});
	}

	@Override
	public final void onTouch(final MotionEvent event)
	{
		if (mElements.isClicked(event) && !sActiveElement.equals(mElements.getSelectedItem()))
		{
			DestroyElements();
			setRect();
			switch (sActiveElement = mElements.getSelectedItem())
			{
				case "GLTexture":
					{
						mGLTexture = new GLTexture(R.drawable.texdef, false);
						mGLTexture.setTexture(BitmapFactory.decodeFile(path + mTestFile.getText() + ".png"));
						mGLTexture.setRect(mRect);
						break;
					}
				case "GLAnimation":
					{
						mGLAnimation = new GLAnimation(R.drawable.texdef, 250, 250, 30);
						mGLAnimation.setTexture(BitmapFactory.decodeFile(path + mTestFile.getText() + ".png"));
						mGLAnimation.setRect(mRect);
						break;
					}
				case "GLText":
					{
						mGLText = new GLText(mTestFile.getText(), mRect.left, mRect.top, new float[]{0.0f, 0.0f, 0.0f, 1.0f});
						break;
					}
				case "GLUIButton": 
					{
						mGLUIButton = new GLUIButton(R.drawable.texdef);
						mGLUIButton.setTexture(BitmapFactory.decodeFile(path + mTestFile.getText() + ".png"));
						mGLUIButton.setRect(mRect);
						break;
					}
				case "GLUIProgressbar":
					{
						mGLUIProgressbar = new GLUIProgressbar(R.drawable.texdef, true);
						mGLUIProgressbar.setTexture(BitmapFactory.decodeFile(path + mTestFile.getText() + ".png"));
						mGLUIProgressbar.setRect(mRect);
						break;
					}
				case "GLUITextbox":
					{
						mGLUITextbox = new GLUITextbox(R.drawable.texdef, 20);
						mGLUITextbox.setTexture(BitmapFactory.decodeFile(path + mTestFile.getText() + ".png"));
						mGLUITextbox.setRect(mRect);
						break;
					}
				case "GLUICombobox":
					{
						mGLUICombobox = new GLUICombobox<String>(R.drawable.texdef, "Option 1");
						mGLUICombobox.setTexture(BitmapFactory.decodeFile(path + mTestFile.getText() + ".png"));
						mGLUICombobox.setRect(mRect);
						mGLUICombobox.setItemTexture(R.drawable.cmbitem);
						mGLUICombobox.addItem("Option 1");
						mGLUICombobox.addItem("Option 2");
						break;
					}
				case "GLUITabView":
					{
						//final WndBase wnds[] =  { new WndRenderObjs(), new WndGame() };
						//mGLUITabView = new GLUITabView(2, 300, wnds);
						break;
					}
			}
		}
		switch (mElements.getSelectedItem())
		{
			case "GLUIButton": mGLUIButton.isClicked(event); break;
			case "GLUITextbox": mGLUITextbox.onTouch(event); break;
			case "GLUICombobox": mGLUICombobox.onTouch(event); break;
			case "GLUITabView": mGLUITabView.onTouch(event); break;
		}
		mTestFile.onTouch(event);
		mOptions.onTouch(event);
	}

	@Override
	public void onDraw()
	{
		mBG.Render();

		switch (mElements.getSelectedItem())
		{
			case "GLTexture": mGLTexture.Render(); break;
			case "GLAnimation": mGLAnimation.Render(); break;
			case "GLText": mGLText.Render(); break;
			case "GLUIButton": mGLUIButton.Render(); break;
			case "GLUIProgressbar": mGLUIProgressbar.Render(); break;
			case "GLUITextbox": mGLUITextbox.Render(); break;
			case "GLUICombobox": mGLUICombobox.Render(); break;
			case "GLUITabView": mGLUITabView.Render(); break;
		}

		mTestFile.Render();
		mOptions.Render();
		mElements.Render();
		mDebugInfo.Render();
	}

	@Override
	public final void Process(final long elapsed)
	{
		if (mElements.getSelectedItem().equals("GLAnimation"))
			mGLAnimation.Update(elapsed);
		else if (mElements.getSelectedItem().equals("GLUIProgressbar"))
			mGLUIProgressbar.setPercent((iFPSFrames * 1000.0f / lFPSCnt));

		lFPSCnt += elapsed;
		iFPSFrames++;
		if (lFPSCnt > 100)
		{
			setDebugInfo((int)(iFPSFrames * 1000 / lFPSCnt));
			lFPSCnt = 0;
			iFPSFrames = 0;
		}
	}

	private final void setDebugInfo(final int fps)
	{
		mDebugInfo.setText(fps + "#" + GLSurf.mCamera.mScreenWidth + "#" + GLSurf.mCamera.mScreenHeight);
	}

	private final void setRect()
	{
		if (!mOptions.getText().isEmpty() && !mOptions.getText().equals("left:top:right:bottom"))
		{
			final String s = mOptions.getText();
			final String rect[] = s.split(":");
			mRect.set(Float.valueOf(rect[0]), Float.valueOf(rect[1]), Float.valueOf(rect[2]), Float.valueOf(rect[3]));
		}
	}

	private final void DestroyElements()
	{
		if (mGLTexture !=  null)
			mGLTexture.Destroy();
		else if (mGLAnimation != null)
			mGLAnimation.Destroy();
		else if (mGLText != null)
			mGLText.Destroy();
		else if (mGLUIButton != null)
			mGLUIButton.Destroy();
		else if (mGLUIProgressbar != null)
			mGLUIProgressbar.Destroy();
		else if (mGLUITextbox != null)
			mGLUITextbox.Destroy();
		else if (mGLUICombobox != null)
			mGLUICombobox.Destroy();
		else if (mGLUITabView != null)
			mGLUITabView.Destroy();
	}

	@Override
	public final void Destroy()
	{
		super.Destroy();
		mBG.Destroy();
		mDebugInfo.Destroy();
		mElements.Destroy();
		mOptions.Destroy();
		mTestFile.Destroy();
	}
}
