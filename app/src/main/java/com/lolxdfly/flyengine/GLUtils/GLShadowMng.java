package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLElements.GLPoly;
import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.GLRenderMngBase;
import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;
import com.lolxdfly.flyengine.GLRenderMngs.GLDynamicObjRenderMng;

import java.util.ArrayList;

public final class GLShadowMng extends GLRenderMngBase
{
	private final ArrayList<GLRect> mObjs = new ArrayList<GLRect>();
	private final GLDynamicObjRenderMng<GLPoly> mShadows;
	
	protected boolean bLight = false;
	protected final float mLightPos[];
	protected short sLiD = -1;
	
	private final GLLightMng g_Lights;

	public GLShadowMng(final GLLightMng lights)
	{
		super();
		
		mShadows = new GLDynamicObjRenderMng<GLPoly>(GLES20.GL_TRIANGLE_FAN);
		g_Lights = lights;
		mLightPos = g_Lights.getLightInfo();
	}

	public final void update()
	{
		// is there a light source
		if (mLightPos == null)
			return;

		// for all Objects
		int shadowidx = 0;
		for (final GLRect glo : mObjs)
		{
			// is glo visible?
			if (!glo.isVisible() || !GLSurf.mCamera.isVisible(glo.mRect))
				continue;

			//create vertice
			final float LightToGlo[] = {
				//Glo pos
				glo.mRect[0], glo.mRect[1], //left bot
				glo.mRect[2], glo.mRect[3], //right bot
				glo.mRect[4], glo.mRect[5], //left top
				glo.mRect[6], glo.mRect[7], //right top

				//Vec from Light to Glo (this is glo to light!?)
				mLightPos[0] - glo.mRect[0], mLightPos[1] - glo.mRect[1],
				mLightPos[0] - glo.mRect[2], mLightPos[1] - glo.mRect[3],
				mLightPos[0] - glo.mRect[4], mLightPos[1] - glo.mRect[5],
				mLightPos[0] - glo.mRect[6], mLightPos[1] - glo.mRect[7]
			};
			
			final float camX = GLSurf.mCamera.getX();
			final float camY = GLSurf.mCamera.getY();

			// check glo location
			/*
					locy = -1
			  locx = -1	* locx = 1
			  		locy = 1
					
			// and locx/locy = 0 when light inside glo
			*/
			int locx = 0;
			int locy = 0;
			if (mLightPos[0] > LightToGlo[2])
				locx = 1;
			else if (mLightPos[0] < LightToGlo[0])
				locx = -1;
			if (mLightPos[1] > LightToGlo[1])
				locy = 1;
			else if (mLightPos[1] < LightToGlo[5])
				locy = -1;

			//remove useless vertice
			int offset = 2; //number of vertice to remove
			if (locx == 0)
			{
				if (locy == 0) //Light inside Glo
				{
					g_Lights.turnoffLight(0);
					final float tmp[] = new float[]{
						camX + GLCamera.mVirtualScreenWidth, camY + GLCamera.mVirtualScreenHeight,
						camX + GLCamera.mVirtualScreenWidth, camY,
						camX, camY,
						camX, camY + GLCamera.mVirtualScreenHeight
					};
					mShadows.getObj(shadowidx++).setVertices(tmp);
					break; //continue to next Light
				}
				else if (locy == -1) //Glo above Light
				{
					//remove LightToGlo.Glo.bot
					LightToGlo[0] = Float.MAX_VALUE;
					LightToGlo[1] = Float.MAX_VALUE;
					LightToGlo[2] = Float.MAX_VALUE;
					LightToGlo[3] = Float.MAX_VALUE;
				}
				else //Glo under Light
				{
					//remove LightToGlow.Glo.top
					LightToGlo[4] = Float.MAX_VALUE;
					LightToGlo[5] = Float.MAX_VALUE;
					LightToGlo[6] = Float.MAX_VALUE;
					LightToGlo[7] = Float.MAX_VALUE;
				}
				offset = 4;
			}
			else if (locy == 0)
			{
				if (locx == -1) //Glo left of Light
				{
					//remove LightToGlo.Glo.right
					LightToGlo[2] = Float.MAX_VALUE;
					LightToGlo[3] = Float.MAX_VALUE;
					LightToGlo[6] = Float.MAX_VALUE;
					LightToGlo[7] = Float.MAX_VALUE;
				}
				else //Glo right of Light
				{
					//remove LightToGlo.Glo.left
					LightToGlo[0] = Float.MAX_VALUE;
					LightToGlo[1] = Float.MAX_VALUE;
					LightToGlo[4] = Float.MAX_VALUE;
					LightToGlo[5] = Float.MAX_VALUE;
				}
				offset = 4;
			}
			else
			{
				if (locx == -1)
				{
					if (locy == -1)
					{
						//remove LightToGlo.Glo.botright
						LightToGlo[2] = Float.MAX_VALUE;
						LightToGlo[3] = Float.MAX_VALUE;
					}
					else
					{
						//remove LightToGlo.Glo.topright
						LightToGlo[6] = Float.MAX_VALUE;
						LightToGlo[7] = Float.MAX_VALUE;
					}
				}
				else
				{
					if (locy == -1)
					{
						//remove LightToGlo.Glo.botleft
						LightToGlo[0] = Float.MAX_VALUE;
						LightToGlo[1] = Float.MAX_VALUE;
					}
					else
					{
						//remove LightToGlo.Glo.topleft
						LightToGlo[4] = Float.MAX_VALUE;
						LightToGlo[5] = Float.MAX_VALUE;
					}
				}
			}

			//turn always on
			g_Lights.turnonLight(0);

			//Light Ray Cast
			short lastaxis = -1;
			final float ToaddCorners[] = {-1, -1, -1, -1};
			int ToaddCornersIdx = 0;
			for (int j = 8; j < 15; j += 2)
			{
				//remove unused vecs
				if (LightToGlo[j - 8] == Float.MAX_VALUE)
				{
					LightToGlo[j] = Float.MAX_VALUE;
					LightToGlo[j + 1] = Float.MAX_VALUE;
					offset += 2;
					continue;
				}

				float x;
				float y;
				short axis = -1;

				//l under glo?
				if (LightToGlo[j + 1] > 0)
				{
					//get x-top cut
					//if x cut, y = 0
					// x = lx + ((0 - ly) / py) * px
					x = mLightPos[0] + ((camY - mLightPos[1]) / LightToGlo[j + 1]) * LightToGlo[j];
					y = camY;
					axis = 0;
				}
				else
				{
					//get x-bot cut
					//if x cut, y = max_y
					// x = lx + ((1080 - ly) / py) * px
					x = mLightPos[0] + ((camY + GLCamera.mVirtualScreenHeight - mLightPos[1]) / LightToGlo[j + 1]) * LightToGlo[j];
					y = camY + GLCamera.mVirtualScreenHeight;
					axis = 2;
				}

				//if cut is in screen range => y cut
				if (x >= camX && x <= camX + GLCamera.mVirtualScreenWidth)
				{
					//set x cut
					LightToGlo[j] = x;
					LightToGlo[j + 1] = y;
				}
				else
				{
					//is glo right of l?
					if (LightToGlo[j] > 0)
					{
						//set y-left cut
						//if y cut, x = 0
						// y = ly + ((0 - lx) / px) * py
						LightToGlo[j + 1] = mLightPos[1] + ((camY - mLightPos[0]) / LightToGlo[j]) * LightToGlo[j + 1];
						LightToGlo[j] = camX;
						axis = 1;
					}
					else
					{
						//set y-right cut
						//if  y cut, x = max_x
						// y = ly + ((1920 - lx) / px) * py
						LightToGlo[j + 1] = mLightPos[1] + ((camX + GLCamera.mVirtualScreenWidth - mLightPos[0]) / LightToGlo[j]) * LightToGlo[j + 1];
						LightToGlo[j] = camX + GLCamera.mVirtualScreenWidth;
						axis = 3;
					}
				}

				//Add Corners if needed
				if (axis != lastaxis && ToaddCornersIdx < 4)
				{
					switch (lastaxis)
					{
						case 0:
							{
								if (axis == 1)
								{
									//add 0,0
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
									else if (!(ToaddCorners[ToaddCornersIdx - 1] == camY && ToaddCorners[ToaddCornersIdx - 2] == camX))
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
								}
								else if (axis == 3)
								{
									//add 1920,0
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = GLCamera.mVirtualScreenWidth + camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
									else if (!(ToaddCorners[ToaddCornersIdx - 1] == camY && ToaddCorners[ToaddCornersIdx - 2] == GLCamera.mVirtualScreenWidth + camX))
									{
										ToaddCorners[ToaddCornersIdx++] = GLCamera.mVirtualScreenWidth + camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
								}
								else
								{
									ToaddCornersIdx = 0;
									if (locx == 1)
									{
										//add 0,0
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
										//add 0,1080
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
									else
									{
										//add 1920,0
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY;
										//add 1920,1080
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								break;
							}
						case 1:
							{
								if (axis == 2)
								{
									//add 0,1080
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
									else if (!(ToaddCorners[ToaddCornersIdx - 1] == camY + GLCamera.mVirtualScreenHeight && ToaddCorners[ToaddCornersIdx - 2] == camX))
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								else if (axis == 0)
								{
									//add 0,0
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
									else if (!(ToaddCorners[ToaddCornersIdx - 1] == camY && ToaddCorners[ToaddCornersIdx - 2] == camX))
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
								}
								else
								{
									ToaddCornersIdx = 0;
									if (locy == 1)
									{
										//add 0,0
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
										//add 1920, 0
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
									else
									{
										//add 0,1080
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
										//add 1920, 1080
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								break;
							}
						case 2:
							{
								if (axis == 3)
								{
									//add 1920,1080
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
									else 
									if (!(ToaddCorners[ToaddCornersIdx - 1] == camY + GLCamera.mVirtualScreenHeight && ToaddCorners[ToaddCornersIdx - 2] == camX + GLCamera.mVirtualScreenWidth))
									{
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY +  GLCamera.mVirtualScreenHeight;
									}
								}
								else if (axis == 1)
								{
									//add 0,1080
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
									else 
									if (!(ToaddCorners[ToaddCornersIdx - 1] == camY + GLCamera.mVirtualScreenHeight && ToaddCorners[ToaddCornersIdx - 2] == camX))
									{
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								else
								{
									ToaddCornersIdx = 0;
									if (locx == 1)
									{
										//add 0,0
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
										//add 0,1080
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
									else
									{
										//add 1920,0
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY;
										//add 1920,1080
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								break;
							}
						case 3:
							{
								if (axis == 0)
								{
									//add 1920,0
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
									else 
									if (!(ToaddCorners[ToaddCornersIdx - 1] == camY && ToaddCorners[ToaddCornersIdx - 2] == camX + GLCamera.mVirtualScreenWidth))
									{
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
								}
								else if (axis == 2)
								{
									//add 1920,1080
									if (ToaddCornersIdx == 0)
									{
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
									else 
									if (!(ToaddCorners[ToaddCornersIdx - 1] == camY + GLCamera.mVirtualScreenHeight && ToaddCorners[ToaddCornersIdx - 2] == camX + GLCamera.mVirtualScreenWidth))
									{
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								else
								{
									ToaddCornersIdx = 0;
									if (locy == 1)
									{
										//add 0,0
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY;
										//add 1920, 0
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY;
									}
									else
									{
										//add 0,1080
										ToaddCorners[ToaddCornersIdx++] = camX;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
										//add 1920, 1080
										ToaddCorners[ToaddCornersIdx++] = camX + GLCamera.mVirtualScreenWidth;
										ToaddCorners[ToaddCornersIdx++] = camY + GLCamera.mVirtualScreenHeight;
									}
								}
								break;
							}
					}
					lastaxis = axis;
				}
			}

			//remove Points / Add Corners
			final float tmp[] = new float[LightToGlo.length - offset + ToaddCornersIdx];
			int tmpidx = 0;
			int tacidx = 0;
			for(final float f : LightToGlo)
			{
				if (f == Float.MAX_VALUE)
				{
					if (tacidx < ToaddCornersIdx)
						tmp[tmpidx++] = ToaddCorners[tacidx++];
				}
				else
					tmp[tmpidx++] = f;
			}

			GLCamera.SortVecDeg(tmp);
			mShadows.getObj(shadowidx++).setVertices(tmp);
		}
		mShadows.updatePolyCount();
	}

	@Override
	public final void Render()
	{
		super.Render();
		update();
		mShadows.Render();
	}

	public final void Destroy()
	{
		mObjs.clear();
		mShadows.Destroy();
	}

	public final void registerObj(final GLRect glo)
	{
		mObjs.add(glo);
		final GLPoly shadow = new GLPoly(false, GLES20.GL_TRIANGLE_FAN);
		shadow.setColor(0, 0, 0, 1);
		mShadows.addObj(shadow);
	}

	public final void unregisterObj(final GLRect glo)
	{
		mObjs.remove(glo);
		mShadows.removeObj(mShadows.getObj(mObjs.size() - 1));
	}
}
