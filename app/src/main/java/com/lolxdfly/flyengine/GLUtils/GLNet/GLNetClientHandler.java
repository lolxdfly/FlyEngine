package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class GLNetClientHandler extends Thread
{
	private final InputStream mReader;
	private final OutputStream mWriter;
	private final GLSync gls;

	private int clientid = -1;
	private boolean kill = false;

	public GLNetClientHandler(final InputStream ois, final OutputStream oos, final GLSync glsync)
	{
		mReader = ois;
		mWriter = oos;
		gls = glsync;

		try
		{
			//ois.read() == (PACKAGE_LOGIN)
			clientid = mReader.read();
		}
		catch (final IOException e)
		{}

	}

	@Override
	public void run()
	{
		super.run();
		try
		{
			for (;;)
			{
				final int PackageId = mReader.read();
				if(kill)
					break;
				switch(PackageId)
				{
					case GLNetPackage.PACKAGE_DISCONNECTED: OnDisconnected(); break;
					case GLNetPackage.PACKAGE_SERIALIZE: OnSerialize(); break;
					default:
						break;
				}
				mReader.read(); //EOP
			}
		}
		catch (final IOException e)
		{}
	}
	
	public final void serialize(final int syncid, final GLSerializable glo)
	{
		try
		{
			mWriter.write(GLNetPackage.PACKAGE_SERIALIZE);
			mWriter.write(syncid);
			glo.serialize(new ObjectOutputStream(mWriter));
			mWriter.write(GLNetPackage.EOP);
			mWriter.flush();
			//mWriter.close();
		}
		catch (final IOException e)
		{}
	}
	
	public final boolean isConnected()
	{
		return !kill;
	}
	
	public final void Destroy()
	{
		kill = true;
		try
		{
			mReader.close();
			mWriter.close();
		}
		catch (final IOException e)
		{}
	}
	
////Client Functions////
	private final void OnSerialize()
	{
		try
		{
			gls.get(mReader.read()).serialize(new ObjectInputStream(mReader));
		}
		catch (IOException e)
		{}
	}
	private final void OnDisconnected()
	{
		Destroy();
	}
////////////////////////
}
