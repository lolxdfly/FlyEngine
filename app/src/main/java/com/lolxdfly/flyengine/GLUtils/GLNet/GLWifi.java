package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.io.IOException;
import java.net.Socket;

public final class GLWifi
{
	private Socket mSock;
	public static final GLSync mServerSync = new GLSync();
	private GLNetClientHandler mNch;
	
	private final String ip;
	private final int port;
	
	public GLWifi(final String ip, final int port)
	{
		this.ip = ip;
		this.port = port;
		Reconnect();
	}
	
	public final void Reconnect()
	{
		try
		{
			mSock = new Socket(ip, port);
			mNch = new GLNetClientHandler(mSock.getInputStream(), mSock.getOutputStream(), mServerSync);
		}
		catch (final IOException e)
		{}
	}
	
	public final void serialize(final int syncID, final GLSerializable glsr)
	{
		if(mNch != null && mNch.isConnected()) // not connected
			mNch.serialize(syncID, glsr);
	}

	public final void Destroy()
	{
		mNch.Destroy();
		try
		{
			mSock.close();
		}
		catch (final IOException e)
		{}
	}
}
