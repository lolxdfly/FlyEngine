package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class GLUnknown implements GLSerializable
{
	byte data[];
	
	@Override
	public final void serialize(final ObjectOutputStream oos)
	{
		try
		{
			for (final byte b : data)
				oos.write(b);
		}
		catch (final IOException e)
		{}
	}

	@Override
	public final void serialize(final ObjectInputStream ois)
	{
		try
		{
			byte b = 0;
			int i = 0;
			while((b = ois.readByte()) != GLNetPackage.EOP)
				data[i++] = b;
		}
		catch (final IOException e)
		{}
	}

}
