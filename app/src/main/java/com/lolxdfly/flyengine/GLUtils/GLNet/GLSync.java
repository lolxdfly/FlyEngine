package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.util.HashMap;

public final class GLSync extends HashMap<Integer, GLSerializable>
{
	public static final int nid_test = 0;
}
