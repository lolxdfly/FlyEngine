package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import com.lolxdfly.flyengine.GLMain.GLSurf;

import java.util.HashMap;
import java.util.Map;

public final class GLAnimationMng
{
	public final class Anim
	{
		public int texid;
		public float width;
		public float height;
		public float atlaswidth;
		public float atlasheight;
		public int dtime;
	}
	
	private final HashMap<Integer, Anim> mAnims;
	
	public GLAnimationMng()
	{
		mAnims = new HashMap<Integer, Anim>();
	}
	
	public final void preloadAnim(final int RID, final float width, final int height, final int time)
	{
		final Anim a = new Anim();
		a.texid = GLSurf.mTexMng.loadTexture(RID);
		a.width = width;
		a.height = height;
		a.atlaswidth = GLSurf.mTexMng.getBitmap(RID).getWidth();
		a.atlasheight = GLSurf.mTexMng.getBitmap(RID).getHeight();
		a.dtime = time;
		
		mAnims.put(RID, a);
	}
	
	public final Anim getAnim(final int RID)
	{
		return mAnims.get(RID);
	}
	
	public final void releaseAnim(final int RID)
	{
		GLSurf.mTexMng.ReleaseTexture(mAnims.get(RID).texid);
		mAnims.remove(RID);
	}
	
	public final void Destroy()
	{
		for (final Map.Entry<Integer, Anim> entry : mAnims.entrySet())
		{
			GLSurf.mTexMng.ReleaseTexture(entry.getValue().texid);
		}
		mAnims.clear();
	}
}
