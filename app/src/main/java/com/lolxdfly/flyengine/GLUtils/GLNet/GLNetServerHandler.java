package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import com.lolxdfly.flyengine.GLServer.GLConsole;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Map;

public final class GLNetServerHandler extends Thread
{
	private final InputStream mReader;
	private final OutputStream mWriter;
	//private static GLSync gls;
	private final Map<Integer, GLNetServerHandler> mClients;
	
	private final int clientid;
	private boolean kill = false;

	public GLNetServerHandler(final InputStream ois, final OutputStream oos, final Map<Integer, GLNetServerHandler> clients, final int id)//, final GLSync gls)
	{
		mClients = clients;
		mReader = ois;
		mWriter = oos;
		clientid = id;
		//this.gls = gls;
		
		try
		{
			//mWriter.write(PACKAGE_LOGIN);
			mWriter.write(clientid);
		}
		catch (final IOException e)
		{}
	}

	@Override
	public void run()
	{
		super.run();
		try
		{
			for (;;)
			{	
				final int PackageId = mReader.read();
				if (kill || PackageId == GLNetPackage.PACKAGE_DISCONNECTED)
					break;
				switch (PackageId)
				{
					case GLNetPackage.PACKAGE_SERIALIZE: OnSerialize(); break;
					default:
						GLConsole.log("Unknown PackageID: " + PackageId);	
						break;
				}
				//mReader.read(); //EOP
			}
		}
		catch (final IOException e)
		{}
		Destroy();
		GLConsole.log("Clientconnection closed: " + clientid);		
	}
	
	public int getClientId()
	{
		return clientid;
	}
	
	public final OutputStream getWriter()
	{
		return mWriter;
	}

	public final void Destroy()
	{
		kill = true;
		GLUnique.freeUnique(clientid);
		try 
		{
			mReader.close();
			mWriter.close();
		} 
		catch (final IOException e)
		{}
	}

////Server Functions////
	public final void SendSerialize(final int syncid, final GLSerializable glsa)
	{
		try
		{
			mWriter.write(GLNetPackage.PACKAGE_SERIALIZE);
			mWriter.write(syncid);
			glsa.serialize(new ObjectOutputStream(mWriter));
			mWriter.write(GLNetPackage.EOP);
			mWriter.flush();
			//mWriter.close();
		}
		catch (final IOException e)
		{}
	}
	private final void OnSerialize()
	{
		try
		{
			final int gloid = mReader.read();
			GLConsole.log("Serializing(Client: " + clientid + ", GLSyncID: " + gloid + ")");
			GLSerializable glsa;// = gls.get(gloid);
			//if (glsa == null)
			//{
				//GLConsole.log("GLSync ID not found: " + gloid);
				
				//create GLUnknown and sync data clientside
				glsa = new GLUnknown();
			//}
			glsa.serialize(new ObjectInputStream(mReader));
				
			//broadcast
			for (final Map.Entry<Integer, GLNetServerHandler> entry : mClients.entrySet())
			{
				if(entry.getValue().getClientId() != clientid)
					entry.getValue().SendSerialize(gloid, glsa);
			}
		}
		catch (final IOException e)
		{}
	}
/////////////////////////
}
