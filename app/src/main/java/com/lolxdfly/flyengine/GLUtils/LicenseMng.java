package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;

public final class LicenseMng
{
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiK1MBysWBEbYMxHRAhYcxAgZgv8/gezaHhdnZDkBI6743G4ouvEH4MnooIZ+wRIvkPUfxfyywqR6N1D8NfAGBDrM2vQc3w7tnydF2d0aYE//ulU0AOlCCQytgzOPth6j6nc/iaf6otWlywMvH8saj+VvcM1DKaPYl1Kov9b4BPWrC43gXnzL58KJWBaO6vbVMnHk6uZ99AnJNWYAZDtq4zyBlQkLAY4Fej7ZP5u0Ghcjg7SugyWlsanSwQ/s1TaG2BSQ5Yybz/yQsOL0u8fcP32X3pIxbyT5RtnY9d+hRtpmopm/rnqDhGEnLGs97LUbZn+sH9I1xrlplC/74b9ypQIDAQAB";

    // Generate your own 20 random bytes, and put them here.
    private static final byte[] SALT = new byte[] {
        -7, -27, 93, 93, -123, -55, 26, -84, 27, 47, -33, -64, -27, 110, -103, -127, -81, 0, -2, 74
    };

    private final LicenseCheckerCallback mLicenseCheckerCallback;
    private final LicenseChecker mChecker;
	private final Context mContext;

    public LicenseMng(final Context c)
	{
		mContext = c;

        // Try to use more data here. ANDROID_ID is a single point of attack.
        final String deviceId = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
		
        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        // Construct the LicenseChecker with a policy.
        mChecker = new LicenseChecker(mContext, new ServerManagedPolicy(mContext, new AESObfuscator(SALT, mContext.getPackageName(), deviceId)), BASE64_PUBLIC_KEY);
        mChecker.checkAccess(mLicenseCheckerCallback);
    }

    private final class MyLicenseCheckerCallback implements LicenseCheckerCallback
	{
        public final void allow(final int policyReason)
		{
			//do nothing... just let it go
        }

        public final void dontAllow(final int policyReason)
		{
			//no License!
			final AlertDialog.Builder adb = new AlertDialog.Builder(mContext);

			if (policyReason == Policy.RETRY)
				adb.setTitle("License Check failed!");
			else
				adb.setTitle("No License found!");

			adb.setMessage("Click yes to exit!")
				.setCancelable(false) //important!
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id)
					{
						if (policyReason != Policy.RETRY)
						{
							final Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
																	   "http://market.android.com/details?id=" + mContext.getPackageName()));
							mContext.startActivity(marketIntent);
						}
						((Activity)mContext).finish();
						System.exit(-1337); //:D
					}
				});

			adb.create().show();
		}

        public final void applicationError(final int errorCode)
		{
			//happens if Developer did a Mistake
		}
    }

    public final void Destroy()
	{
        mChecker.onDestroy();
    }

}
