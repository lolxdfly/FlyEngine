package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.util.Stack;

public final class GLUnique
{
	private static final Stack<Integer> mfreeIds = new Stack<Integer>();
	private static int cur = 0;
	
	public static int getUnique()
	{
		if(!mfreeIds.isEmpty())
			return mfreeIds.pop();
		return cur++;
	}
	
	public static void freeUnique(int id)
	{
		mfreeIds.push(id);
	}
	
	public static void clear()
	{
		cur = 0;
		mfreeIds.clear();
	}
}
