package com.lolxdfly.flyengine.GLUtils.GLNet;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public interface GLSerializable
{
	public void serialize(final ObjectOutputStream oos);
	public void serialize(final ObjectInputStream ois);
}
