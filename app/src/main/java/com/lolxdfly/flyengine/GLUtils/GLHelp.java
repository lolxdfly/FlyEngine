package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.opengl.GLES20;
import android.os.Environment;
import android.os.Vibrator;
import android.text.InputType;
import android.widget.EditText;
import android.widget.Toast;

import com.lolxdfly.flyengine.GLElements.GLUIElements.GLUITextbox;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;

public final class GLHelp
{
	//const.
	public static final short MAX_TEXTURES = 16;
	public static final short BYTES_PER_SHORT = 2;
	public static final short BYTES_PER_INT = 4;
	public static final short BYTES_PER_FLOAT = 4;
	public static final short COORDS_PER_VERTEX = 2;
	public static final short COLORS_PER_VERTEX = 4;
	public static final short TEX_COORDS_PER_VERTEX = 2;
	public static final short MAX_LIGHTS = 5;
	public static final short MAX_SOUND_STREAMS = 5;

	private final Context context;
	
	private boolean bETopen = false;
	private GLUITextbox mTB;
	
	private static String sLastSavedFile = "";

	public GLHelp(final Context c)
	{
		context = c;
	}
	
	public Context getContext()
	{
		return context;
	}

	public static final Bitmap takeScreenShot(final int width, final int height, final boolean save)
	{
		final int screenshotSize = width * height;
		ByteBuffer bb = ByteBuffer.allocateDirect(screenshotSize * 4);
		bb.order(ByteOrder.nativeOrder());
		GLES20.glReadPixels(0, 0, width, height, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, bb);
		int pixelsBuffer[] = new int[screenshotSize];
		bb.asIntBuffer().get(pixelsBuffer);
		bb.clear();
		bb = null;

		for (int i = 0; i < screenshotSize; ++i)
		{
			// The alpha and green channels' positions are preserved while the red and blue are swapped
			pixelsBuffer[i] = ((pixelsBuffer[i] & 0xff00ff00)) | ((pixelsBuffer[i] & 0x000000ff) << 16) | ((pixelsBuffer[i] & 0x00ff0000) >> 16);
		}

		final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixelsBuffer, screenshotSize - width, -width, 0, 0, width, height);

		if (save)
		{
			//save somewhere..
			try
			{
				final File path = new File(Environment.getExternalStorageDirectory() + File.separator + "FlyEnigne");
				if (!path.exists())
					path.mkdirs();
				final File file = new File(path + File.separator + System.currentTimeMillis() + ".png");
				final FileOutputStream fOut = new FileOutputStream(file);
				bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
				fOut.flush();
				fOut.close();
				sLastSavedFile = file.getAbsolutePath();
			}
			catch (final Exception e) 
			{}

			//bitmap.recycle();
		}
		pixelsBuffer = null;
		return bitmap;
	}

	public final void shareScreenShot(final int width, final int height, final String msg)
	{
		takeScreenShot(width, height, true).recycle();
		final Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/png");
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(sLastSavedFile));
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        context.startActivity(Intent.createChooser(sendIntent, ""));
	}

	public final void vibrate(final int ms)
	{
		((Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(ms);
	}

	public final InputStream getFileStream(final int RID)
	{
		return context.getResources().openRawResource(RID);
	}
	
	public final String getFile(final int RID)
	{
		final BufferedReader r = new BufferedReader(new InputStreamReader(getFileStream(RID)));
		final StringBuilder total = new StringBuilder();
		String line;
		try
		{
			while ((line = r.readLine()) != null)
				total.append(line).append('\n');
		}
		catch (final IOException e)
		{}
		return total.toString();
	}
	
	public final int getRID(final String type, final String name)
	{
		return context.getResources().getIdentifier(name, type, context.getPackageName());
	}

	public final void runOnUI(final Runnable run)
	{
		((Activity)context).runOnUiThread(run);
	}

	public final void showToast(final String text, final int time)
	{
		((Activity)context).runOnUiThread(new Runnable(){
				@Override
				public final void run()
				{
					Toast.makeText(context, text, time).show();
				}
			});
	}

	public final void openEditText(final GLUITextbox gluitb)
	{
		if (bETopen)
			return;

		mTB = gluitb;
		bETopen = true;

		((Activity)context).runOnUiThread(new Runnable(){
				public final void run()
				{
					final AlertDialog.Builder builder = new AlertDialog.Builder(context);
					//builder.setTitle("Title");

					// Set up the input
					final EditText input = new EditText(context);
					// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
					input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
					input.setText(mTB.getText());
					builder.setView(input);

					// Set up the buttons
					builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){ 
							@Override
							public final void onClick(final DialogInterface dialog, final int which)
							{
								bETopen = false;
								mTB.setText(input.getText().toString());
							}
						});
					builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							@Override
							public final void onClick(final DialogInterface dialog, final int which)
							{
								dialog.cancel();
								bETopen = false;
							}
						});

					builder.show();
				}});
	}
}
