package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLSurf;

public final class GLShader
{
	/*
	 * Shader Code
	 */
	
	private static final String vs_Obj =
	"uniform mat4 uMVPMatrix;" +
	"attribute vec4 vPosition;" +
	"attribute vec4 a_Color;" +
	"varying vec4 v_Color;" + 
	"void main() {" +
	"  gl_Position = uMVPMatrix * vPosition;" +
	"  v_Color = a_Color;" +
	"  gl_PointSize = 5.0;" +
	"}";
	private static final String fs_Obj =
	"precision mediump float;" +
	"varying vec4 v_Color;" +
	"void main() {" +
	"  gl_FragColor = v_Color;" +
	"}";
	
	private static final String vs_Texture =
	"uniform mat4 uMVPMatrix;" +
	"attribute vec4 vPosition;" +
	"attribute vec4 a_Color;" +
	"attribute vec2 a_texCoord;" +
	"varying vec4 v_Color;" + 
	"varying vec2 v_texCoord;" +
	"void main() {" +
	"  gl_Position = uMVPMatrix * vPosition;" +
	"  v_texCoord = a_texCoord;" +
	"  v_Color = a_Color;" + 
	"}";
	private static final String fs_Texture =
	"precision mediump float;" +
	"varying vec4 v_Color;" +
	"varying vec2 v_texCoord;" +
	"uniform sampler2D s_texture;" +
	"void main() {" +
	"  gl_FragColor = texture2D(s_texture, v_texCoord) * v_Color;" +
	"  gl_FragColor.rgb *= v_Color.a;" +
	"}";  

	private static final String vs_PostProcessing =
	"uniform mat4 uMVPMatrix;" +
	"attribute vec4 vPosition;" +
	"void main() {" +
	"  gl_Position = uMVPMatrix * vPosition;" +
	"}";
	private static final String fs_PostProcessing =
	"precision mediump float;" +
	"uniform sampler2D s_texture;" +
	"float BlurFactor = 12.0;" +
	"vec2 radial_size = vec2(1/1920, 1/1080);" +
	"float radial_blur = 0.08;" +
	"float radial_bright = 1.0;" +
	"vec2 radial_origin = vec2(0.5, 0.5);" +
	"void main() {" +
	"  vec4 SumColor = vec4(0.0, 0.0, 0.0, 0.0);" +
	"  vec2 TexCoord = gl_FragCoord.xy / vec2(1920, 1080);" +
	"  TexCoord += radial_size * 0.5 - radial_origin;" +
	"  for(int i = 0; i < int(BlurFactor); i++) {" +
	"    float scale = 1.0 - radial_blur * (float(i) / (BlurFactor - 1.0));" +
	"    SumColor += texture2D(s_texture, TexCoord * scale + radial_origin);" +
	"  }" +
	"  gl_FragColor = SumColor / (BlurFactor - 1.0) * radial_bright;" +
	"}";
	
	private static final String vs_Lighting =
	"uniform mat4 uMVPMatrix;" +
	"attribute vec4 vPosition;" +
	"void main() {" +
	"	gl_Position = uMVPMatrix * vPosition;" +
	"}";
	private static final String fs_Lighting =
	"precision mediump float;" +
	"uniform vec3 uLightPos[" + GLHelp.MAX_LIGHTS + "];" +
	"uniform vec3 uLightColor[" + GLHelp.MAX_LIGHTS + "];" +
	"uniform sampler2D s_texture;" +
	"void main() {" +
	"   vec2 v_texCoord = gl_FragCoord.xy / vec2(1920, 1080);" +
	"	gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);" +
	"	for(int i = 0; i < " + GLHelp.MAX_LIGHTS + "; i++){" +
	"		if(uLightPos[i].z == 0.0) break;" +
	"		float d = distance(gl_FragCoord.xy / 1080.0, uLightPos[i].xy / 1080.0) * 1080.0;" +
	"		float att = (1.0 / (0.05 + (0.001 * d) + (0.0001 * d * d))) * uLightPos[i].z;" +
	"		gl_FragColor += vec4(att * uLightColor[i].rgb, 1.0);" +
	"	}" +
	"	gl_FragColor *= texture2D(s_texture, v_texCoord);" +
	"}";
	
	private static final String vs_Phong =
	"uniform mat4 uMVPMatrix;" +
	"attribute vec4 vPosition;" +
	"attribute vec4 a_Color;" +
	"attribute vec2 a_texCoord;" +
	"varying vec4 v_Color;" +
	"varying vec2 v_texCoord;" +
	"void main() {" +
	"	gl_Position = uMVPMatrix * vPosition;" +
	"	v_texCoord = a_texCoord;" +
	"	v_Color = a_Color;" + 
	"}";
	private static final String fs_Phong =
	"precision highp float;" +
	"varying vec4 v_Color;" +
	"varying vec2 v_texCoord;" +
	"uniform vec3 uLightPos[" + GLHelp.MAX_LIGHTS + "];" +
	"uniform vec3 uLightColor[" + GLHelp.MAX_LIGHTS + "];" +
	"uniform vec3 uCamPos;" +
	"uniform sampler2D s_texture;" +
	"uniform sampler2D s_normal;" +
	"void main() {" +
	"	const float uCAmbient = 0.4f;" +
	"	const float uCDiffuse = 0.65f;" +
	"	const float uCSpecular = 0.0f;" +
	"	const float uShine = 150.0f;" +
	"	vec3 n = texture2D(s_normal, v_texCoord).rgb * 2.0 - 1.0;" +
	"	vec3 pos = gl_FragCoord.xyz;" +
	"	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);"+
	"	for(int i = 0; i < " + GLHelp.MAX_LIGHTS + "; i++){ " +
	"		if(uLightPos[i].z == 0.0) continue;" +
	"		vec3 l = normalize(uLightPos[i] - pos); " +
	"		float cos_theta = max(dot(n, l), 0.0); " +
	"		float diffuseTerm = uCDiffuse * cos_theta; " +
	"		vec3 v = normalize(uCamPos - pos);" +
	"		vec3 h = normalize(l + v); " +
	"		float cos_nh = max(dot(n, h), 0.0);" +
	"		float specularTerm = uCSpecular * pow(cos_nh, uShine);" +
	"		float phong = uCAmbient + diffuseTerm + specularTerm;" +
	"		color += vec4(uLightColor[i] * phong, 1.0) * texture2D(s_texture, v_texCoord) * v_Color;" +
	"	}" +
	"	gl_FragColor = color;" +
	"}";
	
	
	/*
	 * Shader Classes
	 */
	 
	public static class GLShaderProgram
	{
		public final int mProgram;
		
		public GLShaderProgram(final int program)
		{
			mProgram = program;
		}
	}
	public static class pObj extends GLShaderProgram
	{
		public final int mPosHandle;
		public final int mColorHandle;
		public final int mMatrixHandle;
		
		public pObj(final int program)
		{
			super(program);
		
			mPosHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
			mColorHandle = GLES20.glGetAttribLocation(mProgram, "a_Color");
			mMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
		}
	}
	public static class pTexture extends pObj
	{
		public final int mTexCoordHandle;
		public final int mSamplerHandle;
		
		public pTexture(final int program)
		{
			super(program);

			mTexCoordHandle = GLES20.glGetAttribLocation(mProgram, "a_texCoord");
			mSamplerHandle = GLES20.glGetUniformLocation(mProgram, "s_texture");
		}
	}
	public static class pPostProcessing extends GLShaderProgram
	{
		public final int mPosHandle;
		public final int mSamplerHandle;
		public final int mMatrixHandle;

		public pPostProcessing(final int program)
		{
			super(program);

			mPosHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
			mSamplerHandle = GLES20.glGetUniformLocation(mProgram, "s_texture");
			mMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
		}
	}
	public static final class pLighting extends pPostProcessing
	{
		public final int mLightPosHandle;
		public final int mLightColorHandle;

		public pLighting(final int program)
		{
			super(program);

			mLightPosHandle = GLES20.glGetUniformLocation(mProgram, "uLightPos");
			mLightColorHandle = GLES20.glGetUniformLocation(mProgram, "uLightColor");
		}
	}
	public static final class pPhong extends pTexture
	{
		public final int mLightPosHandle;
		public final int mLightColorHandle;
		public final int mCamPosHandle;
		public final int mNormalSamplerHandle;

		public pPhong(final int program)
		{
			super(program);

			mCamPosHandle = GLES20.glGetUniformLocation(mProgram, "uCamPos");
			mLightPosHandle = GLES20.glGetUniformLocation(mProgram, "uLightPos");
			mLightColorHandle = GLES20.glGetUniformLocation(mProgram, "uLightColor");
			mNormalSamplerHandle = GLES20.glGetUniformLocation(mProgram, "s_normal");
		}
	}
	
	
	/*
	 * Shader instances
	 */
	 
	public static pObj sProgram_Obj;
	public static pTexture sProgram_Texture;
	public static pLighting sProgram_Lighting;
	public static pPostProcessing sProgram_PostProcessing;
	public static pPhong sProgram_Phong;
	
	
    private static int loadShader(final int type, final String shaderCode)
	{
        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        final int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
		
		/*final String err = GLES20.glGetShaderInfoLog(shader);
		Log.e("GLSL-Error", err);
		if(!err.isEmpty())
			throw new RuntimeException(err);*/

        // return the shader
        return shader;
    }

	private static int loadProgram(final int vs, final int fs)
	{
        // prepare shaders and OpenGL program
		final int vertexShader = GLShader.loadShader(GLES20.GL_VERTEX_SHADER, GLSurf.mHelper.getFile(vs));
        final int fragmentShader = GLShader.loadShader(GLES20.GL_FRAGMENT_SHADER, GLSurf.mHelper.getFile(fs));

        final int mProgram = GLES20.glCreateProgram();   // create empty OpenGL Program
        GLES20.glAttachShader(mProgram, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(mProgram, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(mProgram);

		//release shader objects
		GLES20.glDeleteShader(vertexShader);
		GLES20.glDeleteShader(fragmentShader);
		
		return mProgram;
	}
	
	private static int loadProgram(final String vs, final String fs)
	{
        // prepare shaders and OpenGL program
        final int vertexShader = GLShader.loadShader(
			GLES20.GL_VERTEX_SHADER, vs);
        final int fragmentShader = GLShader.loadShader(
			GLES20.GL_FRAGMENT_SHADER, fs);

        final int mProgram = GLES20.glCreateProgram();   // create empty OpenGL Program
        GLES20.glAttachShader(mProgram, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(mProgram, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(mProgram);
		
		//release shader objects
		GLES20.glDeleteShader(vertexShader);
		GLES20.glDeleteShader(fragmentShader);
		
		return mProgram;
	}

	public static final void InitShaders()
	{
		sProgram_Obj = new pObj(loadProgram(vs_Obj, fs_Obj));
		sProgram_Texture = new pTexture(loadProgram(vs_Texture, fs_Texture));
		sProgram_Lighting = new pLighting(loadProgram(vs_Lighting, fs_Lighting));
		sProgram_PostProcessing = new pPostProcessing(loadProgram(vs_PostProcessing, fs_PostProcessing));
		sProgram_Phong = new pPhong(loadProgram(vs_Phong, fs_Phong));
	}
	
	public static final void Destroy()
	{
		GLES20.glReleaseShaderCompiler();
	}
}
