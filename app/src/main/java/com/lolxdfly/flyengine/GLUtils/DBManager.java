package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class DBManager extends SQLiteOpenHelper
{
	public DBManager(final Context context) 
	{
        super(context, "db_flyengine", null, 1);
	}

    @Override
    public final void onCreate(final SQLiteDatabase db)
	{
        // SQL statement to create book table
        /*String CREATE_BOOK_TABLE = "CREATE TABLE books ( " +
		 "id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
		 "title TEXT, "+
		 "author TEXT )";

		 // create books table
		 db.execSQL(CREATE_BOOK_TABLE);*/
    }

    @Override
    public final void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) 
	{
        // Drop older books table if existed
        /*db.execSQL("DROP TABLE IF EXISTS books");

		 // create fresh books table
		 onCreate(db);*/
    }
}
