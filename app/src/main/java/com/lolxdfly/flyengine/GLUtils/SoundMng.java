package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.view.KeyEvent;

import com.lolxdfly.flyengine.GLMain.GLSurf;

import java.util.ArrayList;

public final class SoundMng
{
	private SoundPool mPlayer;
	private final AudioManager mAudioMng;
	private ArrayList<Integer> mSndIDs;
	private short nSnds = 0;
	private final Context mContext;

	private float mVolumeAct;
	private final float mVolumeMax;
	private float mVolume;

	public SoundMng(final Context c)
	{
		mContext = c;

		mPlayer = new SoundPool.Builder().setMaxStreams(GLHelp.MAX_SOUND_STREAMS).build();
		GLSurf.mHelper.runOnUI(new Runnable()
			{
				@Override
				public void run()
				{
					((Activity)mContext).setVolumeControlStream(AudioManager.STREAM_MUSIC);
				}
			});
	 	mAudioMng = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		mSndIDs = new ArrayList<Integer>();

		mVolumeMax = mAudioMng.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		updateVolume();
	}

	public final boolean onKeyDown(final int keyCode, final KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
		{
			mAudioMng.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, 0);
			updateVolume();
		}
		else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
		{
			mAudioMng.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
			updateVolume();
		}
		return true;
	}

	public final void Destroy()
	{
		mPlayer.release();
		mPlayer = null;
		mSndIDs.clear();
		mSndIDs = null;
	}

	public final void loadSnd(final int RID)
	{
		if (mSndIDs.contains(RID))
			return;

		mPlayer.load(mContext, RID, 1);
		mSndIDs.add(RID);
		nSnds++;
	}

	private final int getSndID(final int RID)
	{
		return mSndIDs.indexOf(RID) + 1;
	}

	public final void PlaySnd(final int RID, final boolean loop)
	{
		final int id = getSndID(RID);
		if (loop)
			mPlayer.play(id, mVolume, mVolume, 1, -1, 1.0f);
		else
			mPlayer.play(id, mVolume, mVolume, 1, 0, 1.0f);
	}

	public final void setVolume(final float v)
	{
		for (int i = nSnds; i >= 1; i--)
			mPlayer.setVolume(i, v, v);
	}

	public final void setVolume(final float v, final int id)
	{
		mPlayer.setVolume(id, v, v);
	}

	public final void setVolume(final float l, final float r, final int id)
	{
		mPlayer.setVolume(id, l, r);
	}

	private final void updateVolume()
	{
		mVolumeAct = mAudioMng.getStreamVolume(AudioManager.STREAM_MUSIC);
		mVolume = mVolumeAct / mVolumeMax;
	}
}
