package com.lolxdfly.flyengine.GLUtils;

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLPostProcessor
{
	private final int framebuffer[] = new int[1];
	private final int mTextureID;

	protected int mProgram;
	protected int mPosHandle;
	protected int mSamplerHandle;
	protected int mMatrixHandle;
	
	private boolean bAplha = false;

	private final int buffers[] = new int[1];

	public GLPostProcessor()
	{
		//View
		final float mRect[] = new float[]
		{
			0.0f, GLSurf.mCamera.mScreenHeight,	// left bottom
			GLSurf.mCamera.mScreenWidth, GLSurf.mCamera.mScreenHeight,	// right bottom
			0.0f,   0.0f,	// left top
			GLSurf.mCamera.mScreenWidth,   0.0f	// right top
		};
		
		//Buffers
		final ByteBuffer bbvertexbuff = ByteBuffer.allocateDirect(8 * GLHelp.BYTES_PER_FLOAT);
		bbvertexbuff.order(ByteOrder.nativeOrder());
		final FloatBuffer vertexBuffer = bbvertexbuff.asFloatBuffer();
		vertexBuffer.put(mRect);
		vertexBuffer.position(0);
		
		//VBO
		GLES20.glGenBuffers(1, buffers, 0);						
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity() * GLHelp.BYTES_PER_FLOAT, vertexBuffer, GLES20.GL_STATIC_DRAW);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		vertexBuffer.clear();
		
		//set default Program
		mProgram = GLShader.sProgram_PostProcessing.mProgram;
		mPosHandle = GLShader.sProgram_PostProcessing.mPosHandle;
		mSamplerHandle = GLShader.sProgram_PostProcessing.mSamplerHandle;
		mMatrixHandle = GLShader.sProgram_PostProcessing.mMatrixHandle;
		
		//create empty Texture
		mTextureID = GLSurf.mTexMng.createNewEmpty((int)GLSurf.mCamera.mScreenWidth, (int)GLSurf.mCamera.mScreenHeight);

		//gen Framebuffer
		GLES20.glGenFramebuffers(1, framebuffer, 0);

		//bind
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, framebuffer[0]);

		//enable framebuffer transparency
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFuncSeparate(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA, GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		
		//set Texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, mTextureID, 0);

		//unbind
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	}
	
	public final void setEnableAlpha(final boolean b)
	{
		bAplha = b;
	}

	public void Destory()
	{
		GLES20.glDeleteBuffers(1, buffers, 0);
		GLES20.glDeleteFramebuffers(1, framebuffer, 0);
	}

	public final void beginRender()
	{
		//bind framebuffer
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, framebuffer[0]);

		//reset Viewport and clear
		GLES20.glViewport(0, 0, 1920, 1080);
		if(bAplha)
			GLES20.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}

	public final void endRender()
	{
		//unbind
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	}

	public void Render()
	{
		GLES20.glUseProgram(mProgram);
        
		GLES20.glEnableVertexAttribArray(mPosHandle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
		GLES20.glVertexAttribPointer(mPosHandle, GLHelp.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);//vertexBuffer);

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureID);
		GLES20.glUniform1i(mSamplerHandle, 0);
		
		GLES20.glUniformMatrix4fv(mMatrixHandle, 1, false, GLCamera.mtrxUI, 0);

		RenderInline();
		
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, GLRect.mStdIdxBuff[0]);
		//GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, drawOrder.length);
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, GLRect.drawOrder.length, GLES20.GL_UNSIGNED_BYTE, 0);

		// Clear the currently bound buffer (so future OpenGL calls do not use this buffer).
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(mPosHandle);
	}
	
	protected void RenderInline(){}

	public final int getTextureID()
	{
		return mTextureID;
	}
}
