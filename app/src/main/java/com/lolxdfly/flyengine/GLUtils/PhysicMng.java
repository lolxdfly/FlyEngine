package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.graphics.RectF;

import com.lolxdfly.flyengine.GLElements.GLMover;
import com.lolxdfly.flyengine.GLMain.GLCamera;

import java.util.ArrayList;

public final class PhysicMng
{
	private final ArrayList<GLMover> mMover;
	private int nMover = 0;
	private float g = 9.81f;
	private float mGravFactor;
	private float mGround;

	public PhysicMng()
	{
		mMover = new ArrayList<GLMover>();
		mGravFactor = 0.5f * g;
		mGround = GLCamera.mVirtualScreenHeight;
	}

	public final void attachGravity(final GLMover mover)
	{
		if (mMover.contains(mover))
			return;

		mover.mtime = 0;
		mMover.add(mover);
		nMover++;
	}

	public final void deattachGravity(final GLMover mover)
	{
		mMover.remove(mover);
		nMover--;
	}

	public final void Update(final long elapsed)
	{
		for (int i = 0; i < nMover; i++)
			calcGravity(mMover.get(i), elapsed);
	}

	private final void calcGravity(final GLMover mover, final long elapsed)
	{
		final RectF r = mover.getRect();

		if (r.bottom >= mGround)
		{
			mover.mtime = 0;
			return;
		}

		final float s = mGravFactor * (float)Math.pow(mover.mtime / 1000.0f, 2);
		r.top += s;
		r.bottom += s;

		mover.setRect(r);
		mover.mtime += elapsed;
	}

	public final void setGravity(final float grav)
	{
		g = grav;
		mGravFactor = 0.5f * g;
	}

	public final void setGround(final float gzero)
	{
		mGround = gzero;
	}
}
