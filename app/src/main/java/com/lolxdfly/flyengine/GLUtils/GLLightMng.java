package com.lolxdfly.flyengine.GLUtils;

import android.opengl.GLES20;

import com.lolxdfly.flyengine.GLMain.GLCamera;
import com.lolxdfly.flyengine.GLMain.GLSurf;

public final class GLLightMng extends GLPostProcessor
{
	private final int mLightPosHandle;
	private final int mLightColorHandle;
	
	private final float mLightPos[] = new float[GLHelp.MAX_LIGHTS * 3];
	private final float mLightColor[] = new float[GLHelp.MAX_LIGHTS * 3];
	private final float mLightFinalPos[] = new float[GLHelp.MAX_LIGHTS * 3];
	private short sLiD = -1;
	
	public GLLightMng()
	{
		super();
		
		mProgram = GLShader.sProgram_Lighting.mProgram;
		mPosHandle = GLShader.sProgram_Lighting.mPosHandle;
		mSamplerHandle = GLShader.sProgram_Lighting.mSamplerHandle;
		mMatrixHandle = GLShader.sProgram_Lighting.mMatrixHandle;
		mLightPosHandle = GLShader.sProgram_Lighting.mLightPosHandle;
		mLightColorHandle = GLShader.sProgram_Lighting.mLightColorHandle;
	}
	
	public final float[] getLightInfo()
	{
		return mLightPos;
	}
	
	@Override
	protected final void RenderInline()
	{
		for (short s = 0; s <= sLiD; s++)
		{
			final float tmp[] = new float[]{mLightPos[s * 3], mLightPos[s * 3 + 1]};
			GLSurf.mCamera.transformVec(tmp);
			mLightFinalPos[s * 3] = tmp[0];//mLightPos[s * 3] * (float)Math.cos(d) - mLightPos[s * 3 + 1] * (float)Math.sin(d);// - GLSurf.mCamera.getX() + (float)Math.cos(d + startrot) * (float)dlen;//(mLightPos[s * 3] - GLSurf.mCamera.getX() - (GLSurf.mCamera.mScreenWidth / 2));
			mLightFinalPos[s * 3 + 1] = tmp[1];//mLightPos[s * 3] * (float)Math.sin(d) + mLightPos[s * 3 + 1] * (float)Math.sin(d);// + GLSurf.mCamera.getY() + (float)Math.sin(d + startrot) * (float)dlen;//(mLightPos[s * 3 + 1] + GLSurf.mCamera.getY() - (GLSurf.mCamera.mScreenHeight / 2));
		}

		GLES20.glUniform3fv(mLightPosHandle, GLHelp.MAX_LIGHTS, mLightFinalPos, 0);
		GLES20.glUniform3fv(mLightColorHandle, GLHelp.MAX_LIGHTS, mLightColor, 0);
	}
	
	public final void addLight(final int lID, final float pos[], final float r, final float color[])
	{
		if (lID > sLiD)
		{
			addLight(pos, r, color);
			return;
		}

		mLightPos[lID * 3] = pos[0];
		mLightPos[lID * 3 + 1] = pos[1];
		//mLightPos[lID * 3 + 2] = r * ((GLSurf.mCamera.mScaleX + GLSurf.mCamera.mScaleY)/2);
		mLightFinalPos[lID * 3 + 2] = r * ((GLCamera.mScaleX + GLCamera.mScaleY) / 2); //not perfect
		mLightColor[lID * 3] = color[0];
		mLightColor[lID * 3 + 1] = color[1];
		mLightColor[lID * 3 + 2] = color[2];
	}

	public final void addLight(final float pos[], final float r, final float color[])
	{
		if (sLiD == GLHelp.MAX_LIGHTS) return;

		sLiD++;
		mLightPos[sLiD * 3] = pos[0];
		mLightPos[sLiD * 3 + 1] = pos[1];
		mLightPos[sLiD * 3 + 2] = r * ((GLCamera.mScaleX + GLCamera.mScaleY)/2);
		mLightFinalPos[sLiD * 3 + 2] = mLightPos[sLiD * 3 + 2]; //r * ((GLSurf.mCamera.mScaleX + GLSurf.mCamera.mScaleY) / 2); //not perfect
		mLightColor[sLiD * 3] = color[0];
		mLightColor[sLiD * 3 + 1] = color[1];
		mLightColor[sLiD * 3 + 2] = color[2];
	}

	public final void turnoffLight(final int lID)
	{
		mLightFinalPos[lID * 3 + 2] = 0;
	}

	public final void turnonLight(final int lID)
	{
		mLightFinalPos[lID * 3 + 2] = mLightPos[lID * 3 + 2];
	}
}
