package com.lolxdfly.flyengine.GLUtils.GLNet;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;

public abstract class GLBlue extends Thread
{
	protected BluetoothAdapter mBluetoothAdapter;
	protected static final GLSync mServerSync = new GLSync();
	protected final Context context;
	
	public GLBlue(final Context c)
	{
		context = c;
	}
	
	public void connect()
	{
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		/*if (!mBluetoothAdapter.isEnabled())
		{
			final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			((Activity)context).startActivity(enableBtIntent);
		}*/
		final Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 60);
		context.startActivity(discoverableIntent);
		
		//block
		while(!mBluetoothAdapter.isEnabled()){}
		
		//todo:
		//check onActivityResult for BT deny (startActivity => startActivityforResult)
	}
	
	public boolean isEnabled()
	{
		return mBluetoothAdapter.isEnabled();
	}
}
