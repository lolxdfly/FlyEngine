package com.lolxdfly.flyengine.GLUtils;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.content.Context;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.lolxdfly.flyengine.GLMain.GLSurf;

public final class AdvertisingMng
{
	private final AdView adv;
	private final RelativeLayout mRL;

	public AdvertisingMng(final Context c, final RelativeLayout rl,final String UnitId, final AdSize ads, final RelativeLayout.LayoutParams rlp)
	{

		MobileAds.initialize(c, UnitId);

		mRL = rl;
		adv = new AdView(c);
		adv.setAdUnitId(UnitId);
		adv.setAdSize(ads);
		adv.setLayoutParams(rlp);
		final AdRequest ar = new AdRequest.Builder().build(); //.addTestDevice("64F7CC7CC83B9FA0DEB6B8BE05954ED4").build();
		adv.loadAd(ar);
	}

	public final void attach()
	{
		GLSurf.mHelper.runOnUI(new Runnable(){
				public final void run()
				{
					mRL.addView(adv);
				}
			});
	}

	public final void disattach()
	{
		GLSurf.mHelper.runOnUI(new Runnable(){
				public final void run()
				{
					mRL.removeView(adv);
				}
			});
	}
	
	public final void setState(boolean pause)
	{
		if(pause)
			adv.pause();
		else
			adv.resume();
	}
	
	public final void Destroy()
	{
		adv.destroy();
	}
}
