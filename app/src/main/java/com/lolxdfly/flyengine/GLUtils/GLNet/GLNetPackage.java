package com.lolxdfly.flyengine.GLUtils.GLNet;

public abstract class GLNetPackage
{
	public static final byte EOP = -2;
	
	public static final byte PACKAGE_DISCONNECTED = -1;
	public static final byte PACKAGE_SERIALIZE = 0;
}
