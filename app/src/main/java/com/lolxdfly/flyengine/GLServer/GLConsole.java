package com.lolxdfly.flyengine.GLServer;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import com.lolxdfly.flyengine.MainServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class GLConsole extends Thread
{

	public GLConsole()
	{
		start();
	}

	@Override
	public final void run()
	{
		final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String input = "";
		for (;;)
		{
			System.out.print("=>");
			try
			{
				input = reader.readLine();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			if (input.equals("stop"))
				new Thread(){public final void run() { MainServer.mSock.stopServer();}}.start();
				//execute(MainServer.mSock::stopServer);
			else if (input.equals("exit"))
				System.exit(0);
		}
	}

	private final void execute(final Runnable r)
	{
		new Thread(r).start();
	}

	public static final void log(final String log)
	{
		System.out.print(log + "\n=>");
	}
}
