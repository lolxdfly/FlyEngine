package com.lolxdfly.flyengine.GLServer;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import com.lolxdfly.flyengine.GLUtils.GLNet.GLNetServerHandler;
import com.lolxdfly.flyengine.GLUtils.GLNet.GLSync;
import com.lolxdfly.flyengine.GLUtils.GLNet.GLUnique;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class GLSocket extends Thread{
	
    private ServerSocket serverSocket;
    public static final GLSync mServerSync = new GLSync();
    private final Map<Integer, GLNetServerHandler> clients;
	private final ExecutorService executorService;
	
	public GLSocket(int serverPort)
    {
        GLConsole.log("Starting Server...");
		
		//120 sec Timeout
		executorService = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
												 120L, TimeUnit.SECONDS,
												 new SynchronousQueue<Runnable>());
		clients = new HashMap<Integer, GLNetServerHandler>();
		try {
			serverSocket = new ServerSocket(serverPort);
		} catch (IOException e) {
			GLConsole.log("Error starting Server on " + serverPort + "!");
			e.printStackTrace();
		}
        start();
        GLConsole.log("Server has started!");
    }
    
    //Call the method when you want to stop your server
    public final void stopServer() 
    {
		GLConsole.log("Stopping Server...");
		
        //Stop the executor service.
        executorService.shutdownNow();
        
        //Stop thread
        interrupt();
    }

	@Override
	public final void run() 
	{
		for(;;) 
		{
            try 
            {
            	final Socket s = serverSocket.accept();
    			if (isInterrupted())
    			{
    				s.close();
    				break;
    			}
    			
            	GLConsole.log("Processing connection: " + s.getInetAddress().getHostAddress());
                
				final int id = GLUnique.getUnique();
				final GLNetServerHandler glnsh = new GLNetServerHandler(s.getInputStream(), s.getOutputStream(), clients, id);
				clients.put(id, glnsh);
				executorService.submit(glnsh);
			}
            catch(final IOException ioe) 
            {
            	GLConsole.log("Error accepting connection:");
                ioe.printStackTrace();
            }
        }
		GLConsole.log("Server has stopeed!");
	}
}
