package com.lolxdfly.flyengine.GLMain;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.GLLine;
import com.lolxdfly.flyengine.GLElements.GLPoint;
import com.lolxdfly.flyengine.GLElements.GLRect;
import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLUtils.GLShader;

import javax.microedition.khronos.opengles.GL10;

public final class GLRender implements GLSurfaceView.Renderer
{
    private long mLastTime;
	private static boolean enter = true;
	private short bTouch = 0;
	// 0 = noTouch
	// 1 = Touch
	// 2 = TouchEnd
	private MotionEvent mEvent;
	private static WndBase g_entry;
	private static Class entryclass;

    public GLRender(final WndBase entry, final Class c)
    {
        mLastTime = System.currentTimeMillis() + 100;
		g_entry = entry;
		entryclass = c;
    }

    public final void onPause()
    {
        /* Do stuff to pause the renderer */
    }

    public final void onResume()
    {
        /* Do stuff to resume the renderer */
        mLastTime = System.currentTimeMillis();
    }

	public final void Destroy()
	{
		GLSurf.mWndMng.Destroy(); //first Wnds + their Objects
		GLSurf.mTexMng.Destroy(); //than all textures (if not already released)
		//GLSurf.mGPlay.Destroy(); //than others
		GLSurf.mSndMng.Destroy();
		GLSurf.mAnimMng.Destroy();
		//GLSurf.mAMng.Destroy();
		GLShader.Destroy();

		//release std buffers
		GLRect.ReleaseStdIdxBuff();
		GLLine.ReleaseStdIdxBuff();
		GLPoint.ReleaseStdIdxBuff();
	}

	public final void onTouchEvent(final MotionEvent event)
	{
		if (event.getAction() != MotionEvent.ACTION_UP)
			bTouch = 1;
		else
			bTouch = 2;

		mEvent = event;
	}

    @Override
    public final void onDrawFrame(GL10 unused)
	{
        // Get the current time
     	final long now = System.currentTimeMillis();
		//if (mLastTime >= now) return;

        // Get the amount of time the last frame took.
        final long elapsed = now - mLastTime;

        // Update our example
		Update(elapsed);

        // Render our example
        Render();

		mLastTime = now;
    }

	private final void Update(final long elapsed)
	{
		/*int err;
		 while ((err = GLES20.glGetError()) != GLES20.GL_NO_ERROR)
		 throw new RuntimeException("" + err);*/

		if (bTouch != 0)
			GLSurf.mWndMng.processTouch(mEvent);

		if (bTouch == 2)
			bTouch = 0;

		GLSurf.mWndMng.Update(elapsed);
	}

    private final void Render()
	{
		//clear Color (only if render call does not override everything)
		//GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

		//Render all
		GLSurf.mWndMng.Render();
	}

    @Override
    public final void onSurfaceChanged(final GL10 gl, final int width, final int height)
	{
		GLSurf.mCamera.resize(width, height);

		// WndEntry
		if (enter) //enter only once
		{
			try
			{
				//call specific constructor
				g_entry = (WndBase)entryclass.newInstance();
			}
			catch (final InstantiationException | IllegalAccessException | IllegalArgumentException e)
			{}
			GLSurf.mWndMng.registerWnd(g_entry);
			//GLSurf.mAMng.attach();
			enter = false;
		}
	}

    @Override
    public final void onSurfaceCreated(final GL10 gl, final javax.microedition.khronos.egl.EGLConfig config)
	{
		// Set the clear color to black
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// enable Z Buffer for GL3DPoly if neccessary
		// this crashes the display driver
		// however GL3DPoly seems to work without this
		/*GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glDepthFunc(GLES20.GL_LEQUAL);
		GLES20.glDepthMask(true);
		GLES20.glClearDepthf(1.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT); */

		// enable alpha transparency
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		// enable backface culling
		GLES20.glEnable(GLES20.GL_CULL_FACE);
		GLES20.glCullFace(GLES20.GL_BACK);

		//shaders
		GLShader.InitShaders();

		//standardbuffers
		GLRect.genStdBuffers();
		GLLine.genStdBuffers();
		GLPoint.genStdBuffers();

		// textures
		GLSurf.mTexMng.init();
	}
}
