package com.lolxdfly.flyengine.GLMain;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;

public final class GLTextureMng {

	// Binding IDs
	private final int mTextureIDs[];

	// free Binding IDs
	private final Stack<Integer> mFreedTextureIDs;

	// usage Counter for Textures
	private final int mTextureUsings[];

	// Map from RID to Binding ID
	private final HashMap<Integer, Integer> m_Textures;

	// current ID
	private int mCurrBindID = 0;

	// context for bmp loader
	private final Context mContext;

	public GLTextureMng(final Context ctx)
	{
		mContext = ctx;
		mTextureIDs = new int[GLHelp.MAX_TEXTURES];
		mTextureUsings = new int[GLHelp.MAX_TEXTURES];
		mFreedTextureIDs = new Stack<Integer>();
		m_Textures = new HashMap<Integer, Integer>();
		Arrays.fill(mTextureUsings, 0);
	}

	public final void init()
	{
		GLES20.glGenTextures(GLHelp.MAX_TEXTURES, mTextureIDs, 0);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

		//load default texture
		loadTexture(R.drawable.texdef);
	}

	public final int loadTexture(final int RID)
	{
		// Texture already loaded?
		if (m_Textures.containsKey(RID))
		{
			final int idx = m_Textures.get(RID);
			mTextureUsings[idx - 1]++;
			return idx;
		}

		// get new Binding ID
		final int bindID = getFreeBindId();

		// Temporary create a bitmap
		final Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(), RID);

		// Bind ID
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, bindID);

		// Set filtering
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,
				GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER,
				GLES20.GL_LINEAR);

		// Set wrapping mode
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
				GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
				GLES20.GL_CLAMP_TO_EDGE);

		// Load the bitmap into the bound texture.
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

		// unbind
		//GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

		// We are done using the bitmap so we should recycle it.
		bmp.recycle();

		// bindID starts at 1, but we want to be 0 based at m_textureUsings
		mTextureUsings[bindID - 1]++;

		// map RID -> bindID
		m_Textures.put(RID, bindID);
		// map bindID -> RID (safe, because bindID never near RID)
		m_Textures.put(bindID, RID);

		return bindID;
	}

	public final int createNewEmpty(final int width, final int height)
	{
		final int bindID = getFreeBindId();

		// Bind ID
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, bindID);

		// Set filtering
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,
				GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER,
				GLES20.GL_NEAREST);

		// Set wrapping mode
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
				GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
				GLES20.GL_CLAMP_TO_EDGE);

		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width, height, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);

		mTextureUsings[bindID - 1] = -1; // special bmp load
		return bindID;
	}

	public final int loadTexture(final Bitmap bmp)
	{
		final int bindID = getFreeBindId();

		// Bind ID
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, bindID);

		// Set filtering
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,
				GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER,
				GLES20.GL_LINEAR);

		// Set wrapping mode
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
				GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
				GLES20.GL_CLAMP_TO_EDGE);

		// Load the bitmap into the bound texture.
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

		// We are done using the bitmap so we should recycle it.
		bmp.recycle();

		mTextureUsings[bindID - 1] = -1; // special bmp load
		return bindID;
	}

	public final int getTextureID(final int RID)
	{
		return m_Textures.get(RID);
	}

	public final Bitmap getBitmap(final int RID)
	{
		return BitmapFactory.decodeResource(mContext.getResources(), RID);
	}

	private final int getFreeBindId()
	{
		if (mFreedTextureIDs.isEmpty())
		{
			return mTextureIDs[mCurrBindID++]; // if crash here: too many Texture loaded!
		}

		return mFreedTextureIDs.pop();
	}

	public final void ReleaseTexture(final int bindID)
	{
		// check valid bindingID
		if (bindID == 0)
			return;

		// decrement 0 based Usings
		if (mTextureUsings[bindID - 1] > 0) // don't decrement special bmp load
			mTextureUsings[bindID - 1]--;

		if (mTextureUsings[bindID - 1] == 0) // no Usages anymore => free it
		{
			final int tmp[] = new int[1];
			tmp[0] = bindID;
			GLES20.glDeleteTextures(1, tmp, 0);
			m_Textures.remove(m_Textures.get(bindID)); // remove RID -> bindID
			m_Textures.remove(bindID); // remove bindID -> RID
			mFreedTextureIDs.add(bindID);
		}
		else if (mTextureUsings[bindID - 1] == -2) // special bmp load
		{
			final int tmp[] = new int[1];
			tmp[0] = bindID;
			GLES20.glDeleteTextures(1, tmp, 0);
			mFreedTextureIDs.add(bindID);
		}
	}

	public final void Destroy()
	{
		GLES20.glDeleteTextures(m_Textures.size() / 2, mTextureIDs, 0);
		m_Textures.clear();
		mFreedTextureIDs.clear();
	}
}
