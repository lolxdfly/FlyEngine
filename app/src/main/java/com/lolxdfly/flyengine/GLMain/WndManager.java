package com.lolxdfly.flyengine.GLMain;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.WndBase;

import java.util.ArrayList;

public final class WndManager
{
	private final ArrayList<WndBase> vWndPool;

	public WndManager()
	{
		vWndPool = new ArrayList<WndBase>();
	}

	public final void Destroy()
	{
		for (WndBase wnd : vWndPool)
		{
			wnd.Destroy();
		}
		vWndPool.clear();
	}

	public final void registerWnd(final WndBase wnd)
	{
		vWndPool.add(wnd);
	}

	public final void registerWnd(final WndBase wnd, int location)
	{
		vWndPool.add(location, wnd);
	}

	public final void deleteWnd(WndBase wnd)
	{
		vWndPool.remove(wnd);
		//wnd.Destroy(); called by Destroy()..
	}

	public final void processTouch(final MotionEvent e)
	{
		for (int i = 0; i < vWndPool.size(); i++) // allow ConcurrentModification
		{
			final WndBase wnd = vWndPool.get(i);
			if (!wnd.isPaused())
				wnd.onTouch(e);
		}
	}

	public final void Render()
	{
		for (WndBase wnd : vWndPool) // ConcurrentModificationException if modification at Render
		{
			if (wnd.isVisible())
				wnd.onDraw();
		}
	}

	public final void Update(final long elapsed)
	{
		for (int i = 0; i < vWndPool.size(); i++) // ConcurrentModification
		{
			final WndBase wnd = vWndPool.get(i);
			if (!wnd.isPaused())
				wnd.Process(elapsed);
		}
	}
}
