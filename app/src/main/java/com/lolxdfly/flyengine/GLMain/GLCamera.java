package com.lolxdfly.flyengine.GLMain;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLES20;
import android.opengl.Matrix;

public final class GLCamera
{
	//Resolutions
	public int mScreenWidth = 1920;
    public int mScreenHeight = 1080;
	public final float mCenter[] = new float[2];
	public static float mScaleX = 1.0f;
	public static float mScaleY = 1.0f;
	public static float mScreenScaleX = 1.0f;
	public static float mScreenScaleY = 1.0f;
	public static float miScaleX = 1.0f;
	public static float miScaleY = 1.0f;
	public static final float mVirtualScreenWidth = 1920.0f;
	public static final float mVirtualScreenHeight = 1080.0f;

	//matrices
	private final float mtrxProjection[] = new float[16];
    private final float mtrxView[] = new float[16];
	private final float mtrxModel[] = new float[16];
    private final float mtrxScale[] = new float[16];
	public static final float mtrxMVP[] = new float[16];
	public static final float mtrxUI[] = new float[16];
	
	//other
	private final float pos[] = {0.0f, 0.0f};
	private float rot = 0.0f;

	public GLCamera()
	{
		//Matrix.setIdentityM(mtrxModel, 0);
		//Matrix.translateM(mtrxModel, 0, (-mVirtualScreenWidth / 2), (-mVirtualScreenHeight / 2), 0.0f);
	}

	public final void resize(final int width, final int height)
	{
		// We need to know the current width and height.
        mScreenWidth = width;// = 800;
        mScreenHeight = height;// = 600;
		miScaleX = mVirtualScreenWidth / (float)width;
		miScaleY = mVirtualScreenHeight / (float)height;
		mScreenScaleX = (float)width / mVirtualScreenWidth;
		mScreenScaleY = (float)height / mVirtualScreenHeight;
		mCenter[0] = mScreenWidth / 2;
		mCenter[1] = mScreenHeight / 2;

        // Redo the Viewport, making it fullscreen.
        GLES20.glViewport(0, 0, width, height);

        zeroMatrix(mtrxProjection);
		// Setup our screen width and height for normal sprite translation.
        Matrix.orthoM(mtrxProjection, 0, 0.0f, mVirtualScreenWidth, mVirtualScreenHeight, 0.0f, 0.0f, 1.0f);

		//scale r/v
		Matrix.setIdentityM(mtrxScale, 0);

		updateMVP();
		System.arraycopy(mtrxMVP, 0, mtrxUI, 0, 16);
	}
	
	public final void setNearAndFar(final float near, final float far)
	{
		zeroMatrix(mtrxProjection);
		
		// Setup our screen width and height for normal sprite translation.
        Matrix.orthoM(mtrxProjection, 0, 0.0f, mScreenWidth, mScreenHeight, 0.0f, near, far);
		
		updateMVP();
	}
		
	private final void updateMVP()
	{
		//reset/init
		Matrix.setIdentityM(mtrxModel, 0);
		Matrix.setLookAtM(mtrxView, 0, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

		//transform camera
		Matrix.translateM(mtrxModel, 0, -mCenter[0], -mCenter[1], 0.0f);
		Matrix.translateM(mtrxView, 0, mCenter[0] + pos[0], mCenter[1] + pos[1], 0.0f);
		Matrix.rotateM(mtrxView, 0, rot, 0.0f, 0.0f, 1.0f);

		//multiply
		Matrix.multiplyMM(mtrxMVP, 0, mtrxView, 0, mtrxModel, 0);
		Matrix.multiplyMM(mtrxMVP, 0, mtrxMVP, 0, mtrxScale, 0);
		Matrix.multiplyMM(mtrxMVP, 0, mtrxProjection, 0, mtrxMVP, 0);
	}
	public final float[] getRotatedMatrix(final float degree, final float width, final float height)
	{
		final float[] mtrxTmp = new float[16];

		//reset/init
		Matrix.setIdentityM(mtrxModel, 0);
		Matrix.setLookAtM(mtrxView, 0, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

		//transform camera
		Matrix.translateM(mtrxModel, 0, -mCenter[0], -mCenter[1], 0.0f);
		Matrix.translateM(mtrxView, 0, mCenter[0] + pos[0], mCenter[1] + pos[1], 0.0f);
		Matrix.rotateM(mtrxView, 0, rot, 0.0f, 0.0f, 1.0f);

		//transform object
		Matrix.translateM(mtrxModel, 0, mCenter[0] - (width / 2), mCenter[1] - (height / 2), 0.0f);
		Matrix.translateM(mtrxView, 0, -mCenter[0] + (width / 2), -mCenter[1] + (height / 2), 0.0f);
		Matrix.rotateM(mtrxView, 0, degree, 0.0f, 0.0f, 1.0f);

		//multiply
		Matrix.multiplyMM(mtrxTmp, 0, mtrxView, 0, mtrxModel, 0);
		Matrix.multiplyMM(mtrxTmp, 0, mtrxTmp, 0, mtrxScale, 0);
		Matrix.multiplyMM(mtrxTmp, 0, mtrxProjection, 0, mtrxTmp, 0);
		return mtrxTmp;
	}
	public final void rotate(final float degree)
	{
		rot += degree;
		updateMVP();
	}
	public final void setdegree(final float degree)
	{
		rot = degree;
		updateMVP();
	}
	public final float getdegree()
	{
		return rot;
	}
	public final void scale(final float x, final float y)
	{
		Matrix.setIdentityM(mtrxScale, 0);
		Matrix.scaleM(mtrxScale, 0, x, y, 1.0f);
		mScaleX = x;
		mScaleY = y;
		updateMVP();
	}
	public final void setX(final float x)
	{
		pos[0] = x;
		updateMVP();
	}
	public final void setY(final float y)
	{
		pos[1] = y;
		updateMVP();
	}
	public final void Move(final float x, final float y)
	{
		pos[0] += x;
		pos[1] += y;
		updateMVP();
	}
	public final void setCenterPoint(final float x, final float y)
	{
		setX(x - (mVirtualScreenWidth / 2));
		setY(y - (mVirtualScreenHeight / 2));
	}
	public final void MoveToPoint(final float x, final float y, final float speed)
	{
		final float dx1 = Math.abs(x - pos[0]);
		final float dy1 = Math.abs(y - pos[1]);
		//double d = Math.sqrt(Math.pow(dx1, 2) + Math.pow(dy1, 2));

		if (dx1 <= 10 && dy1 <= 10)
			return;

		float dx = (dx1 / (dx1 + dy1)) * speed;
		float dy = (dy1 / (dx1 + dy1)) * speed;

		if (x <= pos[0])
			dx = -dx;

		if (y <= pos[1])
			dy = -dy;

		Move(dx, dy);
	}
	public final float getX()
	{
		return pos[0];
	}
	public final float getY()
	{
		return pos[1];
	}
	/*public float[] getMVP()
	 {
	 return mtrxMVP;
	 }*/
	private final void zeroMatrix(final float m[])
	{
		for (short i = 0; i < 16; i++)
			m[i] = 0.0f;
	}

	/******Vector*******/

	public static final void rotateVec(final float vec[], float d)
	{
		if (d == 0)
			return;

		d *= Math.PI / 180.0f;
		//d -= Math.atan2(vec[1], vec[0]);

		final float[] tmp = new float[2];
		tmp[0] = vec[0] * (float)Math.cos(d) - vec[1] * (float)Math.sin(d);
		tmp[1] = vec[0] * (float)Math.sin(d) + vec[1] * (float)Math.cos(d);
		vec[0] = tmp[0];
		vec[1] = tmp[1];
	}
	public static final void rotateVec_point(final float vec[], final float origin[], final float d)
	{
		vec[0] -= origin[0];
		vec[1] -= origin[1];
		rotateVec(vec, d);
		vec[0] += origin[0];
		vec[1] += origin[1];
	}
	public final void transformVec(final float vec[])
	{
		//vec[1] = mVirtualScreenHeight - vec[1];
		rotateVec_point(vec, mCenter, -rot);
		vec[0] *= mScaleX * mScreenScaleX;
		vec[1] *= mScaleY * mScreenScaleY;
		vec[0] -= pos[0];
		vec[1] = pos[1] + mScreenHeight - vec[1];
	}
	public static final float[] orthProject(final float[]x, final float[]p)
	{
		final float tmp[] = new float[2];
		final float k = (x[0] * p[0] + x[1] * p[1]) / (x[0] * x[0] + x[1] * x[1]);
		tmp[0] = k * x[0];
		tmp[1] = k * x[1];
		return tmp;
	}
	public static float[] getCenter(final float vec[])
	{
		final float ret[] = {0, 0};

		for (int i = 0; i < vec.length; i += 2)
		{
			ret[0] += vec[i];
			ret[1] += vec[i + 1];
		}
		ret[0] /= vec.length / 2;
		ret[1] /= vec.length / 2;

		return ret;
	}
	public static float[] getCenter3D(final float vec[])
	{
		final float ret[] = {0, 0, 0};

		for (int i = 0; i < vec.length;)
		{
			ret[0] += vec[i++];
			ret[1] += vec[i++];
			ret[2] += vec[i++];
		}
		ret[0] /= vec.length / 3;
		ret[1] /= vec.length / 3;
		ret[2] /= vec.length / 3;

		return ret;
	}

	//this should be faster then atan2
	//actually its not that much faster cause atan2 is native
	final static boolean atan2_fast(final float ax, final float ay, final float bx, final float by, final float center[])
	{
		if (ax - center[0] >= 0 && bx - center[0] < 0)
			return true;
		if (ax - center[0] < 0 && bx - center[0] >= 0)
			return false;
		if (ax - center[0] == 0 && bx - center[0] == 0)
		{
			if (ay - center[1] >= 0 || by - center[1] >= 0)
				return ay > by;
			return by > ay;
		}

		// compute the cross product of vectors (center -> a) x (center -> b)
		final float det = (ax - center[0]) * (by - center[1]) - (bx - center[0]) * (ay - center[1]);
		if (det < 0)
			return true;
		if (det > 0)
			return false;

		// points a and b are on the same line from the center
		// check which point is closer to the center
		final float d1 = (ax - center[0]) * (ax - center[0]) + (ay - center[1]) * (ay - center[1]);
		final float d2 = (bx - center[0]) * (bx - center[0]) + (by - center[1]) * (by - center[1]);
		return d1 > d2;
	}
	public static final void SortVecDeg(final float vec[])
	{
		final float temp[] = new float[2];
		final float c[] = getCenter(vec);
		for (int i = 2; i < vec.length; i += 2)
		{
			for (int j = 0; j < vec.length - i; j += 2)
			{
				//if (-Math.atan2(vec[j] - c[0], -(vec[j + 1] - c[1])) > -Math.atan2(vec[j + 2] - c[0], -(vec[j + 3] - c[1])))
				if (atan2_fast(vec[j + 2], vec[j + 3], vec[j], vec[j + 1],  c))
				{
					temp[0] = vec[j];
					temp[1] = vec[j + 1];

					vec[j] = vec[j + 2];
					vec[j + 1] = vec[j + 3];

					vec[j + 2] = temp[0];
					vec[j + 3] = temp[1];
				}

			}
		}
	}
	public final boolean isVisible(final float rect[])
	{
		if (rot != 0) //too much work
			return true;

		//get min x/y max x/y
		final float tmp[] = new float[]{rect[0], rect[1], rect[0], rect[1]};
		for (short s = 2; s < rect.length; s += 2)
		{
			if (rect[s] < tmp[0])
				tmp[0] = rect[s];

			if (rect[s] > tmp[2])
				tmp[2] = rect[s];

			if (rect[s + 1] < tmp[1])
				tmp[1] = rect[s + 1];

			if (rect[s + 1] > tmp[3])
				tmp[3] = rect[s + 1];
		}

		return !(
			tmp[2] < pos[0] ||
			tmp[0] > pos[0] + mVirtualScreenWidth ||
			tmp[3] < pos[1] ||
			tmp[1] > pos[1] + mVirtualScreenHeight
			);
	}
}
