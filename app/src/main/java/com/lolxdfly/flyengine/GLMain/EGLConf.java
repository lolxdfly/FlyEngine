package com.lolxdfly.flyengine.GLMain;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

public final class EGLConf implements GLSurfaceView.EGLConfigChooser
{
	private int[] mValue;
    private boolean mUsesCoverageAa;

    @Override
    public final EGLConfig chooseConfig(final EGL10 egl, final EGLDisplay display)
	{
        mValue = new int[1];

        // Try to find a normal multisample configuration first.
        int[] configSpec = {
			/* RGB565 */ //(std)
			EGL10.EGL_RED_SIZE, 5,
			EGL10.EGL_GREEN_SIZE, 6,
			EGL10.EGL_BLUE_SIZE, 5,
			EGL10.EGL_DEPTH_SIZE, 16,
			/* OpenGL-ES 2.0 */
			EGL10.EGL_RENDERABLE_TYPE, 4/*EGL14.EGL_OPENGL_ES2_BIT*/,
			/* Antialiasing */
			EGL10.EGL_SAMPLE_BUFFERS, 1,
			EGL10.EGL_SAMPLES, 2,
			/* End */
			EGL10.EGL_NONE
        };
        if (!egl.eglChooseConfig(display, configSpec, null, 0, mValue)) 
		{
            throw new IllegalArgumentException("eglChooseConfig failed");
        }

		int numConfigs = mValue[0];
        if (numConfigs <= 0)
		{
            // No normal multisampling config was found. Try to create a
            // converage multisampling configuration, for the nVidia Tegra2.
            // See the EGL_NV_coverage_sample documentation.

            final int EGL_COVERAGE_BUFFERS_NV = 0x30E0;
            final int EGL_COVERAGE_SAMPLES_NV = 0x30E1;

			configSpec = new int[]{
				/* RGB565 */ //(std)
				EGL10.EGL_RED_SIZE, 5,
				EGL10.EGL_GREEN_SIZE, 6,
				EGL10.EGL_BLUE_SIZE, 5,
				EGL10.EGL_DEPTH_SIZE, 16,
				/* OpenGL-ES 2.0 */
				EGL10.EGL_RENDERABLE_TYPE, 4/*EGL14.EGL_OPENGL_ES2_BIT*/,
				/* Antialiasing */
				EGL_COVERAGE_BUFFERS_NV, 1 /* true */,
				EGL_COVERAGE_SAMPLES_NV, 2,  // always 5 in practice on tegra 2
				/* End */
				EGL10.EGL_NONE
			};
            if (!egl.eglChooseConfig(display, configSpec, null, 0,
									 mValue))
			{
                throw new IllegalArgumentException("2nd eglChooseConfig failed");
            }

            numConfigs = mValue[0];
            if (numConfigs <= 0)
			{
                // Give up, try without multisampling.
				configSpec = new int[]{
					/* RGB565 */ //(std)
					EGL10.EGL_RED_SIZE, 5,
					EGL10.EGL_GREEN_SIZE, 6,
					EGL10.EGL_BLUE_SIZE, 5,
					EGL10.EGL_DEPTH_SIZE, 16,
					/* OpenGL-ES 2.0 */
					EGL10.EGL_RENDERABLE_TYPE, 4/*EGL14.EGL_OPENGL_ES2_BIT*/,
					/* End */
					EGL10.EGL_NONE
                };
                if (!egl.eglChooseConfig(display, configSpec, null, 0,
										 mValue))
				{
                    throw new IllegalArgumentException("3rd eglChooseConfig failed");
                }

                numConfigs = mValue[0];
                if (numConfigs <= 0)
				{
					throw new IllegalArgumentException("No configs match configSpec");
                }
            }
			else
                mUsesCoverageAa = true;
        }

        // Get all matching configurations.
		final EGLConfig[] configs = new EGLConfig[numConfigs];
        if (!egl.eglChooseConfig(display, configSpec, configs, numConfigs,
								 mValue))
		{
            throw new IllegalArgumentException("data eglChooseConfig failed");
        }

        // CAUTION! eglChooseConfigs returns configs with higher bit depth
        // first: Even though we asked for rgb565 configurations, rgb888
        // configurations are considered to be "better" and returned first.
        // You need to explicitly filter the data returned by eglChooseConfig!
        int index = -1;
        for (int i = 0; i < configs.length; ++i)
		{
            if (findConfigAttrib(egl, display, configs[i], EGL10.EGL_RED_SIZE, 0) == 5)
			{
                index = i;
                break;
            }
        }
        return configs.length > 0 ? configs[index] : null;
    }

    private final int findConfigAttrib(final EGL10 egl, final EGLDisplay display,
									   final EGLConfig config, final int attribute, final int defaultValue)
	{
        if (egl.eglGetConfigAttrib(display, config, attribute, mValue))
		{
            return mValue[0];
        }
        return defaultValue;
    }

    public final boolean usesCoverageAa()
	{
        return mUsesCoverageAa;
	}

}
