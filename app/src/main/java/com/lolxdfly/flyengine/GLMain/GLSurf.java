package com.lolxdfly.flyengine.GLMain;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.lolxdfly.flyengine.GLElements.WndBase;
import com.lolxdfly.flyengine.GLUtils.GLAnimationMng;
import com.lolxdfly.flyengine.GLUtils.GLHelp;
import com.lolxdfly.flyengine.GLUtils.LicenseMng;
import com.lolxdfly.flyengine.GLUtils.SoundMng;

public final class GLSurf extends GLSurfaceView
{
	private final GLRender mRenderer;
	private static LicenseMng mLMng;
	//public static AdvertisingMng mAMng;
	//public static GPlayServiceMng mGPlay;
	public static WndManager mWndMng;
	public static GLTextureMng mTexMng;
	public static GLAnimationMng mAnimMng;
	public static SoundMng mSndMng;
	public static GLCamera mCamera;
	public static GLHelp mHelper;

    public GLSurf(final Context context/*, final RelativeLayout rl*/, final WndBase entry, final Class entryclass)
	{
        super(context);

		//nonGL
		mLMng = new LicenseMng(context); //first for licensecheck
		mHelper = new GLHelp(context);
		//mGPlay = new GPlayServiceMng(context);
		/*final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT); 
		 lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM); 
		 mAMng = new AdvertisingMng(context, rl, "ca-app-pub-4161712998623794/3755275569", AdSize.BANNER, lp);*/
		mSndMng = new SoundMng(context);

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

		//GL
		mWndMng = new WndManager();
		mTexMng = new GLTextureMng(context);
		mAnimMng = new GLAnimationMng();
		mCamera = new GLCamera();
		mHelper = new GLHelp(context);

		/*setEGLContextFactory(new EGLContextFactory()
			{
				@Override
				public final void destroyContext(final EGL10 egl, final EGLDisplay display, final EGLContext context)
				{
					egl.eglDestroyContext(display, context);
				}

				@Override
				public EGLContext createContext(final EGL10 egl, final EGLDisplay display, final EGLConfig eglConfig)
				{
					final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
					final int[] contextAttributes = 
					{ EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE };
					final EGLContext renderContext = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, contextAttributes);

					mTexMng.initTexLoadThread(egl, renderContext, display, eglConfig);

					return renderContext;
				}
			});*/

		// set nativ Config (Antialiasing, ...)
		setEGLConfigChooser(new EGLConf());

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new GLRender(entry, entryclass);
        setRenderer(mRenderer);

        // Render the view continuously
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

	public final void onPause()
	{
		mRenderer.onPause();
		//mAMng.setState(true);
	}

	public final void onResume()
	{
		mRenderer.onResume();
		//mAMng.setState(false);
	}

	@Override
	public final boolean onTouchEvent(final MotionEvent event)
	{
		//scale input
		final float x = event.getX() * GLCamera.miScaleX;
		final float y = event.getY() * GLCamera.miScaleY;
		event.setLocation(x, y);

		//pass call
		mRenderer.onTouchEvent(event);

		return true; //super.onTouchEvent(event);
	}

	public final boolean onKeyDown(final int keyCode, final KeyEvent event)
	{
		mSndMng.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) //Exit
		{
			Destroy();
		}
		return true;
	}
	
	public final void Destroy()
	{
		mLMng.Destroy();
		setRenderMode(RENDERMODE_WHEN_DIRTY);
		queueEvent(new Runnable(){
				public final void run()
				{
					mRenderer.Destroy();
				}});
	}
}
